<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTareasTable extends Migration {

	public function up()
	{
		Schema::create('tareas', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('sucursal_id')->unsigned();
			$table->integer('estado_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('tareas');
	}
}