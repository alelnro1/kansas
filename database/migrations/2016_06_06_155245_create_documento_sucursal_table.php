<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentoSucursalTable extends Migration {

	public function up()
	{
		Schema::create('documento_sucursal', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('documento_id')->unsigned();
			$table->integer('usuario_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('documento_sucursal');
	}
}