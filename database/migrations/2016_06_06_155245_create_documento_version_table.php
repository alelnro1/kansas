<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentoVersionTable extends Migration {

	public function up()
	{
		Schema::create('documento_version', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('documento_id')->unsigned();
			$table->integer('usuario_id')->unsigned();
			$table->string('version', 10);
			$table->integer('estado_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('documento_version');
	}
}