<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCargosTable extends Migration {

	public function up()
	{
		Schema::create('cargos', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('padre_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('cargos');
	}
}