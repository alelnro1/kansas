<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCargoUsuarioTable extends Migration {

	public function up()
	{
		Schema::create('cargo_usuario', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('usuario_id')->unsigned();
			$table->integer('cargo_id')->unsigned();
			$table->integer('sucursal_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('cargo_usuario');
	}
}