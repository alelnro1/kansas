<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('tareas', function(Blueprint $table) {
			$table->foreign('sucursal_id')->references('id')->on('sucursales')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('tareas', function(Blueprint $table) {
			$table->foreign('estado_id')->references('id')->on('estados_tareas')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('actividades', function(Blueprint $table) {
			$table->foreign('sucursal_id')->references('id')->on('sucursales')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documento_lectura', function(Blueprint $table) {
			$table->foreign('documento_id')->references('id')->on('documentos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documento_lectura', function(Blueprint $table) {
			$table->foreign('usuario_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documentos', function(Blueprint $table) {
			$table->foreign('tipo_documento_id')->references('id')->on('tipos_documentos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documento_usuario', function(Blueprint $table) {
			$table->foreign('usuario_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documento_usuario', function(Blueprint $table) {
			$table->foreign('documento_id')->references('id')->on('documentos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('cargos', function(Blueprint $table) {
			$table->foreign('padre_id')->references('id')->on('cargos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documento_version', function(Blueprint $table) {
			$table->foreign('documento_id')->references('id')->on('documentos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documento_version', function(Blueprint $table) {
			$table->foreign('usuario_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documento_version', function(Blueprint $table) {
			$table->foreign('estado_id')->references('id')->on('estados_documentos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('logs', function(Blueprint $table) {
			$table->foreign('sucursal_id')->references('id')->on('sucursales')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documento_sucursal', function(Blueprint $table) {
			$table->foreign('documento_id')->references('id')->on('documentos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('documento_sucursal', function(Blueprint $table) {
			$table->foreign('usuario_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('cargo_usuario', function(Blueprint $table) {
			$table->foreign('usuario_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('cargo_usuario', function(Blueprint $table) {
			$table->foreign('cargo_id')->references('id')->on('cargos')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('cargo_usuario', function(Blueprint $table) {
			$table->foreign('sucursal_id')->references('id')->on('sucursales')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('tareas', function(Blueprint $table) {
			$table->dropForeign('tareas_sucursal_id_foreign');
		});
		Schema::table('tareas', function(Blueprint $table) {
			$table->dropForeign('tareas_estado_id_foreign');
		});
		Schema::table('actividades', function(Blueprint $table) {
			$table->dropForeign('actividades_sucursal_id_foreign');
		});
		Schema::table('documento_lectura', function(Blueprint $table) {
			$table->dropForeign('documento_lectura_documento_id_foreign');
		});
		Schema::table('documento_lectura', function(Blueprint $table) {
			$table->dropForeign('documento_lectura_usuario_id_foreign');
		});
		Schema::table('documentos', function(Blueprint $table) {
			$table->dropForeign('documentos_tipo_documento_id_foreign');
		});
		Schema::table('documento_usuario', function(Blueprint $table) {
			$table->dropForeign('documento_usuario_usuario_id_foreign');
		});
		Schema::table('documento_usuario', function(Blueprint $table) {
			$table->dropForeign('documento_usuario_documento_id_foreign');
		});
		Schema::table('cargos', function(Blueprint $table) {
			$table->dropForeign('cargos_padre_id_foreign');
		});
		Schema::table('documento_version', function(Blueprint $table) {
			$table->dropForeign('documento_version_documento_id_foreign');
		});
		Schema::table('documento_version', function(Blueprint $table) {
			$table->dropForeign('documento_version_usuario_id_foreign');
		});
		Schema::table('documento_version', function(Blueprint $table) {
			$table->dropForeign('documento_version_estado_id_foreign');
		});
		Schema::table('logs', function(Blueprint $table) {
			$table->dropForeign('logs_sucursal_id_foreign');
		});
		Schema::table('documento_sucursal', function(Blueprint $table) {
			$table->dropForeign('documento_sucursal_documento_id_foreign');
		});
		Schema::table('documento_sucursal', function(Blueprint $table) {
			$table->dropForeign('documento_sucursal_usuario_id_foreign');
		});
		Schema::table('cargo_usuario', function(Blueprint $table) {
			$table->dropForeign('cargo_usuario_usuario_id_foreign');
		});
		Schema::table('cargo_usuario', function(Blueprint $table) {
			$table->dropForeign('cargo_usuario_cargo_id_foreign');
		});
		Schema::table('cargo_usuario', function(Blueprint $table) {
			$table->dropForeign('cargo_usuario_sucursal_id_foreign');
		});
	}
}