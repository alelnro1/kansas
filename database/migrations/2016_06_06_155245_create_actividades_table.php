<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActividadesTable extends Migration {

	public function up()
	{
		Schema::create('actividades', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('sucursal_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('actividades');
	}
}