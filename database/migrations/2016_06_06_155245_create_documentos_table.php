<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentosTable extends Migration {

	public function up()
	{
		Schema::create('documentos', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('tipo_documento_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('documentos');
	}
}