﻿-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-07-2016 a las 23:30:44
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kansas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE `actividades` (
  `id` int(10) UNSIGNED NOT NULL,
  `manager_log_id` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sucursal_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `actividades`
--

TRUNCATE TABLE `actividades`;
--
-- Volcado de datos para la tabla `actividades`
--

INSERT INTO `actividades` (`id`, `manager_log_id`, `nombre`, `descripcion`, `created_at`, `updated_at`, `deleted_at`, `sucursal_id`) VALUES
(5, 8, 'test', 'lala', '2016-06-28 21:29:26', '2016-06-28 21:29:26', NULL, 1),
(6, 8, 'holaa', 'testing', '2016-06-28 21:34:49', '2016-06-28 21:34:49', NULL, 1),
(7, 8, 'ahora', 'si', '2016-06-28 21:41:02', '2016-06-28 21:41:02', NULL, 1),
(8, 8, 'juanma', 'riquelme', '2016-06-28 21:56:05', '2016-06-28 21:56:05', NULL, 1),
(9, 8, 'juas', 'awesome', '2016-06-28 22:31:16', '2016-06-28 22:31:16', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

CREATE TABLE `archivos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL,
  `documento_id` int(10) UNSIGNED NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncar tablas antes de insertar `archivos`
--

TRUNCATE TABLE `archivos`;
--
-- Volcado de datos para la tabla `archivos`
--

INSERT INTO `archivos` (`id`, `nombre`, `url`, `documento_id`, `updated_at`, `created_at`) VALUES
(1, 'Desert.jpg', 'uploads/archivos/906732_1466456353__2180413__Desert.jpg', 1, '2016-06-20 23:59:13', '2016-06-20 23:59:13'),
(2, 'Hydrangeas.jpg', 'uploads/archivos/927288_1466456353__2180413__Hydrangeas.jpg', 1, '2016-06-20 23:59:14', '2016-06-20 23:59:14'),
(3, 'Jellyfish.jpg', 'uploads/archivos/218398_1466456353__2180413__Jellyfish.jpg', 1, '2016-06-20 23:59:14', '2016-06-20 23:59:14'),
(4, 'composer.txt', 'uploads/archivos/698145_1466462610__2180413__composer.txt', 2, '2016-06-21 01:43:31', '2016-06-21 01:43:31'),
(5, 'proyecto gustavo aguirre berrotaran sitio eventos.pdf', 'uploads/archivos/309802_1466462688__2180413__proyecto gustavo aguirre berrotaran sitio eventos.pdf', 3, '2016-06-21 01:44:48', '2016-06-21 01:44:48'),
(6, 'Sin título.png', 'uploads/archivos/593503_1466462776__2180413__Sin título.png', 4, '2016-06-21 01:46:16', '2016-06-21 01:46:16'),
(7, 'pesas.pdf', 'uploads/archivos/258465_1466519434__2180413__pesas.pdf', 5, '2016-06-21 17:30:34', '2016-06-21 17:30:34'),
(8, 'Modelo de Datos 20160512.pdf', 'uploads/archivos/751209_1466519553__2180413__Modelo de Datos 20160512.pdf', 6, '2016-06-21 17:32:33', '2016-06-21 17:32:33'),
(9, 'avatar.png', 'uploads/archivos/563604_1466520313__2180413__avatar.png', 7, '2016-06-21 17:45:13', '2016-06-21 17:45:13'),
(10, 'Cybran_Faction_Logo.jpg', 'uploads/archivos/699327_1466618920__2180413__Cybran_Faction_Logo.jpg', 10, '2016-06-22 21:13:18', '2016-06-22 21:13:18'),
(11, 'Logo_TV_2015.png', 'uploads/archivos/167534_1466618959__2180413__Logo_TV_2015.png', 10, '2016-06-22 21:13:18', '2016-06-22 21:13:18'),
(12, '2-min.jpg', 'uploads/archivos/744628_1467719352__2180413__2-min.jpg', 14, '2016-07-05 14:52:01', '2016-07-05 14:52:01'),
(13, 'image1.JPG', 'uploads/archivos/814453_1467719368__2180413__image1.JPG', 14, '2016-07-05 14:52:01', '2016-07-05 14:52:01'),
(14, '2-min.jpg', 'uploads/archivos/895996_1467719379__2180413__2-min.jpg', 14, '2016-07-05 14:52:01', '2016-07-05 14:52:01'),
(15, 'DER APM Tool.pdf', 'uploads/archivos/208007_1467719380__2180413__DER APM Tool.pdf', 14, '2016-07-05 14:52:01', '2016-07-05 14:52:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE `cargos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manager_log` tinyint(1) NOT NULL COMMENT '0 => no ve el ML; 1 => ve el ML',
  `empresa_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `padre_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `cargos`
--

TRUNCATE TABLE `cargos`;
--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id`, `nombre`, `manager_log`, `empresa_id`, `created_at`, `updated_at`, `deleted_at`, `padre_id`) VALUES
(1, 'Director', 1, 1, '2016-06-25 04:27:10', '2016-06-25 04:27:10', NULL, NULL),
(2, 'Super Admin', 0, 1, NULL, NULL, NULL, NULL),
(3, 'Gte de Operaciones', 0, 1, '2016-06-25 04:27:45', '2016-06-25 04:27:45', NULL, 1),
(4, 'Administrador', 0, 1, NULL, NULL, NULL, NULL),
(5, 'Jefe de Cocina', 0, 1, NULL, NULL, NULL, 4),
(6, 'Cocinero', 0, 1, '2016-06-18 20:50:28', '2016-06-18 20:50:28', NULL, 5),
(7, 'Analista de Ppto', 0, 1, '2016-06-25 04:28:54', '2016-06-25 04:28:54', NULL, 2),
(8, 'Asistente de Proyectos', 1, 1, '2016-06-25 04:29:09', '2016-07-05 22:55:49', NULL, 7),
(9, 'Ayudante de cocina', 0, 1, '2016-06-18 20:57:43', '2016-06-18 20:57:43', NULL, 6),
(10, 'Limpiador', 0, 1, NULL, NULL, NULL, 6),
(11, 'Jefe Capacitación', 0, 1, '2016-06-25 04:30:59', '2016-06-25 04:30:59', NULL, 2),
(12, 'Jefe Toma de Personal', 0, 1, '2016-06-25 04:31:15', '2016-06-25 04:31:15', NULL, 2),
(13, 'Operario', 0, 1, '2016-06-25 04:31:32', '2016-06-25 19:20:46', '2016-06-25 19:20:46', 7),
(14, 'Limpieza', 0, 1, '2016-06-25 04:31:49', '2016-06-25 04:31:49', NULL, 12),
(15, 'Entrevistador', 0, 1, '2016-06-25 04:32:11', '2016-06-25 04:32:11', NULL, 12),
(16, 'Maestro', 0, 1, '2016-06-25 04:32:22', '2016-06-25 04:32:22', NULL, 11),
(17, 'Operario Planton', 0, 1, '2016-06-25 19:20:33', '2016-06-25 19:20:33', NULL, 13),
(18, 'test', 0, 1, '2016-06-19 09:50:58', '2016-06-19 09:51:59', '2016-06-19 09:51:59', NULL),
(19, 'testing', 0, 1, '2016-06-19 09:51:15', '2016-06-19 09:51:15', NULL, 18),
(20, 'Metre', 0, 1, '2016-06-21 01:47:14', '2016-06-21 01:47:14', NULL, 4),
(21, 'Gerente General', 1, 1, '2016-06-21 14:24:09', '2016-06-21 14:24:44', NULL, 4),
(22, 'CEO', 0, 1, '2016-06-21 14:24:30', '2016-06-21 14:24:30', NULL, NULL),
(23, 'Limpiador de Piso', 0, 1, '2016-06-21 14:25:57', '2016-06-21 14:25:57', NULL, 10),
(24, 'Mozo', 0, 1, '2016-06-21 17:21:33', '2016-06-21 17:21:33', NULL, 4),
(25, 'Mega Manager', 0, 1, '2016-06-22 21:40:41', '2016-06-22 21:40:41', NULL, 21),
(26, 'test', 1, 1, '2016-07-05 22:52:54', '2016-07-05 22:55:41', NULL, NULL),
(27, 'asdasd', 1, 1, '2016-07-05 22:56:04', '2016-07-05 22:56:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo_sucursal_usuario`
--

CREATE TABLE `cargo_sucursal_usuario` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `usuario_id` int(10) UNSIGNED NOT NULL,
  `cargo_id` int(10) UNSIGNED NOT NULL,
  `sucursal_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `cargo_sucursal_usuario`
--

TRUNCATE TABLE `cargo_sucursal_usuario`;
--
-- Volcado de datos para la tabla `cargo_sucursal_usuario`
--

INSERT INTO `cargo_sucursal_usuario` (`id`, `created_at`, `updated_at`, `usuario_id`, `cargo_id`, `sucursal_id`) VALUES
(20, NULL, '2016-06-10 17:08:07', 22, 4, 2),
(24, NULL, NULL, 30, 2, NULL),
(29, NULL, '2016-06-20 22:36:18', 42, 9, 1),
(30, NULL, '2016-06-21 01:41:09', 43, 5, 3),
(31, NULL, '2016-06-21 14:29:33', 44, 23, 8),
(32, NULL, '2016-06-21 17:28:07', 46, 24, 3),
(33, NULL, '2016-06-22 23:52:20', 47, 21, 2),
(34, NULL, '2016-06-22 23:52:20', 47, 5, 1),
(35, NULL, NULL, 32, 21, 1),
(36, NULL, NULL, 32, 21, 3),
(37, NULL, NULL, 32, 4, 1),
(38, NULL, '2016-07-13 15:49:20', 23, 5, 1),
(39, NULL, '2016-07-13 17:00:02', 49, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE `documentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `user_aprobador_id` int(10) UNSIGNED DEFAULT NULL,
  `autor_id` int(10) UNSIGNED NOT NULL,
  `padre_id` int(10) UNSIGNED DEFAULT NULL,
  `version_anterior` int(10) UNSIGNED DEFAULT NULL,
  `estado_id` int(10) UNSIGNED NOT NULL,
  `comentario` text COLLATE utf8_unicode_ci COMMENT 'El comentario del usuario aprobador',
  `empresa_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `tipo_documento_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `documentos`
--

TRUNCATE TABLE `documentos`;
--
-- Volcado de datos para la tabla `documentos`
--

INSERT INTO `documentos` (`id`, `nombre`, `descripcion`, `user_aprobador_id`, `autor_id`, `padre_id`, `version_anterior`, `estado_id`, `comentario`, `empresa_id`, `created_at`, `updated_at`, `deleted_at`, `tipo_documento_id`) VALUES
(1, 'ula ula', 'test', 42, 32, NULL, NULL, 1, '', NULL, '2016-06-20 23:59:13', '2016-06-20 23:59:13', NULL, 2),
(2, 'Prueba Mario', 'Prueba Mario', 43, 32, NULL, NULL, 2, '', NULL, '2016-06-21 01:43:30', '2016-06-21 01:43:30', NULL, 1),
(3, 'Prueba Mario 1', 'Prueba Mario 1', NULL, 32, NULL, NULL, 4, '', NULL, '2016-06-21 01:44:48', '2016-06-21 01:44:48', NULL, 2),
(4, 'Prueba Mario 2', 'Prueba Mario 2', 43, 32, NULL, NULL, 4, '', NULL, '2016-06-21 01:46:16', '2016-06-21 01:46:16', NULL, 3),
(5, 'Doc Mozo', 'Doc Mozo', NULL, 46, NULL, NULL, 4, '', NULL, '2016-06-21 17:30:33', '2016-06-21 17:30:33', NULL, 1),
(6, 'Doc  Mozo 1', 'Doc  Mozo 1', 43, 46, NULL, NULL, 2, '', NULL, '2016-06-21 17:32:33', '2016-06-21 17:32:33', NULL, 2),
(7, 'Doc  Mozo 2', 'Doc  Mozo 2', NULL, 46, NULL, NULL, 4, '', NULL, '2016-06-21 17:45:12', '2016-06-21 17:45:12', NULL, 3),
(10, 'test', 'lalal', NULL, 32, NULL, NULL, 1, '', NULL, '2016-06-22 21:13:18', '2016-06-22 21:13:18', NULL, 1),
(11, 'hola mundo', 'hola mundo', 42, 32, NULL, NULL, 2, '', NULL, '2016-07-05 14:46:37', '2016-07-05 14:46:37', NULL, 1),
(12, 'hola mundo', 'hola mundo', 42, 32, NULL, NULL, 2, '', NULL, '2016-07-05 14:49:47', '2016-07-05 14:49:47', NULL, 1),
(13, 'hola mundo', 'hola mundo', 42, 32, NULL, NULL, 2, '', NULL, '2016-07-05 14:50:10', '2016-07-05 14:50:10', NULL, 1),
(14, 'hola mundo', 'hola mundo', 32, 32, NULL, NULL, 3, 'malisimo', NULL, '2016-07-05 14:52:00', '2016-07-05 17:20:48', NULL, 1),
(15, 'aguante boca', '123123', NULL, 32, NULL, NULL, 4, NULL, 1, '2016-07-05 22:40:35', '2016-07-05 22:40:35', NULL, 1),
(19, '', '', NULL, 23, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 2),
(22, 'mega test', 'una super mega prueba', NULL, 23, NULL, NULL, 1, NULL, 1, '2016-07-13 16:52:03', '2016-07-13 16:52:03', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento_lectura`
--

CREATE TABLE `documento_lectura` (
  `id` int(10) UNSIGNED NOT NULL,
  `leido` tinyint(1) NOT NULL COMMENT 'Dice si el documento fue leido por el destinatario',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `documento_id` int(10) UNSIGNED NOT NULL,
  `usuario_id` int(10) UNSIGNED NOT NULL,
  `sucursal_id` int(10) UNSIGNED NOT NULL,
  `cargo_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `documento_lectura`
--

TRUNCATE TABLE `documento_lectura`;
--
-- Volcado de datos para la tabla `documento_lectura`
--

INSERT INTO `documento_lectura` (`id`, `leido`, `created_at`, `updated_at`, `documento_id`, `usuario_id`, `sucursal_id`, `cargo_id`) VALUES
(1, 0, '2016-06-20 23:59:13', '2016-06-20 23:59:13', 1, 22, 2, 4),
(2, 0, '2016-06-20 23:59:13', '2016-06-20 23:59:13', 1, 23, 1, 5),
(3, 0, '2016-06-20 23:59:13', '2016-06-20 23:59:13', 1, 42, 1, 9),
(4, 0, '2016-06-21 01:43:31', '2016-06-21 01:43:31', 2, 43, 3, 5),
(5, 0, '2016-06-21 01:44:48', '2016-06-21 01:44:48', 3, 22, 2, 4),
(6, 0, '2016-06-21 01:44:48', '2016-06-21 01:44:48', 3, 23, 1, 5),
(7, 0, '2016-06-21 01:44:48', '2016-06-21 01:44:48', 3, 42, 1, 9),
(8, 0, '2016-06-21 01:44:48', '2016-06-21 01:44:48', 3, 43, 3, 5),
(9, 0, '2016-06-21 01:46:16', '2016-06-21 01:46:16', 4, 22, 2, 4),
(10, 0, '2016-06-21 01:46:16', '2016-06-21 01:46:16', 4, 23, 1, 5),
(11, 0, '2016-06-21 01:46:16', '2016-06-21 01:46:16', 4, 42, 1, 9),
(12, 0, '2016-06-21 17:30:34', '2016-06-21 17:30:34', 5, 46, 3, 24),
(13, 1, '2016-06-21 17:32:33', '2016-07-05 18:04:16', 6, 32, 3, 5),
(14, 0, '2016-06-21 17:32:33', '2016-06-21 17:32:33', 6, 46, 3, 24),
(15, 0, '2016-06-21 17:45:13', '2016-06-21 17:45:13', 7, 43, 3, 5),
(16, 0, '2016-06-21 17:45:13', '2016-06-21 17:45:13', 7, 46, 3, 24),
(29, 0, '2016-06-22 21:13:18', '2016-06-22 21:13:18', 10, 22, 2, 4),
(30, 0, '2016-06-22 21:13:18', '2016-06-22 21:13:18', 10, 23, 1, 5),
(31, 0, '2016-06-22 21:13:18', '2016-06-22 21:13:18', 10, 42, 1, 9),
(32, 0, '2016-06-22 21:13:18', '2016-06-22 21:13:18', 10, 43, 3, 5),
(33, 0, '2016-06-22 21:13:18', '2016-06-22 21:13:18', 10, 44, 8, 23),
(34, 0, '2016-06-22 21:13:18', '2016-06-22 21:13:18', 10, 46, 3, 24),
(35, 0, '2016-07-05 14:49:47', '2016-07-05 14:49:47', 12, 22, 2, 4),
(36, 0, '2016-07-05 14:49:47', '2016-07-05 14:49:47', 12, 23, 1, 5),
(37, 0, '2016-07-05 14:52:00', '2016-07-05 14:52:00', 14, 22, 2, 4),
(38, 0, '2016-07-05 14:52:00', '2016-07-05 14:52:00', 14, 23, 1, 5),
(39, 0, '2016-07-05 14:52:00', '2016-07-05 14:52:00', 14, 42, 1, 9),
(40, 0, '2016-07-05 14:52:01', '2016-07-05 14:52:01', 14, 43, 3, 5),
(41, 0, '2016-07-05 14:52:01', '2016-07-05 14:52:01', 14, 44, 8, 23),
(42, 0, '2016-07-05 14:52:01', '2016-07-05 14:52:01', 14, 46, 3, 24),
(43, 0, '2016-07-05 14:52:01', '2016-07-05 14:52:01', 14, 47, 2, 6),
(44, 0, '2016-07-05 14:52:01', '2016-07-05 14:52:01', 14, 47, 1, 5),
(45, 1, '2016-07-05 14:52:01', '2016-07-05 22:20:21', 14, 32, 1, 21),
(46, 0, '2016-07-05 14:52:01', '2016-07-05 14:52:01', 14, 32, 3, 21),
(47, 0, '2016-07-05 22:40:35', '2016-07-05 22:40:35', 15, 42, 1, 9),
(48, 0, '2016-07-13 16:52:03', '2016-07-13 16:52:03', 22, 22, 2, 4),
(49, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 42, 1, 9),
(50, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 43, 3, 5),
(51, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 44, 8, 23),
(52, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 46, 3, 24),
(53, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 47, 2, 6),
(54, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 47, 1, 5),
(55, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 32, 1, 21),
(56, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 32, 3, 21),
(57, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 32, 1, 4),
(58, 0, '2016-07-13 16:52:04', '2016-07-13 16:52:04', 22, 23, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento_sucursal`
--

CREATE TABLE `documento_sucursal` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `documento_id` int(10) UNSIGNED NOT NULL,
  `usuario_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `documento_sucursal`
--

TRUNCATE TABLE `documento_sucursal`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(10) UNSIGNED NOT NULL,
  `razon_social` varchar(200) NOT NULL,
  `cuit` varchar(60) NOT NULL,
  `domicilio` varchar(200) NOT NULL,
  `archivo` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncar tablas antes de insertar `empresas`
--

TRUNCATE TABLE `empresas`;
--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `razon_social`, `cuit`, `domicilio`, `archivo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'DoublePoint', '1234567890', 'ciudad de la paz', 'uploads/archivos/747721_1468413814_.jpg', '2016-07-13 15:43:34', '2016-07-13 15:43:34', NULL),
(2, 'Google', '123123', 'massachusets', '', '2016-07-13 16:57:53', '2016-07-13 16:57:53', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_documentos`
--

CREATE TABLE `estados_documentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `estados_documentos`
--

TRUNCATE TABLE `estados_documentos`;
--
-- Volcado de datos para la tabla `estados_documentos`
--

INSERT INTO `estados_documentos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Borrador', NULL, NULL, NULL),
(2, 'Pendiente de Aprobación', NULL, NULL, NULL),
(3, 'Desaprobado', NULL, NULL, NULL),
(4, 'Publicado', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sucursal_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `logs`
--

TRUNCATE TABLE `logs`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manager_logs`
--

CREATE TABLE `manager_logs` (
  `id` int(11) UNSIGNED NOT NULL,
  `sucursal_id` int(10) UNSIGNED NOT NULL,
  `cerrado` tinyint(1) NOT NULL,
  `caja_apertura` int(10) UNSIGNED DEFAULT NULL,
  `manager_apertura` int(11) NOT NULL,
  `caja_mediodia` int(10) UNSIGNED DEFAULT NULL,
  `manager_mediodia` int(11) NOT NULL,
  `caja_cierre` int(10) UNSIGNED DEFAULT NULL,
  `manager_cierre` int(11) NOT NULL,
  `descuento_empleados_30_neto` double DEFAULT NULL,
  `descuento_empleados_100_neto` double DEFAULT NULL,
  `rrpp_mgr_bebidas_neto` double DEFAULT NULL,
  `rrpp_malo_cocina_neto` double DEFAULT NULL,
  `rrpp_malo_recepcion_neto` double DEFAULT NULL,
  `rrpp_malo_scv_neto` double DEFAULT NULL,
  `rrpp_malo_valet_neto` double DEFAULT NULL,
  `rrpp_malo_cliente_neto` double DEFAULT NULL,
  `rrpp_malo_expo_neto` double DEFAULT NULL,
  `rrpp_malo_mozo_neto` double DEFAULT NULL,
  `ventas` double DEFAULT NULL,
  `guest` double DEFAULT NULL,
  `guest_avg` double DEFAULT NULL,
  `proyectado` double DEFAULT NULL,
  `cambio` double DEFAULT NULL,
  `prosegur` double DEFAULT NULL,
  `empresa_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncar tablas antes de insertar `manager_logs`
--

TRUNCATE TABLE `manager_logs`;
--
-- Volcado de datos para la tabla `manager_logs`
--

INSERT INTO `manager_logs` (`id`, `sucursal_id`, `cerrado`, `caja_apertura`, `manager_apertura`, `caja_mediodia`, `manager_mediodia`, `caja_cierre`, `manager_cierre`, `descuento_empleados_30_neto`, `descuento_empleados_100_neto`, `rrpp_mgr_bebidas_neto`, `rrpp_malo_cocina_neto`, `rrpp_malo_recepcion_neto`, `rrpp_malo_scv_neto`, `rrpp_malo_valet_neto`, `rrpp_malo_cliente_neto`, `rrpp_malo_expo_neto`, `rrpp_malo_mozo_neto`, `ventas`, `guest`, `guest_avg`, `proyectado`, `cambio`, `prosegur`, `empresa_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '2016-06-27 03:00:00', '2016-07-13 20:58:01'),
(4, 1, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-06-28 19:49:23', '2016-06-28 19:49:23'),
(8, 1, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-06-29 20:45:59', '2016-06-28 22:58:56'),
(14, 1, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-06-30 14:18:42', '2016-07-01 18:59:39'),
(15, 1, 0, 20, 32, 30, 47, 40, 32, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 1, '2016-07-01 18:59:43', '2016-07-13 21:37:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_06_155245_create_actividades_table', 1),
('2016_06_06_155245_create_cargo_usuario_table', 1),
('2016_06_06_155245_create_cargos_table', 1),
('2016_06_06_155245_create_documento_lectura_table', 1),
('2016_06_06_155245_create_documento_sucursal_table', 1),
('2016_06_06_155245_create_documento_usuario_table', 1),
('2016_06_06_155245_create_documento_version_table', 1),
('2016_06_06_155245_create_documentos_table', 1),
('2016_06_06_155245_create_estados_documentos_table', 1),
('2016_06_06_155245_create_estados_tareas_table', 1),
('2016_06_06_155245_create_logs_table', 1),
('2016_06_06_155245_create_sucursales_table', 1),
('2016_06_06_155245_create_tareas_table', 1),
('2016_06_06_155245_create_tipos_documentos_table', 1),
('2016_06_06_155255_create_foreign_keys', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `password_resets`
--

TRUNCATE TABLE `password_resets`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE `sucursales` (
  `id` int(10) UNSIGNED NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `domicilio` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_apertura` datetime NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `archivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `sucursales`
--

TRUNCATE TABLE `sucursales`;
--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`id`, `descripcion`, `domicilio`, `fecha_apertura`, `email`, `telefono`, `nombre`, `archivo`, `empresa_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', '', '0000-00-00 00:00:00', '', '', 'Balvanera', '', 1, NULL, NULL, NULL),
(2, '', '', '0000-00-00 00:00:00', '', '', 'Palermo', '', 1, NULL, NULL, NULL),
(3, '', 'Caballito, Buenos Aires, Ciudad Autónoma de Buenos Aires, Argentina', '0000-00-00 00:00:00', 'kansascaballito@kansas.com.ar', '47777777', 'Caballito', '', 1, '2016-06-16 16:50:09', '2016-06-16 16:50:09', NULL),
(4, 'testing', 'Hurlingham, Buenos Aires, Argentina', '0000-00-00 00:00:00', 'test@lala.com', '123', 'test20', '', 1, '2016-06-20 23:58:28', '2016-06-21 00:18:04', '2016-06-21 00:18:04'),
(5, 'testing', 'Palermo, Capital Federal, Ciudad Autónoma de Buenos Aires, Argentina', '0000-00-00 00:00:00', '', '', 'sucu sucu', '/home/doublepoint.com.ar/domains/kansas.doublepoint.com.ar/tmp/phpkzGNik', 1, '2016-06-21 00:12:57', '2016-06-21 00:14:15', '2016-06-21 00:14:15'),
(6, 'lala', 'Ciudad de La Paz, Buenos Aires, Ciudad Autónoma de Buenos Aires, Argentina', '0000-00-00 00:00:00', '', '', 'test2', 'uploads/archivos/802735_1466457500_.jpg', 1, '2016-06-21 00:18:20', '2016-06-21 00:18:20', NULL),
(7, 'mi super casa', 'Ramos Mejía, Buenos Aires, Argentina', '0000-00-00 00:00:00', 'test@testing.com', '47777777', 'mi casa', 'uploads/archivos/481367_1466482942_.jpg', 1, '2016-06-21 07:22:21', '2016-06-21 07:22:22', NULL),
(8, 'sdkfs k fhskdfasjk', 'Pasaje Libarona, Santiago del Estero, Argentina', '0000-00-00 00:00:00', 'barrioparque@doublepoint.com.ar', '20398203', 'Barrio Parque', 'uploads/archivos/342373_1466507924_.jpg', 1, '2016-06-21 14:18:44', '2016-06-21 14:18:44', NULL),
(9, 'kjlsdfjsl', 'Yatay, Buenos Aires, Ciudad Autónoma de Buenos Aires, Argentina', '0000-00-00 00:00:00', '', '', 'dfskjlfsd', '', 1, '2016-06-21 14:39:31', '2016-06-21 14:39:35', '2016-06-21 14:39:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas`
--

CREATE TABLE `tareas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `fecha_cierre` timestamp NULL DEFAULT NULL,
  `comentario_cierre` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sucursal_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `tareas`
--

TRUNCATE TABLE `tareas`;
--
-- Volcado de datos para la tabla `tareas`
--

INSERT INTO `tareas` (`id`, `nombre`, `descripcion`, `fecha_cierre`, `comentario_cierre`, `created_at`, `updated_at`, `deleted_at`, `sucursal_id`) VALUES
(3, 'TAREA 1', '', '2016-06-28 03:00:00', '', '2016-06-27 03:00:00', NULL, NULL, 1),
(4, 'TAREA 2', '', '2016-06-30 03:00:00', 'prueba comentario', '2016-06-27 03:00:00', NULL, NULL, 1),
(6, 'TAREA 3', '', '2016-06-29 03:00:00', '', '2016-06-27 03:00:00', NULL, NULL, 1),
(7, 'prueba', 'prueba', NULL, '', '2016-07-01 18:30:43', '2016-07-01 18:30:43', NULL, 1),
(8, 'test222', 'lala', NULL, '', '2016-07-13 20:30:54', '2016-07-13 20:30:54', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_documentos`
--

CREATE TABLE `tipos_documentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `tipos_documentos`
--

TRUNCATE TABLE `tipos_documentos`;
--
-- Volcado de datos para la tabla `tipos_documentos`
--

INSERT INTO `tipos_documentos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Circular', NULL, NULL, NULL),
(2, 'Proceso', NULL, NULL, NULL),
(3, 'Novedad', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `archivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `es_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=comun; 1=admin',
  `es_super_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=comun; 1=super_admin',
  `empresa_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `users`
--

TRUNCATE TABLE `users`;
--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre`, `apellido`, `email`, `password`, `archivo`, `telefono`, `remember_token`, `es_admin`, `es_super_admin`, `empresa_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(21, 'ale', 'ponzo', 'aleponzo1@gmail.com', '$2y$10$hcbK6Mekp2R3ajMbfP1Ct.eRBmyIfW6kOXabFo3PjRRTHhnDZqQsq', '', '', 'zfxpWiV5sAQKUBccNzAdobxQbZXzDfLVfWiuWNNRgH3vSs0jAGQdEyvmhUYV', 0, 1, 0, NULL, '2016-06-13 15:05:48', NULL),
(22, 'Julian', 'Marino', 'juli@gmail.com', '$2y$10$.NHXoXALoPvZL2IlvaRSLuFQgul728wTGryP4171IW0YVG.VCGnca', '', '', '1dBbw6mjQIAcAZ5zMrtLzufEfalONl0dthbO2WiGdOEX5HZZtCMfvnOE8GV0', 1, 0, 0, '2016-06-10 17:08:07', '2016-06-15 17:22:22', NULL),
(23, 'Juan', 'Riquelme', 'juan@riquelme.com', '$2y$10$HNBOirZx1kYMLfyS0I.E5u0ayNPNFHMnX2FYKo1xqHhiOaZ.3nn7K', '', 'superadmin@doublepoint.com.ar', 'FsluHu9RNmlIYcpF7Hv1frtFWrRUaEBA7QtCMZz4Fxuz2dNa2dXsOt9Le8nl', 0, 0, 1, '2016-06-10 18:10:14', '2016-07-13 16:53:08', NULL),
(32, 'super', 'admin', 'superadmin@doublepoint.com.ar', '$2y$10$1j3xVBjgBQXNT4Vwt.OvQeKjWvmgkX1pc0qou4Xm0V8hdBO6jDcGS', '', '', 'WIQsi1uM7R99wb9l084pliFAaACPGNOzhLIUEBy03esGBkpiuoXmNe2YxxLB', 1, 1, 1, '2016-06-13 15:03:51', '2016-07-13 16:19:21', NULL),
(33, 'admin', 'adminn', 'admin@admin.com', '$2y$10$uBiLKsYnt9CohhhISODcIuyl/rZdodVcKk9yVDcObKMdiq3sN.rUe', '', '', NULL, 0, 1, 0, '2016-06-20 22:07:04', '2016-06-20 22:07:04', NULL),
(42, 'Roberto', 'Abondanzieri', 'rob@ab.com', '$2y$10$FgUNFvfv2z02U3Tq/PfbeurNcg/tY4Kn4GSalmP8/pVm5Fm4ljADe', '', '', NULL, 0, 0, 0, '2016-06-20 22:36:18', '2016-06-20 22:36:18', NULL),
(43, 'Mario Ignacio', 'di Luca', 'midiluca@gmail.com', '$2y$10$C0IkvdTdgLxdzz03DJ52E.UAxpX4NR4vY0lRYiODRZqn9Gxe0IkRC', 'uploads/archivos/654348_1466462469_.jpg', '', 'r8fGBxzCcMSbYC4b9cghYXpWRgBozt5zIOE5Rx4VsqjZOKIZlpNPjGTqYxZO', 0, 0, 0, '2016-06-21 01:41:09', '2016-06-21 17:24:56', NULL),
(44, 'Master', 'Cheff', 'mastercheff@doublepoint.com.ar', '$2y$10$bjvAkicEdbNpqr22GHEGFeIn8R9GPiX5P7djO10NkwYHNtl2QdDra', 'uploads/archivos/631851_1466508573_.jpg', '', NULL, 0, 0, 0, '2016-06-21 14:29:32', '2016-06-21 14:29:33', NULL),
(45, 'Mario', 'Mario', 'mario_diluca@hotmail.com', '$2y$10$OGrPA8NvG/uZ4Dw/ayeK2uvLpAK5hblgC8PIA09UWFKCpK4wa9p2m', 'uploads/archivos/868761_1466519162_.jpg', '', 'PNo0gAeaooTyV8Eo8TkzbT8tMWUEthTRE14qc6Bp9otZygG9w6R2CoQiiK7o', 1, 0, 0, '2016-06-21 17:26:01', '2016-06-21 17:28:11', NULL),
(46, 'Mario', 'DP', 'mariodiluca@doublepoint.com.ar', '$2y$10$0dSZCiKquDT4WHapCBr8x.VRsh9O3/Ip5frF3Q9Ee.PwjHpH5zF1K', 'uploads/archivos/758309_1466519287_.jpg', '', NULL, 0, 0, 0, '2016-06-21 17:28:07', '2016-06-21 17:28:07', NULL),
(47, 'nombreee', 'apellidooo', 'nombre@apellido.com', '$2y$10$hfQQSlO2.UPKjisGNY9ko.Rh/rXwb9m2iTfV1HRgY/sfMqSdLPzeS', 'uploads/archivos/793972_1466628739_.jpg', '', NULL, 0, 0, 1, '2016-06-22 23:52:19', '2016-06-22 23:52:19', NULL),
(49, 'capo', 'google', 'capo@google.com', '$2y$10$tF4wKWCmmuM/6hc.Rhx0Pul5dBcdR2DNSDNrnsIOfn4/62BG0i.im', '', '', NULL, 0, 0, 2, '2016-07-13 17:00:02', '2016-07-13 17:00:02', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `actividades_sucursal_id_foreign` (`sucursal_id`),
  ADD KEY `manager_log_id` (`manager_log_id`);

--
-- Indices de la tabla `archivos`
--
ALTER TABLE `archivos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documento_id` (`documento_id`);

--
-- Indices de la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargos_padre_id_foreign` (`padre_id`),
  ADD KEY `empresa_id` (`empresa_id`);

--
-- Indices de la tabla `cargo_sucursal_usuario`
--
ALTER TABLE `cargo_sucursal_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargo_usuario_usuario_id_foreign` (`usuario_id`),
  ADD KEY `cargo_usuario_cargo_id_foreign` (`cargo_id`),
  ADD KEY `cargo_usuario_sucursal_id_foreign` (`sucursal_id`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documentos_tipo_documento_id_foreign` (`tipo_documento_id`),
  ADD KEY `user_aprobador_id` (`user_aprobador_id`),
  ADD KEY `autor_id` (`autor_id`),
  ADD KEY `padre_id` (`padre_id`),
  ADD KEY `estado_id` (`estado_id`),
  ADD KEY `version_anterior` (`version_anterior`),
  ADD KEY `empresa_id` (`empresa_id`);

--
-- Indices de la tabla `documento_lectura`
--
ALTER TABLE `documento_lectura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documento_lectura_documento_id_foreign` (`documento_id`),
  ADD KEY `documento_lectura_usuario_id_foreign` (`usuario_id`),
  ADD KEY `sucursal_id` (`sucursal_id`),
  ADD KEY `cargo_id` (`cargo_id`);

--
-- Indices de la tabla `documento_sucursal`
--
ALTER TABLE `documento_sucursal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documento_sucursal_documento_id_foreign` (`documento_id`),
  ADD KEY `documento_sucursal_usuario_id_foreign` (`usuario_id`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estados_documentos`
--
ALTER TABLE `estados_documentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logs_sucursal_id_foreign` (`sucursal_id`);

--
-- Indices de la tabla `manager_logs`
--
ALTER TABLE `manager_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sucursal_id` (`sucursal_id`),
  ADD KEY `empresa_id` (`empresa_id`),
  ADD KEY `caja_apertura` (`caja_apertura`),
  ADD KEY `caja_mediodia` (`caja_mediodia`),
  ADD KEY `caja_cierre` (`caja_cierre`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_id` (`empresa_id`);

--
-- Indices de la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tareas_sucursal_id_foreign` (`sucursal_id`);

--
-- Indices de la tabla `tipos_documentos`
--
ALTER TABLE `tipos_documentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `empresa_id` (`empresa_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividades`
--
ALTER TABLE `actividades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `archivos`
--
ALTER TABLE `archivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `cargo_sucursal_usuario`
--
ALTER TABLE `cargo_sucursal_usuario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `documento_lectura`
--
ALTER TABLE `documento_lectura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT de la tabla `documento_sucursal`
--
ALTER TABLE `documento_sucursal`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `estados_documentos`
--
ALTER TABLE `estados_documentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `manager_logs`
--
ALTER TABLE `manager_logs`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `tareas`
--
ALTER TABLE `tareas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `tipos_documentos`
--
ALTER TABLE `tipos_documentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD CONSTRAINT `actividades_manager_log_idforeign` FOREIGN KEY (`manager_log_id`) REFERENCES `manager_logs` (`id`),
  ADD CONSTRAINT `actividades_sucursal_id_foreign` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`id`);

--
-- Filtros para la tabla `archivos`
--
ALTER TABLE `archivos`
  ADD CONSTRAINT `archivos_documento_id_foreign` FOREIGN KEY (`documento_id`) REFERENCES `documentos` (`id`);

--
-- Filtros para la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD CONSTRAINT `cargos_empresa_id_foreign` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cargos_padre_id_foreign` FOREIGN KEY (`padre_id`) REFERENCES `cargos` (`id`);

--
-- Filtros para la tabla `cargo_sucursal_usuario`
--
ALTER TABLE `cargo_sucursal_usuario`
  ADD CONSTRAINT `cargo_usuario_cargo_id_foreign` FOREIGN KEY (`cargo_id`) REFERENCES `cargos` (`id`),
  ADD CONSTRAINT `cargo_usuario_sucursal_id_foreign` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`id`),
  ADD CONSTRAINT `cargo_usuario_usuario_id_foreign` FOREIGN KEY (`usuario_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD CONSTRAINT `documentos_autor_id_foreign` FOREIGN KEY (`autor_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `documentos_empresa_id_foreign` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `documentos_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados_documentos` (`id`),
  ADD CONSTRAINT `documentos_padre_id_foreign` FOREIGN KEY (`padre_id`) REFERENCES `documentos` (`id`),
  ADD CONSTRAINT `documentos_tipo_documento_id_foreign` FOREIGN KEY (`tipo_documento_id`) REFERENCES `tipos_documentos` (`id`),
  ADD CONSTRAINT `documentos_user_aprobador_id_foreign` FOREIGN KEY (`user_aprobador_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `documentos_version_anterior_foreign` FOREIGN KEY (`version_anterior`) REFERENCES `documentos` (`id`);

--
-- Filtros para la tabla `documento_lectura`
--
ALTER TABLE `documento_lectura`
  ADD CONSTRAINT `documento_lectura_cargo_id_foreign` FOREIGN KEY (`cargo_id`) REFERENCES `cargos` (`id`),
  ADD CONSTRAINT `documento_lectura_documento_id_foreign` FOREIGN KEY (`documento_id`) REFERENCES `documentos` (`id`),
  ADD CONSTRAINT `documento_lectura_sucursal_id_foreign` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`id`),
  ADD CONSTRAINT `documento_lectura_usuario_id_foreign` FOREIGN KEY (`usuario_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `documento_sucursal`
--
ALTER TABLE `documento_sucursal`
  ADD CONSTRAINT `documento_sucursal_documento_id_foreign` FOREIGN KEY (`documento_id`) REFERENCES `documentos` (`id`),
  ADD CONSTRAINT `documento_sucursal_usuario_id_foreign` FOREIGN KEY (`usuario_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_sucursal_id_foreign` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`id`);

--
-- Filtros para la tabla `manager_logs`
--
ALTER TABLE `manager_logs`
  ADD CONSTRAINT `manager_logs_empresa_id_foreign` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `manager_logs_sucursal_id_foreign` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`id`);

--
-- Filtros para la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD CONSTRAINT `sucursales_empresa_id_foreign` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD CONSTRAINT `tareas_sucursal_id_foreign` FOREIGN KEY (`sucursal_id`) REFERENCES `sucursales` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_empresa_id_foreign` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
