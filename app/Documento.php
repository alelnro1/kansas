<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento extends Model {

	protected $table = 'documentos';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = [
        'tags', 'autor_id', 'nombre', 'descripcion', 'archivo', 'version', 'version_anterior',
        'tipo_documento_id', 'user_aprobador_id', 'estado_id', 'empresa_id', 'nro_identificador'
    ];

	/* Mutators */
	public function setUserAprobadorIdAttribute($value) {
		$this->attributes['user_aprobador_id'] = $value ?: null;
	}

	public function setTipoDocumentoIdAttribute($value) {
		$this->attributes['tipo_documento_id'] = $value ?: null;
	}

	public function setVersionAnteriorAttribute($value) {
		$this->attributes['version_anterior'] = $value ?: null;
	}

	public function Usuarios()
	{
		return $this->belongsToMany('User');
	}

	public function Sucursal()
	{
		return $this->belongsToMany(Sucursal::class, 'documento_sucursal', 'documento_id', 'sucursal_id');
	}

	public function DocumentosPadres()
	{
		return $this->hasMany(Documento::class, 'id', 'version_anterior');
	}

	public function Tipo()
	{
		return $this->belongsTo(TipoDocumento::class, 'tipo_documento_id');
	}

	public function UsuarioAprobador()
	{
		return $this->hasOne(User::class, 'id', 'user_aprobador_id');
	}

	public function Autor()
	{
		return $this->hasOne(User::class, 'user_id', 'autor_id');
	}

	public function Estado()
	{
		return $this->belongsTo(EstadoDocumento::class, 'estado_id');
	}

	public function Archivos()
	{
		return $this->hasMany(Archivo::class);
	}

	public function Destinatarios()
	{
		return $this->hasMany(DocumentoLectura::class, 'documento_id', 'id');
	}

	public function Empresa()
	{
		return $this->belongsTo(Empresa::class);
	}

}