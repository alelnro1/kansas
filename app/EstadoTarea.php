<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoTarea extends Model {

	protected $table = 'estados_tareas';
	public $timestamps = true;

	public function Tareas()
	{
		return $this->hasMany('Tarea');
	}

}