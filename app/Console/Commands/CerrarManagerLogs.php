<?php

namespace App\Console\Commands;

use App\ManagerLog;
use Illuminate\Console\Command;

class CerrarManagerLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cerrar:managerlogs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cierre de los Manager Logs abiertos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $manager_logs_abiertos = ManagerLog::where('cerrado', 0)->get();
        $this->info('Encontre los siguientes manager logs abiertos: ' . $manager_logs_abiertos);

        foreach ($manager_logs_abiertos as $ml) {
            $ml->cerrado = true;
            $ml->fecha_cierre = date("Y-m-d H:i:s", time());

            $ml->save();

            $this->info('Cierro el manager log: ' . $ml->id);
        }
    }
}
