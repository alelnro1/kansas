<?php

namespace App\Console;

use App\ManagerLog;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CerrarManagerLogs::class
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Cierro el manager log de todas las empresas y sucursales a las 5AM
        $schedule
            ->call(function() {
                $manager_logs_abiertos = ManagerLog::where('cerrado', 0)->get();

                foreach ($manager_logs_abiertos as $ml) {
                    $ml->cerrado = true;
                    $ml->fecha_cierre = date("Y-m-d H:i:s", time());

                    $ml->save();
                }
            })
            ->dailyAt('05:00');
    }
}
