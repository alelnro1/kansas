<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;

class User extends Authenticatable
{
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellido', 'email', 'password', 'telefono', 'archivo', 'empresa_id', 'solo_tareas'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Recorro todos los cargos del usuario, y si hay uno que puede ver el manager log => devuelvo true
     */
    public function puedeVerManagerLog()
    {
        $cargos_sucursales = CargoSucursalUsuario::where('usuario_id', $this->id)->select(['cargo_id'])->get();

        $cargos_sucursales->load(['cargos' => function($query){
            $query->select(['id', 'manager_log']);
        }]);

        foreach($cargos_sucursales as $cargo_sucursal) {

            foreach ($cargo_sucursal->cargos as $cargo) {
                if ($cargo->manager_log == '1') {
                    return true;
                }
            }
        }
    }

    public function tieneDocumentosNuevos()
    {
        return ($this->cantidadDocumentosParaAprobar() > 0 || $this->cantidadDocumentosParaLeer() > 0);
    }

    public function cantidadDocumentosParaAprobar()
    {
        return Documento::where('user_aprobador_id', $this->id)
            ->whereHas(
                'estado', function($query) {
                    $query->where('nombre', 'Pendiente de Aprobación');
                }
            )
            ->count();
    }

    private function cantidadDocumentosParaLeer()
    {
        return Documento::where('autor_id', '<>', $this->id)
            ->whereHas('destinatarios', function($query) {
                $query->where('usuario_id', $this->id);
                $query->where('leido', false);
            })
            ->whereHas('estado', function($query){
                $query->where('nombre', 'Publicado');
            })
            ->count();
    }

    public function nombreEmpresaUsuario() {
        $empresa_usuario = Empresa::where('id', $this->empresa_id)->select(['id', 'razon_social'])->first();

        $razon_social = "Documentos";

        if (isset($empresa_usuario->razon_social))
            $razon_social = $empresa_usuario->razon_social;

        return $razon_social;
    }

    public function logoEmpresaUsuario() {
        $empresa_usuario = Empresa::where('id', $this->empresa_id)->select(['id', 'archivo'])->first();

        $archivo = "";

        if (isset($empresa_usuario->archivo))
            $archivo = $empresa_usuario->archivo;

        return $archivo;
    }

    public function esSuperAdmin()
    {
        if ($this->es_super_admin == 1)
            return true;
        else
            return false;
    }

    public function esAdministrador()
    {
        return $this->es_admin;
    }

    public function esUserSoloTareas()
    {
        return $this->solo_tareas;
    }

    // Relaciones
    public function Documentos()
    {
        return $this->hasMany(Documento::class, 'autor_id');
    }

    public function Sucursales()
    {
        return $this->belongsToMany(Sucursal::class, 'cargo_sucursal_usuario', 'usuario_id', 'sucursal_id')
            ->withPivot('cargo_id');
            //->where('empresa_id', $this->empresa_id);
    }

    public function Cargos()
    {
        return $this->belongsToMany(Cargo::class, 'cargo_sucursal_usuario', 'usuario_id', 'cargo_id')
            ->withPivot('sucursal_id');
            //->where('empresa_id', $this->empresa_id);
    }

    public function Empresa()
    {
        return $this->belongsTo(Empresa::class);
    }
}
