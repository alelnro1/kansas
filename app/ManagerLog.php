<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagerLog extends Model
{
    protected $table = 'manager_logs';

    public $fillable = [
        'sucursal_id', 'cerrado', 'empresa_id',
        'caja_apertura', 'manager_apertura',
        'caja_mediodia', 'manager_mediodia',
        'caja_cierre', 'manager_cierre',
        'descuento_empleados_30_neto',
        'descuento_empleados_100_neto',
        'rrpp_mgr_bebidas_neto',
        'rrpp_malo_cocina_neto',
        'rrpp_malo_recepcion_neto',
        'rrpp_malo_scv_neto',
        'rrpp_malo_valet_neto',
        'rrpp_malo_cliente_neto',
        'rrpp_malo_expo_neto',
        'rrpp_malo_mozo_neto',
        'ventas', 'guest',  'guest_avg', 'proyectado', 'cambio', 'prosegur'
    ];

    /* Relaciones */
    public function Actividades()
    {
        return $this->hasMany(Actividad::class);
    }

    public function Sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    public function Empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function UserCajaApertura()
    {
        return $this->hasOne(User::class, 'id', 'caja_apertura');
    }

    public function UserCajaMediodia()
    {
        return $this->hasOne(User::class, 'id', 'caja_mediodia');
    }

    public function UserCajaCierre()
    {
        return $this->hasOne(User::class, 'id', 'caja_cierre');
    }

    public function Denominaciones()
    {
        return $this->belongsToMany(Denominacion::class, 'denominacion_manager_log', 'manager_log_id', 'denominacion_id')
            ->withPivot('cantidad');
    }

}
