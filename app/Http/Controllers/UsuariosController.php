<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\DocumentoLectura;
use App\Sucursal;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\CargoSucursalUsuario;
use Illuminate\Support\Facades\Validator;
use App\Documento;
use Illuminate\Support\Facades\Hash;
use App\Empresa;

class UsuariosController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Si el usuario actual no es super admin => no puede ver usuarios super admins
        if (!Auth::user()->esSuperAdmin()) {
            $usuarios = User::where('es_super_admin', '<>', '1')->where('empresa_id', Auth::user()->empresa_id);
        } else {
            $usuarios = User::where('es_admin', true);
        }

        // Cargo todos los que no sean el usuario actual
        $usuarios = $usuarios->where('id', '<>', Auth::user()->id)->get();

        // Si es un admin, cargamos las empresas. Sino cargamos las sucursales y cargos
        if (Auth::user()->esAdministrador()) {
            $usuarios->load('empresa');
        } else {
            $usuarios->load('sucursales');
            $usuarios->load('cargos');
        }

        return view('usuarios.listar', array('usuarios' => $usuarios));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Seteo las variables
        $empresas = $cargos = $sucursales = null;

        // Si es un admin, cargamos las empresas para que seleccione. Sino cargamos las sucursales y cargos
        if (Auth::user()->esAdministrador()) {
            $cargos = Cargo::where('empresa_id', Auth::user()->empresa_id)->get();
            $sucursales = Sucursal::where('empresa_id', Auth::user()->empresa_id)->get();
        } else {
            $empresas = Empresa::all();
        }

        return view('usuarios.create', array('cargos' => $cargos, 'sucursales' => $sucursales, 'empresas' => $empresas));
    }

    /*private function getCargos()
    {
        if (Auth::user()->esSuperAdmin() ){ // Esta viendo el sitio un super admin (Mariano)
            $cargos = Cargo::all();
        } else if(Auth::user()->esAdministrador()) { // Solo puede crear de admins para abajo
            $cargos = Cargo::->get();
        } else {re
            $cargos = Cargo::where('nombre', '<>', 'super admin')->where('nombre', '<>', 'administrador')->get();
        }

        return $cargos;
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtengo la empresa, si es super admin va a ser obligatoria en el request, sino se deduce de quien está logueado
        $request->empresa_id = $this->obtenerEmpresa($request);

        $validator = $this->validarUsuario($request);

        if ($validator->fails()) {
            return redirect('usuarios/create')
                ->withErrors($validator)
                ->withInput();
        }

        if (isset($request->solo_tareas) && $request->solo_tareas == "on") {
            $solo_tareas = true;
        } else {
            $solo_tareas = false;
        }

        // Creo el usuario
        $usuario = User::create([
            'nombre'      => $request->nombre,
            'apellido'    => $request->apellido,
            'email'       => $request->email,
            'password'    => bcrypt($request->password),
            'empresa_id'  => $request->empresa_id,
            'solo_tareas' => $solo_tareas
        ]);

        // Si se subio una foto para el usuario => subirla y guardarla
        $this->guardarFotoSiHay($request, $usuario);

        // Si el usuario a crear es un admin, no necesito adjuntarlo a una sucursal, solo le tengo que poner el flag en la tabla usuarios
        //   Si el que crea es un super admin => está creando admins
        if ($request->es_admin == "on" || Auth::user()->esSuperAdmin()) {
            $usuario->es_admin = true;
            $usuario->save();
        }

        if (isset($request->sucursales_cargos) && $request->sucursales_cargos != "") {
            $this->vincularCargosDeSucursalesConUsuario($usuario, $request->sucursales_cargos);
        }

        // Buscar documentos ya creados, y si hay ofrecerle crear el registro en documento_lectura
        $this->vincularDocumentosYaCreadosParaEseCargoYSucursal($usuario);

        return redirect('/usuarios');
    }

    private function vincularDocumentosYaCreadosParaEseCargoYSucursal($usuario)
    {
        // Obtengo los cargos y sucursales del usuario
        $cargos     = $usuario->cargos;
        $sucursales = $usuario->sucursales;

        // Recorro todos los cargos del usuario
        foreach ($cargos as $cargo) {
            // Busco los destinatarios con cargo igual al del usuario
            $documentos_con_cargo = DocumentoLectura::where('cargo_id', $cargo->id)->get();

            // Si hay destinatarios con el mismo cargo, filtro los que tengan la misma sucursal
            if (count($documentos_con_cargo) > 0) {
                // Recorro las sucursales del usuario
                foreach ($sucursales as $sucursal) {

                    foreach ($documentos_con_cargo as $documento_lectura) {
                        // De todos los documentos con el mismo cargo del usuario, quiero los que tambien tienen la misma sucursal
                        if ($sucursal->id == $documento_lectura->sucursal_id) {
                            $cant_documentos_lecturas = DocumentoLectura::where('documento_id', $documento_lectura->documento_id)
                                ->where('usuario_id', $usuario->id)
                                ->where('cargo_id', $documento_lectura->cargo_id)
                                ->where('sucursal_id', $documento_lectura->sucursal_id)
                                ->count();

                            // Verifico que el registro no exista
                            if ($cant_documentos_lecturas <= 0) {
                                // Creo el registro
                                DocumentoLectura::create([
                                    'documento_id' => $documento_lectura->documento_id,
                                    'usuario_id' => $usuario->id,
                                    'cargo_id' => $documento_lectura->cargo_id,
                                    'sucursal_id' => $documento_lectura->sucursal_id,
                                    'leido' => '0'
                                ]);
                            }
                        }
                    }
                };
            }
        }
    }

    /**
     * Asignamos la empresa del usuario que estamos creando/editando
     * @param $request
     * @return null
     */
    private function obtenerEmpresa($request)
    {
        // Seteo la variable
        $empresa_id = null;

        // Si en el request nos viene una empresa => un super admin está tocando usuarios
        if (isset($request->empresa_id) && $request->empresa_id != "")
        {
            $empresa_id = $request->empresa_id;
        }

        // Si en el request nos viene la empresa_id vacia (o no existe) => un usuario admin está creando un usuario
        else
        {
            // La empresa que pertenece el usuario que esta logueado será la del nuevo usuario
            $empresa_id = Auth::user()->empresa_id;
        }

        return $empresa_id;
    }

    /**
     * Si hay una foto, la sube y guarda la referencia en la base de datos
     * @param $request
     * @param $usuario
     */
    private function guardarFotoSiHay($request, $usuario)
    {
        if (isset($request->files) && count($request->files) > 0) {
            $archivo = $this->subirArchivo($request);

            $usuario->archivo = $archivo;
            $usuario->save();
        }
    }

    private function validarUsuario(Request $request, $usuario=null)
    {
        // Si el usuario existe, lo estoy editando y quiero saber su id
        (!$usuario) ? $id = "NULL" : $id = $usuario->id;

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $id,
            'telefono' => 'max:60',
            'archivo'  => 'max:5000|mimes:jpg,jpeg,png'
            //'sucursal_id' => 'required_without:es_super_admin,on|required_without:es_admin,on',
        ]);

        // Si no es admin => tiene que haber elegido una sucursal y un cargo. Ademas, si escribio una contraseña, tiene que estar confirmada
        $validator->after(function($validator) use ($request, $usuario) {

            // No es admin y el usuario NO tiene sucursales (y el usuario que crea/edita no es super admin)
            if ( ($request->es_super_admin != "on" && $request->es_admin != "on" && !Auth::user()->esSuperAdmin())) {
                if ($request->sucursales_cargos == "") {
                    $validator->errors()->add('sucursal_id', 'Debe seleccionar una sucursal para el usuario');
                }
            }
        });

        // Si el usuario que crea/edita es un admin, necesita seleccionar sucursal y cargos
        $validator->sometimes('sucursal_id', 'required_without:es_super_admin,on|required_without:es_admin,on', function(){
            if (!Auth::user()->esSuperAdmin()){
                return true;
            }

            return false;
        });

        // Si escribio una contraseña y no hay un usuario (o sea se esta creando uno) => validarla
        $validator->sometimes('password', 'required|min:6|confirmed', function($input) use ($usuario){
            if ($usuario != null && $input->password != ""){
                return true;
            }

            if ($usuario == null){
                return true;
            }
        });

        return $validator;
    }

    private function vincularCargosDeSucursalesConUsuario($usuario, $sucursales_cargos){
        $sucursales_cargos = explode("||", $sucursales_cargos);

        foreach ($sucursales_cargos as $sucursal_cargo){
            $sucursal_cargo = explode(",", $sucursal_cargo);

            //Validar sucursal y cargo
            $sucursal = Sucursal::find($sucursal_cargo[0]);
            $cargo = Cargo::find($sucursal_cargo[1]);

            if ($sucursal && $cargo) {

                //Si existe la sucursal y el cargo, creo la nueva relacion
                $cargo_sucursal = new CargoSucursalUsuario();
                $cargo_sucursal->sucursal_id = $sucursal->id;
                $cargo_sucursal->cargo_id = $cargo->id;
                $cargo_sucursal->usuario_id = $usuario->id;

                $cargo_sucursal->save();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::findOrFail($id);
        $usuario->load('documentos');
        $usuario->load('sucursales');
        $usuario->load('cargos');

        return view('usuarios.show', array('usuario' => $usuario));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::findOrFail($id);
        $usuario->load('sucursales');
        $usuario->load('cargos');
        $usuario->load('empresa');

        // Seteo las variables
        $empresas = $cargos = $sucursales = null;

        // Si es un admin, cargamos las empresas para que seleccione. Sino cargamos las sucursales y cargos
        if (Auth::user()->esAdministrador()) {
            $cargos = Cargo::where('empresa_id', Auth::user()->empresa_id)->get();
            $sucursales = Sucursal::where('empresa_id', Auth::user()->empresa_id)->get();
        } else {
            $empresas = Empresa::all();
        }

        // Busco todas las sucursales y sus cargos del usuario a editar, y limito en las tablas relacionadas la busqueda
        $sucursales_cargos = CargoSucursalUsuario::with([
            'sucursales' => function($query){
                $query->select('id', 'nombre');
            },
            'cargos' => function($query){
                $query->select('id', 'nombre');
            },
        ])->where('usuario_id', $usuario->id)->select(['sucursal_id', 'cargo_id'])->get();

        return view('usuarios.edit', array('usuario' => $usuario, 'cargos' => $cargos, 'sucursales' => $sucursales,
            'sucursales_cargos' => $sucursales_cargos, 'empresa' => $usuario->empresa, 'empresas' => $empresas));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Busco el usuario, si no existe exploto
        $usuario = User::findOrFail($id);

        // Obtengo la empresa, si es super admin va a ser obligatoria en el request, sino se deduce de quien está logueado
        $request->empresa_id = $this->obtenerEmpresa($request);

        // Valido al usuario
        $validator = $this->validarUsuario($request, $usuario);

        if ($validator->fails()) {
            return redirect('usuarios/' . $usuario->id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }

        // Si se subio una foto para el usuario => subirla y guardarla
        $this->guardarFotoSiHay($request, $usuario);

        // Actualizar el usuario
        $usuario->nombre     = $request->nombre;
        $usuario->apellido   = $request->apellido;
        $usuario->email      = $request->email;
        $usuario->telefono   = $request->telefono;
        $usuario->empresa_id = $request->empresa_id;

        if ($request->es_super_admin == "on") {
            $usuario->es_super_admin = true;
        } else {
            $usuario->es_super_admin = false;
        }

        if($request->es_admin == "on") {
            $usuario->es_admin = true;
        } else {
            $usuario->es_admin = false;
        }

        if (isset($request->solo_tareas) && $request->solo_tareas == "on") {
            $usuario->solo_tareas = true;
        } else {
            $usuario->solo_tareas = false;
        }

        // Si hay una contraseña, la actualizo
        if ($request->password != "") {
            $usuario->password = Hash::make($request->password);
        }

        // Guardo en la base de datos los cambios
        $usuario->save();

        // Elimino las sucursales del usuario para poder crearlas de nuevo despues con las que vienen
        $usuario->Cargos()->detach();

        // Vinculo los nuevos cargos del usuario
        if (isset($request->sucursales_cargos) && $request->sucursales_cargos != "") {
            $this->vincularCargosDeSucursalesConUsuario($usuario, $request->sucursales_cargos);
        }

        // Buscar documentos ya creados, y si hay ofrecerle crear el registro en documento_lectura
        $this->vincularDocumentosYaCreadosParaEseCargoYSucursal($usuario);

        return redirect('/usuarios/')->with('usuario_actualizado', 'El usuario ' . $usuario->email . ' ha sido actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::findOrFail($id);

        $email = $usuario->email;

        // Actualizo el email para que no rompa si quiere crear otro nuevo con el mismo email
        $usuario->email = "'" . $email;
        $usuario->save();

        $usuario->delete();

        return redirect('/usuarios/')->with('usuario_eliminado', 'Usuario con nombre ' . $usuario->nombre . ' eliminado');
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }
}
