<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        if (Auth::user()) {
            $solo_tareas = session('USER_SOLO_TAREAS');

            if ($solo_tareas === null && Auth::user()) {
                $user = User::where('id', Auth::user()->id)->select(['id', 'solo_tareas'])->first();

                session(['USER_SOLO_TAREAS' => $user->solo_tareas]);
            }

        }
    }
}
