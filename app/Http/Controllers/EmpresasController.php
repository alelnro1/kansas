<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Empresa;
use Illuminate\Support\Facades\Validator;

class EmpresasController extends Controller
{
    use SoftDeletes;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::all();
        return view('empresas.listar')->with('empresas', $empresas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarEmpresa($request);

        $empresa = Empresa::create($request->all());

        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            $empresa->archivo = $archivo;
            $empresa->save();
        }

        return redirect('/empresas/')->with('empresa_creado', 'Empresa con razón social ' . $request->nombre . ' creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresa::findOrFail($id);
        return view('empresas.show')->with('empresa', $empresa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = Empresa::findOrFail($id);
        return view('empresas.edit')->with('empresa', $empresa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'razon_social'  => 'required|max:100|unique:empresas,id,' . $id,
            'cuit'          => 'required|max:100|unique:empresas,id,' . $id,
            'domicilio'     => 'required'
        ]);

        if ($validator->fails())
            return redirect('empresas/' . $id . '/edit')->withErrors($validator)->withInput();

        $empresa = Empresa::findOrFail($id);

        $empresa->update($request->all());

        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            $empresa->archivo = $archivo;
            $empresa->save();
        }

        return redirect('/empresas/')->with('empresa_actualizado', 'Empresa actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = Empresa::findOrFail($id);

        $empresa->delete();

        return redirect('/empresas/')->with('empresa_eliminado', 'Empresa con nombre ' . $empresa->nombre . ' eliminado');
    }

    private function validarEmpresa($request)
    {
        $this->validate($request, [
            'razon_social'  => 'required|max:100|unique:empresas',
            'cuit'          => 'required|max:100|unique:empresas,razon_social,' . $request->razon_social,
            'domicilio'     => 'required'
        ]);
    }

        /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

}