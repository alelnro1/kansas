<?php

namespace App\Http\Controllers;

use App\CargoSucursalUsuario;
use App\Denominacion;
use App\ManagerLog;
use App\Sucursal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

class ManagerLogController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Antes de hacer cualquier cosa, tiene que seleccionar la sucursal. De ahi se redirige a la ruta del ML de la sucursal
     */
    public function seleccionarSucursal()
    {
        $cargos_sucursales_usuarios = CargoSucursalUsuario::where('usuario_id', Auth::user()->id);

        // Si el usuario es solo tareas => no tiene que filtrar si el cargo que tiene puede ver el ML
        if (!Auth::user()->esUserSoloTareas()) {
            $cargos_sucursales_usuarios
                ->whereHas('cargos', function($query){
                    $query->where('manager_log', true);
                });
        }

        $cargos_sucursales_usuarios = $cargos_sucursales_usuarios->with(['cargos', 'sucursales'])->get();

        /*$cargos_sucursales_usuarios = $cargos_sucursales_usuarios->get();

        dump($cargos_sucursales_usuarios);

        $cargos_sucursales_usuarios->load('cargos', 'sucursales');*/

        /*Sucursal::with([
        // Quiero las sucursales con el usuario actual
        'users' => function($query){
            $query->where('users.id', '=', Auth::user()->id);
            $query->with('cargos');
            /*$query->whereHas('cargos', function($query){
                $query->where('manager_log', '1');
            });*/

        // Limito las columnas
        //$query->select(['users.id']);
        /*}
    ])
    ->whereHas('users', function($query){
        $query->where('users.id', '=', Auth::user()->id);
        $query->whereHas('cargos', function($query){
            $query->where('manager_log', '1');
        });
    })
    // Quiero solo la id y nombre de la sucursal
    ->select(['id', 'nombre'])
    ->get();*/

        return view('man-log.seleccionar-sucursal', array('cargos_sucursales_usuarios' => $cargos_sucursales_usuarios));
    }

    /**
     * Listo todos los managers logs, y si HOY no se creo uno, lo informo y le digo que lo cree
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Obtengo la sucursal que estoy mirando segun la url
        $sucursal_actual = $this->sucursalActual($request);

        // Obtengo todos los manager logs de esa sucursal
        $manager_logs =
            ManagerLog::where('sucursal_id', '=', $sucursal_actual->id)
                ->where('empresa_id', Auth::user()->empresa_id)
                ->orderBy('created_at', 'DESC')
                ->get();

        // Obtengo el manager log de hoy
        $manager_log_hoy = $this->obtenerManagerLogDeHoy($manager_logs);

        // Obtengo todos los manager logs de antes de hoy
        $manager_logs_historicos = $this->obtenerManagerLogsHistoricos($manager_logs);

        if ($manager_log_hoy) {
            // Obtengo las actividades, y tareas de la sucursal
            $manager_log = $manager_log_hoy;

            // Fecha de creacion del manager log de hoy
            $fecha_ml = $manager_log->created_at;

            // Fecha de cierre del manager log
            $fecha_cierre_ml = $manager_log->fecha_cierre;

            $manager_log->load([
                'actividades' => function($query) use($manager_log_hoy) {
                    $query->where('manager_log_id', '=', $manager_log_hoy->id);
                },
                'sucursal' => function ($query) use ($fecha_ml, $fecha_cierre_ml) {
                    $this->getTareas($query, $fecha_ml, $fecha_cierre_ml);
                },
                'Denominaciones',
                'UserCajaApertura',
                'UserCajaMediodia',
                'UserCajaCierre'
            ]);

            $actividades  = $manager_log->actividades;
            $tareas       = $manager_log->sucursal->tareas;
            $cuenta_plata = $manager_log->Denominaciones;
        } else {
            $actividades = $tareas = $cuenta_plata = null;
        }

        // Obtengo las denominaciones
        $denominaciones = Denominacion::where('empresa_id', Auth::user()->empresa_id)->get();

        $plata_en_caja = 0;
        // A las denominaciones le agrego la cantidad del manager log actual para mandarla a la vista
        if ($cuenta_plata) {
            foreach ($cuenta_plata as $key => $cp) {
                foreach ($denominaciones as $denominacion) {
                    if ($denominacion->id == $cp->id) {
                        $denominacion['cantidad'] = $cp->pivot->cantidad;

                        $plata_en_caja = $plata_en_caja + $cp->pivot->cantidad * $cp->valor;
                    }
                }
            }
        }

        // Busco los usuarios que pueden ver el manager log, de mi empresa
        $usuarios_con_ml = $this->cargarUsuariosConMLDeEmpresaActual();

        return view('man-log.index',
            array('sucursal' => $sucursal_actual, 'manager_log' => $manager_log_hoy,
                'manager_logs_historicos' => $manager_logs_historicos, 'actividades' => $actividades, 'tareas' => $tareas,
                'usuarios' => $usuarios_con_ml,
                'denominaciones' => $denominaciones,
                'cuenta_plata' => $cuenta_plata,
                'plata_en_caja' => $plata_en_caja));
    }

    public function actualizarCampos(Request $request, $id)
    {
        $manager_log = ManagerLog::where('sucursal_id', $id)->where('cerrado', '0')->where('empresa_id', Auth::user()->empresa_id)->first();

        $manager_log->update($request->all());

        return redirect('/man-log/' . $id)->with('campos_actualizados', 'Los campos fueron actualizados');
    }

    private function cargarUsuariosConMLDeEmpresaActual()
    {
        $usuarios = [];

        $usuarios_con_cargo =
            CargoSucursalUsuario::groupBy('usuario_id')
                ->whereHas('cargos', function($query) {
                    $query->where('manager_log', 1);
                })
                ->with(['usuarios' => function($query) {
                    $query->where('empresa_id', Auth::user()->empresa_id);
                    $query->select(['id', 'nombre', 'apellido']);
                }])
                ->get();

        foreach ($usuarios_con_cargo as $usuario_con_cargo) {
            foreach ($usuario_con_cargo->usuarios as $usuario) {
                array_push($usuarios, $usuario);
            }
        }

        return $usuarios;
    }

    private function getTareas($query, $fecha_ml, $fecha_cierre_ml)
    {
        return $query->with([
            'tareas' => function($query) use ($fecha_ml, $fecha_cierre_ml) {
                if ($fecha_cierre_ml) {
                    $query
                        ->where(function($query) use ($fecha_ml, $fecha_cierre_ml) {
                            $query->where('created_at', '<=', $fecha_cierre_ml)
                                    ->where('created_at', '>=', $fecha_ml);
                        })
                        ->orWhere(function($query) use ($fecha_ml, $fecha_cierre_ml) {
                            $query->where('fecha_cierre', '<=', $fecha_cierre_ml)
                                ->where('fecha_cierre', '>=', $fecha_ml);
                        })
                        ->orWhere(function($query) use ($fecha_ml, $fecha_cierre_ml) {
                            $query->where('created_at', '<=', $fecha_ml)
                                ->where('fecha_cierre', '>=', $fecha_cierre_ml);
                        })
                        ->orWhere(function($query) use ($fecha_ml, $fecha_cierre_ml) {
                            $query->where('created_at', '<=', $fecha_cierre_ml)
                                ->whereNull('fecha_cierre');
                        });
                } else {
                    $query->where('created_at', '<=', date("Y-m-d H:i:s", time()))
                        ->where(function($query) use ($fecha_ml) {
                            $query
                                ->whereNull('fecha_cierre')
                                ->orWhere('created_at', '>=', $fecha_ml)
                                ->orWhere('fecha_cierre', '>=', $fecha_ml);
                        });
                }
            }
        ]);
    }

    public function create($sucursal_id)
    {
        // Me fijo si el dia de hoy ya se abrio un manager log y si se cerro, lo re-abro
        if (!$this->yaExisteManagerLogDelDiaRebrir($sucursal_id)) {
            ManagerLog::create(['sucursal_id' => $sucursal_id, 'cerrado' => '0', 'empresa_id' => Auth::user()->empresa_id]);
        }
        return redirect('man-log/' . $sucursal_id);
    }

    /**
     * Busca los manager logs de la sucursal de la fecha actual. Si encuentra alguno, lo reabre
     * @param $sucursal_id
     * @return bool
     */
    private function yaExisteManagerLogDelDiaRebrir($sucursal_id)
    {
        $manager_log = ManagerLog::where('sucursal_id', $sucursal_id)->whereDate('created_at', '=', Carbon::today()->toDateString())->first();

        if (isset($manager_log) && $manager_log->count() > 0){
            $manager_log->fecha_cierre = null;
            $manager_log->cerrado = 0;

            $manager_log->save();

            return true;
        }
    }

    /**
     * Cierra el manager log que recibe (suele ser el del mismo dia)
     * @param $manager_log
     */
    public function cerrar($manager_log_id){
        $manager_log = ManagerLog::findOrFail($manager_log_id);

        $manager_log->fecha_cierre = Carbon::now()->toDateTimeString();
        $manager_log->cerrado = 1;

        $manager_log->save();

        return response()->json(['valid' => true]);
    }

    /**
     * Busca el manager log de hoy y lo devuelve. Si no existe, devuelve false
     * @param $manager_logs
     */
    private function obtenerManagerLogDeHoy($manager_logs)
    {
        return $manager_logs->filter(function($man_log){
            if (!$man_log->cerrado){
                return $man_log;
            }
        })->first();
    }

    /**
     * Busco los manager logs historicos (de antes de hoy)
     */
    private function obtenerManagerLogsHistoricos($manager_logs)
    {
        return $manager_logs->filter(function($man_log){
            if ($man_log->cerrado){
                return $man_log;
            }
        });
    }

    public function show(Request $request, $sucursal_id, $manager_log_id)
    {
        // Si el manager log al cual esta intentando ingresar, no corresponde a la sucursal actual => aborto con ERR 404
        if(!$this->managerLogCorrespondeASucursal($sucursal_id, $manager_log_id)) {
            abort(404);
        }

        $manager_log = ManagerLog::findOrFail($manager_log_id);
        $fecha_ml = $manager_log->created_at;
        $fecha_cierre_ml = $manager_log->fecha_cierre;

        // Si no es null, lo convierto
        if ($manager_log->fecha_cierre) {
            $fecha_cierre_ml = $manager_log->fecha_cierre;//->toDateString();
        }

        $manager_log->load([
            'actividades',
            'sucursal' => function($query) use ($fecha_ml, $fecha_cierre_ml) {
                $query->select(['id', 'nombre']);
                $this->getTareas($query, $fecha_ml, $fecha_cierre_ml);
            },
            'Denominaciones'
        ]);

        // Busco los usuarios que pueden ver el manager log, de mi empresa
        $usuarios_con_ml = $this->cargarUsuariosConMLDeEmpresaActual();

        $sucursal    = $manager_log->sucursal;
        $actividades = $manager_log->actividades;
        $tareas      = $sucursal->tareas;
        $denominaciones = $manager_log->Denominaciones;

        return view('man-log.show', array('manager_log' => $manager_log, 'sucursal' => $sucursal, 'tareas' => $tareas,
            'actividades' => $actividades, 'usuarios' => $usuarios_con_ml, 'denominaciones' => $denominaciones));
    }

    /**
     * Verifica que el manager log al cual estoy tratando de acceder, corresponde a la sucursal seleccionada
     * Parametro de seguridad
     */
    public function managerLogCorrespondeASucursal($sucursal_id, $manager_log_id)
    {
        return ManagerLog::where('id', '=', $manager_log_id)
            ->whereHas('sucursal', function($query) use($sucursal_id) {
                $query->where('id', '=', $sucursal_id);
            })->count();
    }


    /**
     * Obtengo la sucursal que estamos cambiando, segun la ruta
     * Con getPathInfo() obtengo algo asi:
     * array:4 [â–¼
    0 => ""
    1 => "man-log"
    2 => "1"
    3 => "create"
    ]
     */
    private function sucursalActual($request)
    {
        // Abro la ruta a la que quiero ingresar y obtengo el id de la sucursal
        $sucursal_actual_id = explode('/', $request->getPathInfo());

        // Busco los datos de la sucursal a la que accedÃ­
        $sucursal = Sucursal::where('id', '=', $sucursal_actual_id[2])->select(['id', 'nombre'])->first();

        return $sucursal;
    }

    /**
     * Se actualiza la plata en caja (cuenta plata) del manager log
     * @param Request $request
     */
    public function actualizarCuentaPlata(Request $request)
    {
        $manager_log = ManagerLog::where('id', $request->ml_act)->first();

        // Valido todas las denominaciones recibidas para ver que sean solo números enteros
        if (!$this->validarCuentaPlata($request)) {
            return redirect('/man-log/' . $manager_log->sucursal_id)
                ->with('valores_numericos', 'Los valores de la cuenta plata deben ser números enteros');
        }

        $manager_log->load('Denominaciones');

        // Busco la plata del manager log actual
        $cuenta_plata_ml = $manager_log->Denominaciones;

        // Recorro todos los valores recibidos por POST
        foreach ($request->all() as $key => $cantidad) {
            // Si el valor es _token o ml_act no me interesa
            if ($key == "_token" || $key == "ml_act") continue;

            // Elimino los _ que se agregan al enviar el formulario
            $key = str_replace("_", " ", $key);

            // Busco en la base la denominacion
            $denominacion = Denominacion::where('nombre', $key)->where('empresa_id', Auth::user()->empresa_id)->first();

            // Me fijo si la denominacion (billete) ya existe para el manager log actual
            if ($cuenta_plata_ml->contains($denominacion)) {
                // Existe => Lo busco y actualizo el valor
                foreach ($cuenta_plata_ml as $cp) {
                    if ($cp->id == $denominacion->id) {
                        $cp->pivot->cantidad = $cantidad;
                        $cp->pivot->save();
                    }
                }
            } else {
                // No existe => lo creo
                $denominacion->ManagerLogs()->attach($manager_log->id, ['cantidad' => $cantidad]);
            }
        }

        return redirect('/man-log/' . $manager_log->sucursal_id)->with('campos_actualizados', 'Los campos fueron actualizados');
    }

    /**
     * Se validan la cantidad de denominaciones (billetes) ingresados, deben ser todos numeros enteros
     * @param $request
     */
    private function validarCuentaPlata($request)
    {
        $valid = true;

        foreach ($request->all() as $key => $cantidad) {
            // Si el valor es _token o ml_act no me interesa
            if ($key == "_token" || $key == "ml_act") continue;

            // Si no es entero, no hacer nada y redirigir con un error
            if (!is_numeric($cantidad)) {
                $valid = false;

                // No es valido, salgo
                break;
            }
        }

        return $valid;
    }
}
