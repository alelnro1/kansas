<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Actividad;

class ActividadesController extends Controller
{
    use SoftDeletes;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actividades = Actividad::all();
        return view('actividades.listar')->with('actividades', $actividades);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($sucursal_id, $manager_log)
    {
        return view('actividades.create', array('sucursal_id' => $sucursal_id, 'manager_log' => $manager_log));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $sucursal_id, $manager_log_id)
    {
        $this->validarActividad($request);

        $request->request->add(['sucursal_id' => $sucursal_id, 'manager_log_id' => $manager_log_id]);

        Actividad::create($request->except(['_token']));

        return redirect('/man-log/' . $sucursal_id)->with('actividad_creado', 'Actividad con nombre ' . $request->nombre . ' creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actividad = Actividad::findOrFail($id);
        return view('actividades.show')->with('actividad', $actividad);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($man_log_id, $sucursal_id, $id)
    {
        $actividad = Actividad::findOrFail($id);

        return view('actividades.edit', array('actividad' => $actividad, 'manager_log_id' => $man_log_id, 'sucursal_id' => $sucursal_id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $man_log_id, $sucursal_id, $actividad_id)
    {
        $this->validarActividad($request);

        $actividad = Actividad::findOrFail($actividad_id);

        $actividad->update($request->all());

        return redirect('/man-log/' . $sucursal_id)->with('actividad_actualizada', 'Actividad actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($man_log_id, $sucursal_id, $id)
    {
        $actividad = Actividad::findOrFail($id);

        $actividad->delete();

        return redirect('/man-log/' . $sucursal_id)->with('actividad_eliminado', 'Actividad con nombre ' . $actividad->nombre . ' eliminada');
    }

    private function validarActividad($request)
    {
        $this->validate($request, [
            'nombre'      => 'required|max:100',
            'descripcion' => 'required|max:500',
        ]);
    }
}