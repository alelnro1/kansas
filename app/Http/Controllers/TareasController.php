<?php

namespace App\Http\Controllers;

use App\EstadoTarea;
use App\Sucursal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Tarea;

class TareasController extends Controller
{
    use SoftDeletes;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Cada vez que se carga el formulario de crear una nueva tarea
     *
     * @return \Illuminate\Http\Response
     */
    public function create($sucursal_id)
    {
        $sucursal = Sucursal::findOrFail($sucursal_id);

        session(['tarea_de_sucursal_id' => $sucursal_id]);

        return view('tareas.create')->with(['sucursal_nombre' => $sucursal->nombre, 'sucursal_id' => $sucursal->id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarTarea($request);

        // Obtengo el id de la sucursal en donde vamos a crear la tarea (la sesion se creo en el metodo "create")
        $sucursal_id = session('tarea_de_sucursal_id');

        // Le agrego la sucursal_id a la instancia del request
        $request->request->add(['sucursal_id' => $sucursal_id]);

        // Creo la tarea
        $tarea = Tarea::create($request->all());

        // Actualizo el campo created_at con el de fecha de inicio
        //$fecha_inicio = str_replace("/", "-", $request->fecha_inicio);


        $tarea->created_at = Carbon::createFromFormat("d/m/Y", $request->fecha_inicio)->toDateTimeString();
        $tarea->save();

        return redirect('/man-log/' . $sucursal_id)->with('tarea_creado', 'Tarea con nombre ' . $request->nombre . ' creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tarea = Tarea::findOrFail($id);
        return view('tareas.show')->with('tarea', $tarea);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tarea = Tarea::findOrFail($id);
        return view('tareas.edit')->with('tarea', $tarea);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validarTarea($request);

        $tarea = Tarea::findOrFail($id);

        $tarea->update($request->all());

        return redirect('/man-log/' . $tarea->sucursal_id)->with('tarea_creado', 'Tarea con nombre ' . $request->nombre . ' actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $tarea = Tarea::findOrFail($id);

        $tarea->delete();

        return redirect('/man-log/' . $tarea->sucursal_id)->with('tarea_creado', 'Tarea con nombre ' . $request->nombre . ' eliminada');
    }

    private function validarTarea($request)
    {
        $this->validate($request, [
            'nombre'       => 'required|max:150',
            'descripcion'  => 'required|max:500',
            'fecha_inicio' => 'required'
        ]);
    }

    /**
     * Cierra una tarea. Si tiene un comentario, lo escribe
     * @param Request $request
     * @param $tarea_id
     * @param null $comentario
     */
    public function cerrar(Request $request, $tarea_id)
    {
        // Busco la tarea
        $tarea = Tarea::findOrFail($tarea_id);

        // Cierro la tarea y le pongo un comentario
        $tarea->fecha_cierre = Carbon::now()->toDateTimeString();
        $tarea->comentario_cierre = $request->comentario;

        $tarea->save();

        return response()->json(['valid' => true]);
    }
}