<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Cargo;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\CargoSucursalUsuario;
use App\Sucursal;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Override del metodo que está en Illuminate\Routing\Router::auth() para poder pasarle variables al form del registro
     *
     * @return void
     */
    public function showRegistrationForm()
    {
        $cargos = Cargo::all();
        $sucursales = Sucursal::all();

        if (property_exists($this, 'registerView')) {
            return view($this->registerView);
        }

        return view('auth.register', array('cargos' => $cargos, 'sucursales' => $sucursales) );
    }


    public function logout()
    {
        Auth::logout();
        Session::flush();
        //session(['ACTUAL_CARGO' => null]);
        //session(['ACTUAL_SUCURSAL' => null]);

        return redirect('/login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'cargo_id' => 'required|not_in:0',
            'sucursal_id' => 'required|not_in:0',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

    }
}
