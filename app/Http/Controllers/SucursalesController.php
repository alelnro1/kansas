<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Sucursal;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class SucursalesController extends Controller
{
    use SoftDeletes;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sucursales = Sucursal::where('empresa_id', Auth::user()->empresa_id)->get();
        return view('sucursales.listar')->with('sucursales', $sucursales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sucursales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarSucursal($request);

        // Agrego la empresa del usuario actual
        $request->request->add(['empresa_id' => Auth::user()->empresa_id]);

        $sucursal = Sucursal::create($request->all());

        if (isset($request->files) && count($request->files) > 0) {
            $archivo = $this->subirArchivo($request);

            $sucursal->archivo = $archivo;
            $sucursal->save();
        }

        return redirect('/sucursales/')->with('sucursal_creado', 'Sucursal con nombre ' . $request->nombre . ' creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sucursal = Sucursal::findOrFail($id);

        return view('sucursales.show')->with(['sucursal' => $sucursal]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sucursal = Sucursal::findOrFail($id);
        return view('sucursales.edit')->with('sucursal', $sucursal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validarSucursal($request);

        $sucursal = Sucursal::findOrFail($id);

        $sucursal->update($request->except(['_method', '_token']));

        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            $sucursal->archivo = $archivo;
            $sucursal->save();
        }

        return redirect('/sucursales/')->with('sucursal_actualizada', 'Sucursal con nombre ' . $sucursal->nombre . ' actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sucursal = Sucursal::findOrFail($id);

        $sucursal->delete();

        return redirect('/sucursales/')->with('sucursal_eliminado', 'Sucursal con nombre ' . $sucursal->nombre . ' eliminado');
    }

    private function validarSucursal($request)
    {
        $this->validate($request, [
            'nombre'          => 'required|max:100',
            'descripcion'     => 'max:500',
            'archivo'         => 'max:30000|mimes:jpg,jpeg,png,gif',
            'domicilio'       => 'required|max:150',
            'email'           => 'email|max:100',
            'telefono'        => 'max:60'
        ]);
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return $url;
    }

}