<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\CargoSucursalUsuario;
use App\DocumentoLectura;
use App\Sucursal;
use App\TipoDocumento;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Documento;
use Illuminate\Support\Facades\Auth;
use App\EstadoDocumento;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Archivo;
use RecursiveIteratorIterator;
use RecursiveArrayIterator;

class DocumentosController extends Controller
{
    use SoftDeletes;

    protected $documentos_create;

    protected $urls_archivos; // Las urls de los archivos subidos para un documento

    private $separador_nombre_archivo; // El separador entre el nombre original de un archivo y el random generado

    public $usuarios_filtrados_de_select;

    public function __construct()
    {
        parent::__construct();

        $this->usuarios_filtrados_de_select = array();
        $this->urls_archivos = array();
        $this->separador_nombre_archivo = '__2180413__';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documentos_propios = Documento::where('autor_id', Auth::user()->id)
                                ->with(['UsuarioAprobador', 'Tipo', 'Estado'])
                                ->orderBy('created_at', 'asc')
                                ->get();

        $documentos_a_aprobar = Documento::where('user_aprobador_id', Auth::user()->id)
                                    ->where('empresa_id', Auth::user()->empresa_id)
                                    ->whereHas(
                                        'estado', function($query) {
                                            $query->where('nombre', 'Pendiente de Aprobación');
                                        }
                                    )
                                    ->get();

        $documentos_para_mi =
            Documento::where('autor_id', '<>', Auth::user()->id)
                ->where('empresa_id', Auth::user()->empresa_id)
                ->whereHas('destinatarios', function($query) {
                    $query->where('usuario_id', Auth::user()->id);
                    $query->where('leido', false);
                })
                ->whereHas('estado', function($query){
                    $query->where('nombre', 'Publicado');
                })
                ->get();


        $cargos_sucursales_usuarios =
            CargoSucursalUsuario::where('usuario_id', Auth::user()->id) // Tengo todos los cargos del usuario actual
                ->with(['cargos' => function($query_cargos){ // Cargo los hijos
                    $query_cargos->with(['CargosHijos' => function($query) {
                        $query->with(['DocumentosLecturas' => function($query){
                            $query->where('leido', 0);

                            // Cargo los datos del usuario
                            $query->with(['Usuario' => function($query) {
                                $query->select(['id', 'nombre', 'apellido']);

                                // Cargo las sucursales del usuario que tiene que leer el documento
                                $query->with(['Sucursales' => function($query){
                                    $query->select(['sucursal_id']);
                                }]);
                            }]);

                            // Cargo los datos del documento
                            $query->with(['Documento' => function($query){
                                $query->select(['id', 'nombre', 'created_at', 'estado_id', 'tags']);

                                $query->whereHas('estado', function($query) {
                                    $query->where('nombre', 'Publicado');
                                });
                            }]);
                        }]);
                    }]);
                }])
                ->get();

        // Obtengo la cantidad de dependientes del usuario actual
        $cantidad_cargos_dependientes = $this->obtenerCantidadCargosDependientes();

        // Seteo los documentos de dependientes como un array
        $documentos_de_dependientes = [];

        // Obtengo los documentos de dependientes
        foreach ($cargos_sucursales_usuarios as $cargo_sucursal_usuario) {
            // Si el usuario esta en mas de una sucursal, almaceno temporalmente la sucursal que estoy revisando
            // si tiene documentos nuevos
            $sucursal_user_id_recorriendo = $cargo_sucursal_usuario->sucursal_id;

            // Voy a los cargos del usuario
            foreach ($cargo_sucursal_usuario->cargos as $cargo) {
                // Voy a los hijos de los cargos que tiene asignado el usuario actual
                foreach ($cargo->CargosHijos as $cargo_hijo) {
                    // Voy a los DocumentosLecturas con el cargo_id del hijo
                    foreach ($cargo_hijo->DocumentosLecturas as $documento_lectura){
                        // Recorro las sucursales y me fijo si es igual a la que estaba mas arriba
                        foreach ($documento_lectura->Usuario->Sucursales as $sucursal) {
                            if ($sucursal->sucursal_id == $sucursal_user_id_recorriendo) {
                                // Si el documento es vacio, entonces alguna condicion no se cumplio => no lo muestro
                                if ($documento_lectura->Documento) {
                                    $documento_dependiente =
                                        [
                                            'id'                => $documento_lectura->documento->id,
                                            'nombre'            => $documento_lectura->documento->nombre,
                                            'nro_identificador' => $documento_lectura->documento->nro_identificador,
                                            'tags'              => $documento_lectura->documento->tags,
                                            'created_at'        => $documento_lectura->documento->created_at,
                                            'usuario_id'        => $documento_lectura->usuario->id,
                                            'usuario_nombre'    => $documento_lectura->usuario->nombre,
                                            'usuario_apellido'  => $documento_lectura->usuario->apellido
                                        ];

                                    array_push($documentos_de_dependientes, $documento_dependiente);
                                }
                            }
                        }
                    }
                }
            }
        }

        return view('documentos.listar', [
            'documentos_propios'            => $documentos_propios,
            'documentos_a_aprobar'          => $documentos_a_aprobar,
            'documentos_para_mi'            => $documentos_para_mi,
            'cantidad_documentos_a_aprobar' => $documentos_a_aprobar->count(),
            'cantidad_documentos_para_mi'   => $documentos_para_mi->count(),
            'documentos_de_dependientes'    => $documentos_de_dependientes,
            'cantidad_cargos_dependientes'  => $cantidad_cargos_dependientes
        ]);
    }

    private function obtenerCantidadCargosDependientes()
    {
        $cargos_dependientes = CargoSucursalUsuario::where('usuario_id', Auth::user()->id) // Tengo todos los cargos del usuario actual
                                    ->with(['cargos' => function($query_cargos){ // Cargo los hijos
                                        $query_cargos->with('CargosHijos');
                                    }])
                                    ->get();

        $cantidad = 0;

        foreach ($cargos_dependientes as $cargos_dependiente){
            foreach ($cargos_dependiente->cargos as $cargos) {
                $cantidad += count($cargos->CargosHijos);
            }
        }

        return $cantidad;
    }

    public function biblioteca()
    {
        $documentos =
            Documento::where('autor_id', '<>', Auth::user()->id)
                ->whereHas('destinatarios', function($query) {
                    $query->where('usuario_id', Auth::user()->id);
                })
                ->whereHas('estado', function($query){
                    $query->where('nombre', 'Publicado');
                })
                ->orderBy('tipo_documento_id')
                ->get();


        // Voy a armar un array asi => $documento_por_tipo[tipo_doc_id][doc_1|2|3|4|5]
        $documentos_por_tipo = [];

        // Armo un array con los documentos
        foreach ($documentos as $key => $documento) {
            // Verifico que el key en $documento por tipo no exista, para crear el primer documento
            if (!isset($documentos_por_tipo[$documento->tipo_documento_id])) {
                $documentos_por_tipo[$documento->Tipo->nombre] = [];
            }

            // Cargo el documento a ese sub-array
            array_push($documentos_por_tipo[$documento->Tipo->nombre], $documento);

        }

        return view('documentos.biblioteca', array('documentos_por_tipo' => $documentos_por_tipo));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Busco las versiones anteriores
        $documentos = Documento::with(['tipo', 'estado'])
            ->whereHas('tipo', function($query) {
                $query->where('nombre', '=', 'Proceso');
            })
            ->whereHas('estado', function($query) {
                $query->where('nombre', '=', 'Publicado');
            })
            ->where('empresa_id', Auth::user()->empresa_id)
            ->get();

        $estados    = EstadoDocumento::all();
        $tipos_documento = TipoDocumento::all();

        // Esto es para seleccionar multiples destinatarios
        /*$sucursales = Sucursal::where('empresa_id', Auth::user()->empresa_id)->get();
        $cargos     = Cargo::where('nombre', '<>', 'super admin')->where('empresa_id', Auth::user()->empresa_id)->get();
        $usuarios   = User::where('es_super_admin', 0)->where('empresa_id', Auth::user()->empresa_id)->where('id', '<>', Auth::user()->id)->get(); // Tambien para el usuario aprobador*/


        $cargos_sucursales_usuarios =
            CargoSucursalUsuario::with([
                    'cargos', 'sucursales', 'usuarios'
                ])
                ->whereHas('sucursales', function($query) {
                        $query
                            ->where('empresa_id', Auth::user()->empresa_id);
                })
                ->whereHas('cargos', function($query) {
                        $query
                            ->where('nombre', '<>', 'super admin')
                            ->where('empresa_id', Auth::user()->empresa_id);
                })
                ->whereHas('usuarios', function($query) {
                        $query
                            ->where('es_super_admin', 0)
                            ->where('empresa_id', Auth::user()->empresa_id)
                            ->where('id', '<>', Auth::user()->id);
                })
                ->groupBy('sucursal_id', 'cargo_id', 'usuario_id')
                ->get();

        // Armo las estructuras de datos
        $sucursales = $cargos = $usuarios = array();

        foreach($cargos_sucursales_usuarios as $cargo_sucursal_usuario) {
            $cargo      = $cargo_sucursal_usuario->cargos[0];
            $sucursal   = $cargo_sucursal_usuario->sucursales[0];
            $usuario    = $cargo_sucursal_usuario->usuarios[0];

            if (!in_array($cargo, $cargos))
                array_push($cargos, $cargo);

            if (!in_array($sucursal, $sucursales))
                array_push($sucursales, $sucursal);

            if (!in_array($usuario, $usuarios))
                array_push($usuarios, $usuario);
        }

        return view('documentos.create', array('usuarios' => $usuarios, 'documentos' => $documentos,
                                               'estados' => $estados, 'tipos_documento' => $tipos_documento, 'sucursales' => $sucursales, 'cargos' => $cargos));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* Primero valido el documento y si no anda redirecciono con ese error */
            // Valido el documento
            $validator = Validator::make($request->all(), [
                'nombre'      => 'required|max:100',
                'descripcion' => 'required|max:500',
                'estado_id'   => 'required|not_in:0',
                'tipo_documento_id' => 'required|not_in:0',
                'version_anterior' => 'required_unless:primera_version,on',
                'user_aprobador_id' => 'required_if:requiere_aprobacion,on',
                'destinatarios_1' => 'required_if:destinatarios_todos,0',
                'tags' => 'required'
            ]);

            $validator->after(function($validator) use($request){
                if (isset($request->requiere_aprobacion) && $request->requiere_aprobacion == "on") {
                    if ($request->user_aprobador_id == "0"){
                        $validator->errors()->add('user_aprobador_id', 'Debe seleccionar un usuario aprobador');
                    }
                }

                if (isset($request->tipo_documento_id) && $request->tipo_documento_id == "2") { // Eligio proceso
                    if(!isset($request->primera_version)) { // No es primera version
                        if(isset($request->version_anterior) && $request->version_anterior == "0") {
                            $validator->errors()->add('tipo_documento_id', 'Debe seleccionar una version anterior para el tipo de documento');
                        }
                    }
                }
            });

            if ($validator->fails()) {
                return redirect('documentos/create')
                    ->withErrors($validator)
                    ->withInput();
            }

        /* Segundo creo el documento PERO si no hay destinatarios, lo borro */
            // Vinculo al autor del documento con el usuario actual
            $request->request->add(['autor_id' => Auth::user()->id]);

            // Agrego la empresa del usuario actual
            $request->request->add(['empresa_id' => Auth::user()->empresa_id]);

            // Si el documento que quiero crear (con los mismos atributos) no habia tenido errores => lo creo, sino lo vinculo
            $documento_viejo = Documento::onlyTrashed()
                ->where('nombre', $request->nombre)
                ->where('descripcion', $request->descripcion)
                ->where('autor_id', $request->autor_id)
                ->where('empresa_id', $request->empresa_id)
                ->first();

            // Obtengo el tipo de documento que se usará al crear el documento
            $tipo_documento =
                TipoDocumento::where('id', $request->tipo_documento_id)->select(['id', 'cantidad'])->first();

            // Obtengo el nuevo identificador del documento por tipo
            $nro_identificador = $tipo_documento->cantidad + 1;

            // Actualizo la cantidad de documentos del tipo
            $tipo_documento->cantidad = $nro_identificador;
            $tipo_documento->save();

            $request->request->add(['nro_identificador' => $nro_identificador]);

            if (count($documento_viejo) <= 0) { // Es nuevito, nunca se habia creado
                $documento = Documento::create($request->all());
            } else {
                $documento = $documento_viejo;
            }

            // Si se seleccionaron todos, se les envía a todos, si no se filtra
            if ($request->destinatarios_todos == "1"){
                $todos = true;
            } else {
                $this->generarFiltrosDestinatarios($request);

                // Si para el documento recien creado no hay destinatarios, eliminarlo y mostrar el error
                $validator->after(function ($validator) use ($request, $documento) {
                    if (!$this->verificarQueHayaAlMenos1Destinatario()) {
                        $documento->delete();

                        // Tambien borro los archivos que se hayan subido
                        $this->eliminarArchivosSubidos();

                        $validator->errors()->add('destinatarios_1', 'No hay destinatarios');
                    }
                });

                if ($validator->fails()) {
                    return redirect('documentos/create')
                        ->withErrors($validator)
                        ->withInput();
                }

                $todos = false;
            }

        // Pasaron las validaciones, restauro el documento
        $documento->deleted_at = null;
        $documento->save();

        // El documento se generó correctamente y hay al menos 1 destinatario => vinculo destinatarios y genero archivos
        $this->vincularDestinatariosConDocumento($documento, $todos);

        if ($request->urls_archivos != "") {
            $this->urls_archivos = $request->urls_archivos;

            $this->vincularArchivosConDocumento($documento);
        }

        return redirect('/documentos/')->with('documento_creado', 'Documento con nombre ' . $request->nombre . ' creado');
    }

    private function generarFiltrosDestinatarios($request)
    {
        // Todos los usuarios con todos los cargos de todas las sucursales, de la empresa del usuario actual, para luego ir filtrando
        $usuarios = CargoSucursalUsuario::whereHas('usuarios', function($query){
            $query->where('empresa_id', Auth::user()->empresa_id);
        })->get();

        // Recorro todos los filtros y los cargo en un array, correspondiente a cada filtro
        for ($i = 1; $i <= 3; $i++) {
            // Arranco con todos los array vacios
            $filtro_sucursales[$i] = array();
            $filtro_cargos[$i] = array();
            $filtro_personas[$i] = array();

            // Lleno los array de filtros
            if (isset($request->{"destinatarios_" . $i})) {
                foreach ($request->{"destinatarios_" . $i} as $destinatario) {
                    $campo = explode(';', $destinatario);
                    $tipo = $campo[0];
                    $valor = $campo[1];

                    if ($tipo == "sucursal") {
                        array_push($filtro_sucursales[$i], $valor);
                    }

                    if ($tipo == "cargo") {
                        array_push($filtro_cargos[$i], $valor);
                    }

                    if ($tipo == "usuario") {
                        array_push($filtro_personas[$i], $valor);
                    }
                }

                // Lleno los usuarios filtrados de los selects del formulario de documentos.create
                $this->usuarios_filtrados_de_select[$i] = $this->filtrarUsuariosPorSucursal($usuarios, $filtro_sucursales, $i);
                $this->usuarios_filtrados_de_select[$i] = $this->filtrarUsuariosPorCargo($this->usuarios_filtrados_de_select[$i], $filtro_cargos, $i);
                $this->usuarios_filtrados_de_select[$i] = $this->filtrarUsuariosPorPersonas($this->usuarios_filtrados_de_select[$i], $filtro_personas, $i);
            }
        }
    }

    protected function filtrarUsuariosPorSucursal($usuarios_filtrados, $filtro_sucursales, $i)
    {
        if(empty($filtro_sucursales[1])) return $usuarios_filtrados; // Si no hay filtros no hacer nada

        return $usuarios_filtrados->filter(function ($usuario) use ($filtro_sucursales, $i){
                    foreach ($usuario->sucursales as $sucursal_usuario) {
                        $id_sucursal_usuario =  $sucursal_usuario->id;

                        foreach ($filtro_sucursales[$i] as $sucursal_filtro) {
                            if ($id_sucursal_usuario == $sucursal_filtro) {
                                return true;
                            }
                        }
                    }
                });
    }

    protected function filtrarUsuariosPorCargo($usuarios_filtrados, $filtro_cargos, $i)
    {
        if(empty($filtro_cargos[1])) return $usuarios_filtrados; // Si no hay filtros no hacer nada

        return $usuarios_filtrados->filter(function ($usuario) use ($filtro_cargos, $i){
            foreach ($usuario->cargos as $cargo_usuario) {
                $id_cargo_usuario =  $cargo_usuario->id;

                foreach ($filtro_cargos[$i] as $cargo_filtro) {
                    if ($id_cargo_usuario == $cargo_filtro) {
                        return true;
                    }
                }
            }
        });
    }

    protected function filtrarUsuariosPorPersonas($usuarios_filtrados, $filtro_personas, $i)
    {
        if(empty($filtro_personas[1])) return $usuarios_filtrados; // Si no hay filtros no hacer nada

        return $usuarios_filtrados->filter(function ($usuario) use ($filtro_personas, $i){
            foreach ($usuario->usuarios as $filtro_persona) {
                $id_persona_usuario =  $filtro_persona->id;

                foreach ($filtro_personas[$i] as $persona_filtro) {
                    if ($id_persona_usuario == $persona_filtro) {
                        return true;
                    }
                }
            }
        });
    }

    /**
     * De todos los usuarios filtrados para crear un documento, hay que verificar si hay al menos 1 para poder crear el documento
     * @return bool
     * @internal param $usuarios_filtrados
     */
    private function verificarQueHayaAlMenos1Destinatario()
    {
        for ($i = 1; $i <= 3; $i++) {
            if (count($this->usuarios_filtrados_de_select[$i]) >= 1) {
                return true;
            } else {
                return false;
            }
        }
    }

    private function eliminarArchivosSubidos()
    {
        if (count($this->urls_archivos) > 0) {
            foreach ($this->urls_archivos as $url) {
                unlink($url);
            }
        }

        return true;
    }

    /**
     * Creo los destinatarios. Si estan todos seleccionados, se crea un registro por usuario, sino, de los filtros
     * @param $documento
     * @param null $todos
     */
    protected function vincularDestinatariosConDocumento($documento, $todos = null)
    {
        if ($todos){
            $usuarios = CargoSucursalUsuario::whereHas('usuarios', function($query){
                $query->where('empresa_id', Auth::user()->empresa_id);
            })->get();

            foreach ($usuarios as $usuario) {
                // Por las dudas de que haya un registro malo, donde algun campo sea null, lo hago mas robusto
                if ($usuario->usuario_id != null && $usuario->sucursal_id != null && $usuario->cargo_id) {
                    DocumentoLectura::create([
                        'documento_id' => $documento->id,
                        'usuario_id' => $usuario->usuario_id,
                        'sucursal_id' => $usuario->sucursal_id,
                        'cargo_id' => $usuario->cargo_id
                    ]);
                }
            }
        } else {
            for ($i = 1; $i <= 3; $i++) {
                if (isset($this->usuarios_filtrados_de_select[$i]) && count($this->usuarios_filtrados_de_select[$i]) >= 1) {
                    // Creo la relacion en la tabla documento_lectura
                    foreach ($this->usuarios_filtrados_de_select[$i] as $usuario) {
                        $usuario_lectura = DocumentoLectura::where('documento_id', $documento->id)
                            ->where('usuario_id', $usuario->usuario_id)
                            ->where('sucursal_id', $usuario->sucursal_id)
                            ->where('cargo_id', $usuario->cargo_id)->get();

                        if (count($usuario_lectura) <= 0) { // Si no hay un registro de lectura para ese usuario de esa sucursal con ese cargo => crearlo
                            DocumentoLectura::create([
                                'documento_id' => $documento->id,
                                'usuario_id' => $usuario->usuario_id,
                                'sucursal_id' => $usuario->sucursal_id,
                                'cargo_id' => $usuario->cargo_id
                            ]);
                        }
                    }
                }
            }
        }
    }

    private function vincularArchivosConDocumento($documento)
    {
        $this->urls_archivos = explode('||', $this->urls_archivos);

        foreach ($this->urls_archivos as $url){
            $nombre = explode($this->separador_nombre_archivo, $url);

            Archivo::create(['nombre' => $nombre[1], 'url' => $url, 'documento_id' => $documento->id]);
        }

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $documento = Documento::findOrFail($id);
        $documento->load('archivos');
        $documento->load('UsuarioAprobador');
        $documento->load('DocumentosPadres');

        // Busco los destinatarios del documento
        $destinatarios = $this->cargarDestinatarios($id, true);

        // Busco todos los que tienen que leer el documento
        $documento_lectura = DocumentoLectura::where('documento_id', $documento->id)->where('usuario_id', Auth::user()->id)->first();

        // Busco los parámetros (cargo y sucursal)
        $documento_parametros = DocumentoLectura::where('documento_id', $documento->id)
            ->groupBy('sucursal_id', 'cargo_id')
            ->with(
                [
                    'Sucursal' => function($query) {
                        $query->select(['id', 'descripcion']);
                    },
                    'Cargo' => function($query) {
                        $query->select(['id', 'nombre']);
                    }
                ])
            ->get();

        // Si existe el usuario como aprobador como destinatario, cuando va a aprobar/desaprobar, lo marca leido
        if ($documento->user_aprobador_id == Auth::user()->id) {
            $this->usuarioAprobadorLeer($destinatarios, $documento_lectura);
        }

        // Actualizo que el usuario leyó el documento
        if ($documento->autor_id != Auth::user()->id && $documento->user_aprobador_id != Auth::user()->id) {
            //$documento_lectura = DocumentoLectura::where('documento_id', $documento->id)->where('usuario_id', Auth::user()->id)->first();

            // Salvo cualquier situacion del documento de dependientes
            if ($documento_lectura) {
                $documento_lectura->leido = true;
                $documento_lectura->save();
            }
        }

        return view('documentos.show', array('documento' => $documento, 'destinatarios' => $destinatarios, 'parametros' => $documento_parametros));
    }

    private function usuarioAprobadorLeer($destinatarios, $documento_lectura)
    {
        foreach ($destinatarios as $destinatario) {
            if ($destinatario->usuario_id == Auth::user()->id) {

                // Actualizo que el usuario aprobador leyó el documento
                $documento_lectura->leido = true;
                $documento_lectura->save();
            }
        }
    }

    /**
     * Devuelve los destinatarios de un documento
     * @param $documento_id
     * @param bool $distinct Si es verdadero, vamos a agrupar por usuario, para que no se repita en la lista
     * @return
     */
    private function cargarDestinatarios($documento_id, $distinct=false)
    {
        $destinatarios = DocumentoLectura::where('documento_id', $documento_id);

        if ($distinct){
            $destinatarios->groupBy('usuario_id');
        }

        return $destinatarios->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $documento_id El documento que vamos a editar
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit($documento_id)
    {
        $documento = Documento::findOrFail($documento_id);

        $documento->load('tipo', 'estado');

        // Si el documento aún no esta aprobado, y es del usuario actual
        if ($this->puedeEditarDocumento($documento))
        {
            // Busco todos los estados posibles de un documento
            $estados = EstadoDocumento::all();

            // Busco todos los tipos de documentos
            $tipos_documento = TipoDocumento::all();

            // Busco todos los documentos que pueden ser anteriores
            $documentos = Documento::where('empresa_id', Auth::user()->empresa_id);

            // Busco los destinatarios del documento
            $destinatarios = $this->cargarDestinatarios($documento_id);

            // Esto es para seleccionar multiples destinatarios
            $sucursales = Sucursal::where('empresa_id', Auth::user()->empresa_id)->get();
            $cargos   = Cargo::where('nombre', '<>', 'super admin')->where('empresa_id', Auth::user()->empresa_id)->get();
            $usuarios   = User::where('es_super_admin', 0)->where('empresa_id', Auth::user()->empresa_id)->where('id', '<>', Auth::user()->id)->get(); // Tambien para el usuario aprobador

            return view('documentos.edit', array('documento' => $documento, 'usuarios' => $usuarios, 'documentos' => $documentos,
                'estados' => $estados, 'tipos_documento' => $tipos_documento, 'sucursales' => $sucursales, 'cargos' => $cargos, 'destinatarios' => $destinatarios));
        } else {
            abort(404);
        }
    }

    /**
     * Se verifica que un documento se pueda editar
     * @param $documento El documento que se desea editar
     * @return bool
     */
    private function puedeEditarDocumento($documento)
    {
        // Si no esta aprobado y es mio => puedo editarlo
        if (($documento->estado->nombre != "Aprobado" && $documento->autor_id == Auth::user()->id)
            || $documento->user_aprobador_id == Auth::user()->id)
        {
            return true;
        }
    }

    public function eliminarArchivo($documento_id, $archivo_id)
    {
        $documento = Documento::findOrFail($documento_id);
        $documento->load('archivos');

        $valid = false;

        // Si el archivo pertenece al documento, y el usuario actual puede editar el documento => eliminar archivo
        if (Auth::user()->id == $documento->autor_id)
        {
            foreach ($documento->archivos as $archivo) {
                // Es el archivo que quiero borrar
                if ($archivo->id == $archivo_id){
                    // Eliminar el archivo
                    unlink($archivo->url);
                    $archivo->delete();

                    $valid = true;
                }
            }
        }

        echo json_encode(array('valid' => $valid));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $documento = Documento::findOrFail($id);

        /* Primero valido el documento y si no anda redirecciono con ese error */
        // Valido el documento
        $validator = Validator::make($request->all(), [
            'nombre'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            'estado_id'   => 'required|not_in:0',
            'tipo_documento_id' => 'required|not_in:0',
            'version_anterior' => 'required_unless:primera_version,on',
            'user_aprobador_id' => 'required_if:requiere_aprobacion,on'
        ]);

        // Genero el filtro con los destinatarios
        $this->generarFiltrosDestinatarios($request);

        $validator->after(function($validator) use($request, $documento) {
            if (isset($request->requiere_aprobacion) && $request->requiere_aprobacion == "on") {
                if ($request->user_aprobador_id == "0"){
                    $validator->errors()->add('user_aprobador_id', 'Debe seleccionar un usuario aprobador');
                }
            }

            if (isset($request->tipo_documento_id) && $request->tipo_documento_id == "2") { // Eligio proceso
                if(!isset($request->primera_version)) { // No es primera version
                    if(isset($request->version_anterior) && $request->version_anterior == "0") {
                        $validator->errors()->add('tipo_documento_id', 'Debe seleccionar una version anterior para el tipo de documento');
                    }
                }
            }

            // Si de la seleccion, no hay ningun destinatario
            if (isset($request->destinatarios_1)) {
                if (!$this->verificarQueHayaAlMenos1Destinatario()) {
                    $validator->errors()->add('destinatarios', 'No hay destinatarios filtrados');
                }
            }
        });

        if ($validator->fails()) {
            return redirect('documentos/' . $documento->id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }

        $documento->update($request->all());

        // El documento se generó correctamente y hay al menos 1 destinatario => vinculo destinatarios y genero archivos
        if (isset($request->destinatarios_1)) {
            // Elimino todos los destinatarios del documento
            $documento_lectura = DocumentoLectura::where('documento_id', $documento->id)->select(['documento_id', 'id'])->get();;

            foreach ($documento_lectura as $doc){
                $doc->delete();
            }

            // Vinculo
            $this->vincularDestinatariosConDocumento($documento);
        }

        if ($request->urls_archivos != "") {
            $this->urls_archivos = $request->urls_archivos;

            $this->vincularArchivosConDocumento($documento);
        }

        return redirect('/documentos/')->with('documento_actualizado', 'Documento con nombre ' . $documento->nombre . ' actualizado');



        /* Segundo creo el documento PERO si no hay destinatarios, lo borro */
        // Vinculo al autor del documento con el usuario actual
        /*$request->request->add(['autor_id' => Auth::user()->id]);

        // Si el documento que quiero crear (con los mismos atributos) no habia tenido errores => lo creo, sino lo vinculo
        $documento_viejo = Documento::onlyTrashed()
            ->where('nombre', $request->nombre)
            ->where('descripcion', $request->descripcion)
            ->where('autor_id', $request->autor_id)
            ->first();

        if (count($documento_viejo) <= 0) { // Es nuevito, nunca se habia creado
            $documento = Documento::create($request->all());
        } else {
            $documento = $documento_viejo;
        }



        // Si para el documento recien creado no hay destinatarios, eliminarlo y mostrar el error
        $validator->after(function($validator) use ($request, $documento) {
            if ( !$this->verificarQueHayaAlMenos1Destinatario($documento) ) {
                $documento->delete();

                // Tambien borro los archivos que se hayan subido
                $this->eliminarArchivosSubidos();

                $validator->errors()->add('destinatarios', 'No hay destinatarios');
            }
        });

        if ($validator->fails()) {
            return redirect('documentos/create')
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->urls_archivos != "") {
            $this->urls_archivos = $request->urls_archivos;

            $this->vincularArchivosConDocumento($documento);
        }*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $documento = Documento::findOrFail($id);

        $documento->delete();

        return redirect('/documentos/')->with('documento_eliminado', 'Documento con nombre ' . $documento->nombre . ' eliminado');
    }

    /**
     * Apruebo un documento, con un comentario
     * @param Request $request
     * @param $documento_id El documento que se va a aprobar
     * @return
     */
    public function aprobar(Request $request, $documento_id)
    {
        $this->aprobarODesaprobar($request, $documento_id, 'Publicado');

        return response()->json(['message' => 'El documento se aprobó']);
    }

    private function aprobarODesaprobar($request, $documento_id, $tipo_operacion)
    {
        $documento = Documento::findOrFail($documento_id);

        // Verifico que el usuario aprobador sea el actual
        if (Auth::user()->id == $documento->user_aprobador_id){
            // Busco el id del estado aprobado
            $estado = EstadoDocumento::where('nombre', $tipo_operacion)->select(['id'])->first();

            $documento->comentario = $request->comentario;
            $documento->estado_id = $estado->id;

            $documento->save();
        }
    }

    /**
     * Desapruebo un documento, con un comentario
     * @param Request $request
     * @param $documento_id El documento que se va a desaprobar
     * @return
     */
    public function desaprobar(Request $request, $documento_id)
    {
        $this->aprobarODesaprobar($request, $documento_id, 'Desaprobado');

        return response()->json(['message' => 'El documento se desaprobó']);
    }

    /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivos(Request $request)
    {
        // Seteo el directorio destino, que tiene los permisos necesarios
        $directorio_destino = 'uploads/archivos/';

        $archivo = $request->files->all()['files'][0];

        /*// Reglas de archivos
        $reglas = [
            'archivo' => 'mimes:png,gif,jpg,jpeg,txt,pdf,doc,docx'
        ];

        // Mensajes de error
        $mensajes = [
            'mimes' => "Extension inválida. Solo png, gif, jpg, jpeg, txt, pdf o doc"
        ];

        $validator = Validator::make(array('archivo'=> $archivo), $reglas, $mensajes);*/

        //if($validator->passes())
        //{
            $nombre_original    = $archivo->getClientOriginalName();
            $nuevo_nombre       = rand(111111,999999) .'_'. time() . $this->separador_nombre_archivo . $nombre_original;

            $archivo->move($directorio_destino, $nuevo_nombre);

            echo json_encode(array('valid' => true, 'url' => $directorio_destino . $nuevo_nombre));
        //}

        //echo json_encode(array('valid' => false, 'error' => ));
    }

    private function validarDocumento($request)
    {
        $this->validate($request, [
            'nombre'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            //'archivo'     => 'required|max:1000|mimes:jpg,jpeg,png,gif',
            //'version'     => 'required|not_in:0',
            //'tipo'        => 'required|not_in:0'
        ]);
    }

}