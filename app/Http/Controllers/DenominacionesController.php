<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Denominacion;
use Illuminate\Support\Facades\Auth;

class DenominacionesController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $denominaciones = Denominacion::where('empresa_id', Auth::user()->empresa_id)->get();

        return view('denominaciones.listar')->with('denominaciones', $denominaciones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('denominaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarDenominacion($request);

        $request->request->add(['empresa_id' => Auth::user()->empresa_id]);

        $denominacion = Denominacion::create($request->all());

        return redirect('/denominaciones/')->with('denominacion_creado', 'Denominacion con nombre ' . $request->nombre . ' creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $denominacion = Denominacion::where('id', $id)->where('empresa_id', Auth::user()->empresa_id)->firstOrFail();
        return view('denominaciones.show')->with('denominacion', $denominacion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $denominacion = Denominacion::findOrFail($id);
        return view('denominaciones.edit')->with('denominacion', $denominacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validarDenominacion($request);

        $denominacion = Denominacion::findOrFail($id);

        $denominacion->update($request->all());

        return redirect('/denominaciones/' . $denominacion->id)->with('denominacion_actualizado', 'Denominacion actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $denominacion = Denominacion::findOrFail($id);

        $denominacion->delete();

        return redirect('/denominaciones/')->with('denominacion_eliminado', 'Denominacion con nombre ' . $denominacion->nombre . ' eliminado');
    }

    private function validarDenominacion($request)
    {
        $this->validate($request, [
            'nombre'    => 'required|max:100',
            'valor'     => 'required|numeric'
        ]);
    }
}