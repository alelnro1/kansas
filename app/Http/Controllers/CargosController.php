<?php

namespace App\Http\Controllers;

use App\CargoSucursalUsuario;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Cargo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class CargosController extends Controller
{
    use SoftDeletes;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cargos = Cargo::where('nombre', '<>', 'super admin')
            ->where('empresa_id', Auth::user()->empresa_id)
            ->with('CargoPadre')->orderBy('padre_id')->get();

        return view('cargos.listar')->with(['cargos' => $cargos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cargos = Cargo::where('empresa_id', Auth::user()->empresa_id)->get();
        return view('cargos.create')->with('cargos', $cargos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->armarValidadorCargo($request);

        if ($validator->fails()) {
            return redirect('cargos/create')
                ->withErrors($validator)
                ->withInput();
        }

        if (isset($request->manager_log)) {
            $request->request->add(['manager_log' => true]);
        }

        // Agrego la empresa del usuario actual
        $request->request->add(['empresa_id' => Auth::user()->empresa_id]);

        $cargo = Cargo::create($request->all());

        return redirect('/cargos/')->with('cargo_creado', 'Cargo con nombre ' . $request->nombre . ' creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cargo = Cargo::findOrFail($id);

        return view('cargos.show')->with('cargo', $cargo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cargo = Cargo::findOrFail($id);
        $cargos = Cargo::where('id', '<>', $id)->where('empresa_id', Auth::user()->empresa_id)->get();

        return view('cargos.edit', array('cargo' => $cargo, 'cargos' => $cargos));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->armarValidadorCargo($request);

        if ($validator->fails()) {
            return redirect('cargos/create')
                ->withErrors($validator)
                ->withInput();
        }

        $cargo = Cargo::findOrFail($id);

        // Si tiene_padre no se envio => no tendra padre
        if (!isset($request->tiene_padre)) {
            $request->request->add(['padre_id' => null]);
        }

        if (isset($request->manager_log)) {
            $request->request->add(['manager_log' => true]);
        }

        $cargo->update($request->except(['_token']));

        return redirect('/cargos/')->with('cargo_actualizado', 'Cargo actualizado');
    }

    /**
     * Elimina un cargo solamente si no hay usuarios con ese cargo
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Busco el cargo
        $cargo = Cargo::findOrFail($id);

        // Busco todos los usuarios que tengan ese cargo en alguna sucursal
        $usuarios_con_cargo = CargoSucursalUsuario::where('cargo_id', '=', $id)->count();

        // Verifico que no haya usuarios
        if ($usuarios_con_cargo > 0) {
            return redirect('/cargos/')->with('cargo_tiene_usuarios', 'El cargo ' . $cargo->nombre . ' no puede ser eliminado porque tiene usuarios');
        } else {
            // Busco si el cargo a eliminar tiene hijos
            $cargos_hijos = Cargo::where('padre_id', $id)->get();

            // El futuro padre de todos los hijos huerfanos sera el padre del eliminado
            $cargo_padre_id = $cargo->padre_id;

            // Si el cargo que voy a eliminar tiene padre, ese pasara a ser el padre de los hijos
            if ($cargo_padre_id) {
                foreach ($cargos_hijos as $cargo_hijo) {
                    $cargo_hijo->padre_id = $cargo_padre_id;
                    $cargo_hijo->save();
                }
            }

            // Elimino el cargo
            $cargo->delete();

            return redirect('/cargos/')->with('cargo_eliminado', 'Cargo con nombre ' . $cargo->nombre . ' eliminado');
        }
    }

    private function armarValidadorCargo($request)
    {
        $validator = Validator::make($request->all(), [
            'nombre'      => 'required|max:100',
        ]);

        $validator->after(function($validator) use ($request){
            if ($request->tiene_padre == "on") {
                if($request->padre_id == "0"){
                    $validator->errors()->add('padre_id', 'Debe seleccionar un padre');
                }
            }
        });

        return $validator;
    }
}