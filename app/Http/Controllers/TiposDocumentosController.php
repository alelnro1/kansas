<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\TipoDocumento;

class TiposDocumentosController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos_documentos = TipoDocumento::all();
        return view('tipos-documentos.listar')->with('tiposDocumentos', $tipos_documentos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipos-documentos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'      => 'required|max:100',
        ]);

        if (isset($request->versionado) && $request->versionado == "on") {
            $versionado = true;
        } else {
            $versionado = false;
        }

        TipoDocumento::create([
            'nombre' => $request->nombre,
            'versionado' => $versionado
        ]);

        return redirect('/tipos-documentos/')->with('tipoDocumento_creado', 'Tipo de documento  creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipo_documento = TipoDocumento::findOrFail($id);
        return view('tipos-documentos.show')->with('tipoDocumento', $tipo_documento);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_documento = TipoDocumento::findOrFail($id);
        return view('tipos-documentos.edit')->with('tipoDocumento', $tipo_documento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipo_documento = TipoDocumento::findOrFail($id);

        $this->validate($request, [
            'nombre'      => 'required|max:100',
        ]);

        if (isset($request->versionado) && $request->versionado == "on") {
            $versionado = true;
        } else {
            $versionado = false;
        }

        $tipo_documento->update([
            'nombre' => $request->nombre,
            'versionado' => $versionado
        ]);

        return redirect('/tipos-documentos/')->with('tipoDocumento_actualizado', 'Tipo de documento  actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo_documento = TipoDocumento::findOrFail($id);
        $tipo_documento->load('Documentos');

        if (count($tipo_documento->Documentos) > 0) {
            return redirect('tipos-documentos')->with('aun_hay_documentos_del_tipo', 'No se puede eliminar un tipo de documento con documentos');
        } else {
            $tipo_documento->delete();

            return redirect('/tipos-documentos/')->with('tipoDocumento_eliminado', 'TipoDocumento con nombre ' . $tipo_documento->nombre . ' eliminado');
        }
    }
}