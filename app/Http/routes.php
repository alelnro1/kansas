<?php


Route::auth();

Route::get('/', 'HomeController@index');

// Rutas que necesitan que este logueado
Route::group(['middleware' => ['auth']], function() {
    Route::resource('sucursales', 'SucursalesController');
    Route::resource('documentos', 'DocumentosController');
    Route::resource('tareas',     'TareasController', ['except' => ['store', 'create', 'index']]);
    Route::resource('cargos',     'CargosController');

    // Biblioteca de Documentos
    Route::get('/biblioteca-de-documentos', 'DocumentosController@biblioteca');

    Route::get('/cambiar-clave-personal', 'PerfilController@cambiarClaveForm');
    Route::post('/cambiar-clave-personal', 'PerfilController@actualizarClave');

    Route::get('/perfil', 'PerfilController@verPerfil');
    Route::get('/perfil/edit', 'PerfilController@editarPerfil');
    Route::patch('/perfil/update', 'PerfilController@actualizarPerfil');

    Route::get('sucursales/{sucursal_id}/tareas/create', 'TareasController@create');
    Route::post('sucursales/{sucursal_id}/tareas', 'TareasController@store');
    Route::get('documentos/{documento_id}/archivos/{archivo_id}/eliminar', 'DocumentosController@eliminarArchivo');

    // Subida de archivos cuando se crean documentos
    Route::post('documentos/create/subir-archivos', 'DocumentosController@subirArchivos');
    Route::patch('documentos/edit/subir-archivos', 'DocumentosController@subirArchivos'); // Este lo salvo, porque editar me lo manda acá

    Route::resource('usuarios', 'UsuariosController', ['except' => 'store', 'update']);
    Route::post('usuarios/store', 'UsuariosController@store');
    Route::post('usuarios/{id}', 'UsuariosController@update');
    Route::get('usuarios/{id}/delete', 'UsuariosController@destroy');

    /* Manager Logs */
    Route::get('man-log', 'ManagerLogController@seleccionarSucursal');
    Route::get('man-log/{sucursal}', 'ManagerLogController@index');
    Route::get('man-log/{sucursal}/show/{manager_log}', 'ManagerLogController@show');
    Route::get('man-log/{sucursal}/create', 'ManagerLogController@create');
    Route::get('man-log/cerrar/{manager}', 'ManagerLogController@cerrar');
    Route::post('/man-log/{sucursal}/actualizar-campos', 'ManagerLogController@actualizarCampos');

    /* Actividades del Manager Log */
    Route::get('man-log/{manager}/sucursales/{sucursal}/actividades/create', 'ActividadesController@create');
    Route::get('man-log/{manager}/sucursales/{sucursal}/actividades/{actividad}/edit', 'ActividadesController@edit');
    Route::patch('man-log/{manager}/sucursales/{sucursal}/actividades/{actividad}', 'ActividadesController@update');
    Route::delete('man-log/{manager}/sucursales/{sucursal}/actividades/{actividad}', 'ActividadesController@destroy');
    Route::post('man-log/{manager}/sucursales/{sucursal}/actividades', 'ActividadesController@store');

    /* Tareas */
    Route::post('/tareas/{tarea}/cerrar', 'TareasController@cerrar');

    /* Cuenta Plata del Manager Log */
    Route::post('/man-log/actualizar-cuenta-plata', 'ManagerLogController@actualizarCuentaPlata');

    /* Aprobacion/Desaprobacion de Documentos */
    Route::post('/documentos/{documento}/aprobar', 'DocumentosController@aprobar');
    Route::post('/documentos/{documento}/desaprobar', 'DocumentosController@desaprobar');

    Route::resource('empresas', 'EmpresasController');

    Route::resource('tipos-documentos', 'TiposDocumentosController');

    // Asignar documentos a un usuario despues de la creacion del mismo
    Route::get('/usuarios/{usuario}/asignar-documentos', 'UsuariosController@asignarDocumentos');

    Route::resource('denominaciones', 'DenominacionesController');
});