<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Clase para validar que el usuario que esta logueado y es solo tareas, pueda acceder solo a tareas
 * Class CheckSection
 * @package App\Http\Middleware
 */
class SoloTareas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $path = explode("/", $request->getPathInfo());

        $section = (isset($path[1])) ? $path[1] : null;
        $action  = (isset($path[3])) ? $path[3] : null;

        $secciones_basicas = [
            '', 'man-log', 'logout', 'perfil', 'cambiar-clave-personal'
        ];

        if (Auth::user()) {
            if ($request->session()->has('USER_SOLO_TAREAS') == true && session('USER_SOLO_TAREAS') == true) {
                // El usuario es solo tareas => puede entrar solo a tareas
                if (in_array($section, $secciones_basicas)) {
                    return $next($request);

                } else if ($section == "sucursales" && $action == "tareas") {
                    return $next($request);

                } else if ($section == "tareas") {
                    return $next($request);

                } else {
                    return response('Pagina no encontrada', 404);
                }
            } else {
                return $next($request);
            }
        } else {
            return $next($request);
        }
    }
}