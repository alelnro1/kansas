<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Clase para validar que el usuario que esta logueado pueda acceder a la seccion deseada
 * Class CheckSection
 * @package App\Http\Middleware
 */
class CheckSection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $path = explode("/", $request->getPathInfo());

        $section = (isset($path[1])) ? $path[1] : null;
        $action  = (isset($path[3])) ? $path[3] : null;

        // Las secciones que solo el admin podrá ver
        $admin_sections = [
            'usuarios', 'sucursales', 'cargos'
        ];

        // Las acciones que solo el admin podrá realizar
        $admin_restricted_actions = [
            'edit', 'create', 'destroy', 'remove', 'delete', 'store', 'update'
        ];

        if (in_array($section, $admin_sections))
        {
            // Verifico que el usuario sea admin o super admin y el cargo este dentro del array
            if (in_array($action, $admin_restricted_actions) || $section == "cargos") {

                if (Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin()) {
                    return $next($request);
                } else {
                    return response('Pagina no encontrada', 404);
                }
            }
        }

        return $next($request);

    }
}