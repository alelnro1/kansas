<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoDocumento extends Model {

    protected $table = 'tipos_documentos';
    public $timestamps = true;

    protected $fillable = ['nombre', 'versionado'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function Documentos()
    {
        return $this->hasMany(Documento::class, 'tipo_documento_id', 'id');
    }

}