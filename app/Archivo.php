<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Archivo extends Model
{
    protected $fillable = [
        'nombre', 'url', 'documento_id'
    ];

    public function Documento()
    {
        return $this->belongsTo(Documento::class);
    }
}
