<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denominacion extends Model
{
    protected $table = 'denominaciones';

    protected $fillable = [
        'nombre', 'valor', 'empresa_id'
    ];

    public function Empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function ManagerLogs()
    {
        return $this->belongsToMany(ManagerLog::class, 'denominacion_manager_log', 'denominacion_id', 'manager_log_id')
            ->withPivot('cantidad');
    }
}
