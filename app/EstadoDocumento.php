<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoDocumento extends Model {

	protected $table = 'estados_documentos';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function Documentos()
	{
		return $this->hasMany(DocumentoVersion::class);
	}

}