<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sucursal extends Model {

	protected $table = 'sucursales';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $fillable = [
		'nombre', 'descripcion', 'archivo', 'fecha_apertura', 'domicilio', 'email', 'telefono', 'empresa_id'
	];

	public function Users()
	{
		return $this->belongsToMany(User::class, 'cargo_sucursal_usuario', 'sucursal_id', 'usuario_id')->withPivot('cargo_id');
	}

	public function Tareas()
	{
		return $this->hasMany(Tarea::class);
	}

	public function ManagerLogs()
	{
		return $this->hasMany(ManagerLog::class);
	}

	public function Empresa()
	{
		return $this->belongsTo(Empresa::class);
	}

}