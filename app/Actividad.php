<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'actividades';

    protected $fillable = [
        'nombre', 'descripcion', 'sucursal_id', 'manager_log_id'
    ];

    public function ManagerLog()
    {
        return $this->belongsTo(ManagerLog::class);
    }
}
