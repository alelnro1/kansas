<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CargoUsuario extends Model {

	protected $table = 'cargo_usuario';
	public $timestamps = true;

	public function Usuario()
	{
		return $this->hasOne('User');
	}

	public function Sucursal()
	{
		return $this->hasOne('Sucursal');
	}

	public function Cargo()
	{
		return $this->hasOne('Cargo');
	}

}