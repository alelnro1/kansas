<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoSucursal extends Model {

	protected $table = 'documento_sucursal';
	public $timestamps = true;

}