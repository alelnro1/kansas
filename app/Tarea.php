<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    protected $table = 'tareas';

    protected $fillable = [
        'nombre', 'descripcion', 'estado_id', 'sucursal_id'
    ];

    public function Sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }
}
