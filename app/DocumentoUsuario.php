<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoUsuario extends Model {

	protected $table = 'documento_usuario';
	public $timestamps = true;

}