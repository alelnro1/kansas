<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoLectura extends Model {

	protected $table = 'documento_lectura';
	public $timestamps = true;

	public $fillable = [
		'documento_id', 'usuario_id', 'sucursal_id', 'cargo_id'
	];

	public function Documento()
	{
		return $this->belongsTo(Documento::class);
	}

	public function Usuario()
	{
		return $this->belongsTo(User::class);
	}

	public function Sucursal()
	{
		return $this->belongsTo(Sucursal::class);
	}

	public function Cargo()
	{
		return $this->belongsTo(Cargo::class);
	}

}