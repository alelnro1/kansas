<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Cargo extends Model
{
    use SoftDeletes;

    protected $table = 'cargos';

    protected $fillable = [
        'nombre', 'padre_id', 'manager_log', 'empresa_id'
    ];

    protected $guarded = ['delete_at'];

    /* Mutators */
    public function setPadreIdAttribute($value) {
        $this->attributes['padre_id'] = $value ?: null;
    }

    public function puedeVerManagerLog()
    {
        return $this->manager_log;
    }

    public function CargoPadre()
    {
        return $this->hasOne(Cargo::class, 'id', 'padre_id');//->where('empresa_id', Auth::user()->empresa_id);
    }

    public function Usuarios()
    {
        //return $this->belongsToMany(User::class, )
    }

    public function CargosHijos()
    {
        return $this->hasMany(Cargo::class, 'padre_id', 'id');//->where('empresa_id', Auth::user()->empresa_id);
    }

    public function todosLosCargosHijos()
    {
        return $this->CargosHijos()->with('todosLosCargosHijos');
    }

    public function DocumentosLecturas()
    {
        return $this->hasMany(DocumentoLectura::class, 'cargo_id');
    }

    public function Empresa()
    {
        return $this->belongsTo(Empresa::class);
    }
}
