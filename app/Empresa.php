<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresa extends Model
{
    // Soft Deletes
    use SoftDeletes;

    // Atributos
    protected $table = "empresas";
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'razon_social', 'cuit', 'domicilio', 'logo'
    ];

    // Metodos
    public function Usuarios()
    {
        return $this->hasMany(User::class);
    }

    public function Documentos()
    {
        return $this->hasMany(Documento::class);
    }

    public function Cargos()
    {
        return $this->hasMany(Cargo::class);
    }

    public function Sucursales()
    {
        return $this->hasMany(Sucursal::class);
    }

    public function ManagerLogs()
    {
        return $this->hasMany(ManagerLog::class);
    }
}
