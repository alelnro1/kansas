<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CargoSucursalUsuario extends Model
{
    public $table = 'cargo_sucursal_usuario';

    public function Usuarios()
    {
        return $this->hasMany(User::class, 'id', 'usuario_id');
    }

    public function Cargos()
    {
        return $this->hasMany(Cargo::class, 'id', 'cargo_id');
    }

    public function Sucursales()
    {
        return $this->hasMany(Sucursal::class, 'id', 'sucursal_id');
    }
}
