@extends('layouts.app')

@section('site-name', 'Sucursales')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">

@stop

@section('content')
    <div class="panel-heading">
        Sucursales

        <div style="float:right;">
            <a href="/sucursales/create">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('sucursal_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('sucursal_eliminado') }}
            </div>
        @endif

            @if(Session::has('sucursal_actualizada'))
                <div class="alert alert-success">
                    {{ Session::get('sucursal_actualizada') }}
                </div>
            @endif

        @if(Session::has('sucursal_creado'))
            <div class="alert alert-success">
                {{ Session::get('sucursal_creado') }}
            </div>
        @endif

        @if (count($sucursales) > 0)
            <table class="table table-striped task-table" id="sucursales">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Ubicación</th>
                        <th>Teléfono</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($sucursales as $sucursal)
                    <tr>
                        <td>{{ $sucursal->nombre }}</td>
                        <td style="width:40%">{{ $sucursal->domicilio }}</td>
                        <td>{{ $sucursal->telefono }}</td>

                        <td>
                            @if(Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin())
                            <a href="/sucursales/{{ $sucursal['id'] }}/edit">Editar</a>
                            |
                            <a href="/sucursales/{{ $sucursal['id'] }}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar a sucursal con nombre {{ $sucursal->nombre }}?">
                                Eliminar
                            </a>
                            @else
                                <a href="/sucursales/{{ $sucursal['id'] }}">Ver</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay sucursales
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.1.2/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/sucursales/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/sucursales/delete-link.js') }}"></script>
@stop