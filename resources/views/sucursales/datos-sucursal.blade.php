@if(Session::has('sucursal_actualizado'))
    <div class="alert alert-success">
        {{ Session::get('sucursal_actualizado') }}
    </div>
@endif

<table class="table table-striped task-table" style="width: 60%;">
    <tr>
        <td><strong>Nombre</strong></td>
        <td>{{ $sucursal->nombre }}</td>
    </tr>

    <tr>
        <td><strong>Descripción</strong></td>
        <td>{{ $sucursal->descripcion }}</td>
    </tr>

    <tr>
        <td><strong>Domicilio</strong></td>
        <td>{{ $sucursal->domicilio }}</td>
    </tr>

    <tr>
        <td><strong>Email</strong></td>
        <td>{{ $sucursal->email }}</td>
    </tr>

    <tr>
        <td><strong>Teléfono</strong></td>
        <td>{{ $sucursal->telefono }}</td>
    </tr>

    @if($sucursal->archivo != "")
        <tr>
            <td><strong>Imagen</strong></td>
            <td><img src="/{{ $sucursal->archivo }}" width="250" /></td>
        </tr>
    @endif

    <tr>
        <td><strong>Fecha Creación</strong></td>
        <td>{{ $sucursal->created_at }}</td>
    </tr>
</table>