@extends('layouts.app')

@section('site-name', 'Sucursal ' . $sucursal->nombre)

@section('content')
    <div class="panel-heading">
        Sucursal con nombre <b><i>{{ $sucursal->nombre }}</i></b>
    </div>

    <div class="panel-body">
        <fieldset>
            <legend>Datos</legend>

            @include('sucursales.datos-sucursal')
        </fieldset>
    </div>

    @if(Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin())
        <div>
            <a href="/sucursales/{{ $sucursal->id }}/edit">Editar</a>
        </div>
    @endif

    <div>
        <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
    </div>
@stop
