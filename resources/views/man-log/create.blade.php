@extends('layouts.app')

@section('site-name', 'Nuevo manager log')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo</div>

    <div class="panel-body">
        @if (!Auth::user()->esUserSoloTareas())
            <fieldset>
                <legend>Campos Específicos</legend>

                @include('man-log.campos-especificos')
            </fieldset>
        @endif

        @if (!Auth::user()->esUserSoloTareas())
            <fieldset>
                <legend>Actividades</legend>

                @include('actividades.listar')
            </fieldset>
        @endif

        <fieldset>
            <legend>Tareas</legend>

            @include('tareas.listar-tareas-pendientes')
        </fieldset>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
@stop