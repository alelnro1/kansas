<form action="/man-log/{{ $manager_log->sucursal_id }}/actualizar-campos" method="POST" class="form-horizontal" id="campos-form">
    <table class="table table-responsive table-bordered">
        <tbody>
            <!-- Caja Apertura -->
            <tr>
                <td class="text-right"><label>Caja Apertura</label></td>
                <td class="col-xs-2">
                    <input type="text" class="form-control common-man-log" name="caja_apertura" value="{{ $manager_log->caja_apertura }}">
                </td>
                <td>
                    <select name="manager_apertura" class="form-control">
                        <option value="0">Seleccionar un Manager Apertura...</option>

                        @foreach ($usuarios as $usuario)
                            <option value="{{ $usuario->id }}" @if($usuario->id == $manager_log->manager_apertura) selected @endif>{{ $usuario->nombre }} {{ $usuario->apellido }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <!-- Caja Mediod�a -->
            <tr>
                <td class="text-right"><label>Caja Mediod�a</label></td>
                <td class="col-xs-2">
                    <input type="text" class="form-control common-man-log" name="caja_mediodia" value="{{ $manager_log->caja_mediodia }}">
                </td>
                <td>
                    <select name="manager_mediodia" class="form-control">
                        <option value="0">Seleccionar un Manager Mediod�a...</option>

                        @foreach ($usuarios as $usuario)
                            <option value="{{ $usuario->id }}" @if($usuario->id == $manager_log->manager_mediodia) selected @endif>{{ $usuario->nombre }} {{ $usuario->apellido }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <!-- Caja Cierre -->
            <tr>
                <td class="text-right"><label>Caja Cierre</label></td>
                <td class="col-xs-2">
                    <input type="text" class="form-control common-man-log" name="caja_cierre" value="{{ $manager_log->caja_cierre }}">
                </td>
                <td>
                    <select name="manager_cierre" class="form-control">
                        <option value="0">Seleccionar un Manager Cierre...</option>

                        @foreach ($usuarios as $usuario)
                            <option value="{{ $usuario->id }}" @if($usuario->id == $manager_log->manager_cierre) selected @endif>{{ $usuario->nombre }} {{ $usuario->apellido }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
        </tbody>
    </table>
    {{ csrf_field() }}

    <table class="table table-responsive table-bordered">
        <thead>
            <tr>
                <th></th>
                <th>Neto</th>
                <th>Bruto</th>
            </tr>

            <!-- Descuento Empleados 30% -->
            <tr>
                <td class="text-right">Descuento Empleados 30%</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="descuento_empleados_30_neto" id="descuento_empleados_30_neto" value="{{ $manager_log->descuento_empleados_30_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="descuento_empleados_30_neto_bruto" disabled>
                </td>
            </tr>

            <!-- Descuento Empleados 100% -->
            <tr>
                <td class="text-right">Descuento Empleados 100%</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="descuento_empleados_100_neto" id="descuento_empleados_100_neto" value="{{ $manager_log->descuento_empleados_100_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="descuento_empleados_100_neto_bruto" disabled>
                </td>
            </tr>

            <!-- RR.PP. MGR. BEBIDAS -->
            <tr>
                <td class="text-right">RR.PP. MGR. BEBIDA</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="rrpp_mgr_bebidas_neto" id="rrpp_mgr_bebidas_neto" value="{{ $manager_log->rrpp_mgr_bebidas_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="rrpp_mgr_bebidas_neto_bruto" disabled>
                </td>
            </tr>

            <!-- RR.PP. MALO COCINA -->
            <tr>
                <td class="text-right">RR.PP. MALO COCINA</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="rrpp_malo_cocina_neto" id="rrpp_malo_cocina_neto" value="{{ $manager_log->rrpp_malo_cocina_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="rrpp_malo_cocina_neto_bruto" disabled>
                </td>
            </tr>

            <!-- RR.PP. MALO RECEPCION -->
            <tr>
                <td class="text-right">RR.PP. MALO RECEPCION</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="rrpp_malo_recepcion_neto" id="rrpp_malo_recepcion_neto" value="{{ $manager_log->rrpp_malo_recepcion_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="rrpp_malo_recepcion_neto_bruto" disabled>
                </td>
            </tr>

            <!-- RR.PP. MALO S.V.C. -->
            <tr>
                <td class="text-right">RR.PP. MALO S.V.C.</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="rrpp_malo_scv_neto" id="rrpp_malo_scv_neto" value="{{ $manager_log->rrpp_malo_scv_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="rrpp_malo_scv_neto_bruto" disabled>
                </td>
            </tr>

            <!-- RR.PP. MALO VALET -->
            <tr>
                <td class="text-right">RR.PP. MALO VALET</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="rrpp_malo_valet_neto" id="rrpp_malo_valet_neto" value="{{ $manager_log->rrpp_malo_valet_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="rrpp_malo_valet_neto_bruto" disabled>
                </td>
            </tr>

            <!-- RR.PP. MALO CLIENTE -->
            <tr>
                <td class="text-right">RR.PP. MALO CLIENTE</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="rrpp_malo_cliente_neto" id="rrpp_malo_cliente_neto" value="{{ $manager_log->rrpp_malo_cliente_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="rrpp_malo_cliente_neto_bruto" disabled>
                </td>
            </tr>

            <!-- RR.PP. MALO EXPO -->
            <tr>
                <td class="text-right">RR.PP. MALO EXPO</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="rrpp_malo_expo_neto" id="rrpp_malo_expo_neto" value="{{ $manager_log->rrpp_malo_expo_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="rrpp_malo_expo_neto_bruto" disabled>
                </td>
            </tr>

            <!-- RR.PP. MALO MOZO -->
            <tr>
                <td class="text-right">RR.PP. MALO MOZO</td>

                <td>
                    <input type="text" class="form-control common-man-log" name="rrpp_malo_mozo_neto" id="rrpp_malo_mozo_neto" value="{{ $manager_log->rrpp_malo_mozo_neto }}">
                </td>

                <td>
                    <input type="text" class="form-control common-man-log" id="rrpp_malo_mozo_neto_bruto" disabled>
                </td>
            </tr>
        </thead>
    </table>


    <!-- VENTAS -->
    <div class="form-group{{ $errors->has('ventas') ? ' has-error' : '' }}">
        <label class="col-md-5 control-label">VENTAS</label>

        <div class="col-md-6">
            <input type="text" class="form-control common-man-log" name="ventas" id="ventas" value="{{ $manager_log->ventas }}">

            @if ($errors->has('ventas'))
                <span class="help-block">
                    <strong>{{ $errors->first('ventas') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <!-- GUEST -->
    <div class="form-group{{ $errors->has('guest') ? ' has-error' : '' }}">
        <label class="col-md-5 control-label">GUEST</label>

        <div class="col-md-6">
            <input type="text" class="form-control common-man-log" name="guest" id="guest" value="{{ $manager_log->guest }}">

            @if ($errors->has('guest'))
                <span class="help-block">
                    <strong>{{ $errors->first('guest') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <!-- GUEST AVG. -->
    <div class="form-group{{ $errors->has('guest_avg') ? ' has-error' : '' }}">
        <label class="col-md-5 control-label">GUEST AVG.</label>

        <div class="col-md-6">
            <input type="text" class="form-control common-man-log" name="guest_avg" id="guest_avg" value="{{ $manager_log->guest_avg }}" readonly>

            @if ($errors->has('guest_avg'))
                <span class="help-block">
                    <strong>{{ $errors->first('guest_avg') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <!-- PROYECTADO -->
    <div class="form-group{{ $errors->has('proyectado') ? ' has-error' : '' }}">
        <label class="col-md-5 control-label">PROYECTADO</label>

        <div class="col-md-6">
            <input type="text" class="form-control common-man-log" name="proyectado" value="{{ $manager_log->proyectado }}">

            @if ($errors->has('proyectado'))
                <span class="help-block">
                    <strong>{{ $errors->first('proyectado') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <!-- CAMBIO -->
    <div class="form-group{{ $errors->has('cambio') ? ' has-error' : '' }}">
        <label class="col-md-5 control-label">CAMBIO</label>

        <div class="col-md-6">
            <input type="text" class="form-control common-man-log" name="cambio" value="{{ $manager_log->cambio }}">

            @if ($errors->has('cambio'))
                <span class="help-block">
                    <strong>{{ $errors->first('cambio') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <!-- PROSEGUR -->
    <div class="form-group{{ $errors->has('prosegur') ? ' has-error' : '' }}">
        <label class="col-md-5 control-label">PROSEGUR</label>

        <div class="col-md-6">
            <input type="text" class="form-control common-man-log" name="prosegur" value="{{ $manager_log->prosegur }}">

            @if ($errors->has('prosegur'))
                <span class="help-block">
                    <strong>{{ $errors->first('prosegur') }}</strong>
                </span>
            @endif
        </div>
    </div>

    @if($manager_log->cerrado == 0)
        <div class="form-group">
            <div class="col-md-7 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user"></i>&nbsp;Actualizar
                </button>
            </div>
        </div>
    @endif
</form>