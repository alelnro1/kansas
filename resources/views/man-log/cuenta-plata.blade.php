@if (count($denominaciones) > 0)
    <form action="{{ url('man-log/actualizar-cuenta-plata') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="ml_act" value="@if(isset($manager_log) && $manager_log){{ $manager_log->id }}@endif" />

        <table class="table table-striped task-table">
            <thead>
                <tr>
                    <td class="col-xs-4"><strong>Nombre</strong></td>
                    <td><strong>Cantidad</strong></td>
                    <td><strong>Total</strong></td>
                </tr>
            </thead>

            <tbody>
                {{-- */ $total = 0; /* --}}
                @foreach ($denominaciones as $denominacion)
                    {{-- */ $total = $total + $denominacion->cantidad * $denominacion->valor; /* --}}
                    <tr>
                        <td>{{ $denominacion->nombre }}</td>
                        <td>
                            <input type="number"
                                   class="form-control"
                                   style="width:150px"
                                   name="{{ $denominacion->nombre }}"
                                   @if (isset($denominacion->cantidad))
                                    value="{{ $denominacion->cantidad }}"
                                   @else
                                     value="0"
                                   @endif>
                        </td>
                        <td class="text-right">
                            ${{ number_format($denominacion->cantidad * $denominacion->valor, 2) }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="text-right">
            Total en Caja: ${{ number_format($plata_en_caja, 2) }}
        </div>

        <div class="form-group">
            <div class="col-md-7 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-wrench"></i>&nbsp;Actualizar
                </button>
            </div>
        </div>
    </form>
@else
    No hay denominaciones
@endif