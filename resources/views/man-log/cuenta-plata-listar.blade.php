@if (count($denominaciones) > 0)
        <table class="table table-striped task-table">
            <thead>
            <tr>
                <td class="col-xs-4"><strong>Nombre</strong></td>
                <td class="text-right"><strong>Cantidad</strong></td>
                <td class="text-right"><strong>Total</strong></td>
            </tr>
            </thead>

            <tbody>
            {{-- */ $plata_en_caja = 0; /* --}}
            @foreach ($denominaciones as $denominacion)
                {{-- */ $plata_en_caja = $plata_en_caja + $denominacion->pivot->cantidad * $denominacion->valor; /* --}}
                <tr>
                    <td>{{ $denominacion->nombre }}</td>
                    <td class="text-right">
                        @if (isset($denominacion->pivot->cantidad))
                            {{ $denominacion->pivot->cantidad }}
                        @else
                            0
                        @endif
                    </td>
                    <td class="text-right">
                        ${{ number_format($denominacion->pivot->cantidad * $denominacion->valor, 2) }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-right">
            Total en Caja: ${{ number_format($plata_en_caja, 2) }}
        </div>
@else
    No hay denominaciones
@endif