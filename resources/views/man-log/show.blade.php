@extends('layouts.app')

@section('site-name', 'Manager Log Histórico ' . $sucursal->nombre)

@section('styles')
    <style>
        label {
            font-weight: 300;
        }
    </style>
@stop


@section('content')
    <div class="panel-heading">
        Manager Log de la sucursal <b><i>{{ $sucursal->nombre }}</i></b> del <b>{{ $manager_log->created_at }}</b>
        @if(isset($manager_log->fecha_cierre)&& $manager_log->fecha_cierre != null)
            cerrado el {{ $manager_log->fecha_cierre }}
        @endif
    </div>

    <div class="panel-body">
        <div id="accordion">
            <h3>Datos</h3>
            <fieldset>
                @include('man-log.campos-especificos')
            </fieldset>

            <h3>Tareas Pendientes</h3>
            <fieldset>
                @include('tareas.listar-tareas-pendientes', array('sucursal' => $sucursal))
            </fieldset>

            <h3>Tareas Realizadas</h3>
            <fieldset>
                @include('tareas.listar-tareas-realizadas', array('sucursal' => $sucursal))
            </fieldset>

            <h3>Actividades</h3>
            <fieldset>
                @include('actividades.listar')
            </fieldset>

            <h3>Cuenta Plata</h3>
            <fieldset>
                @include('man-log.cuenta-plata-listar')
            </fieldset>
        </div>

        <div>
            <br><br>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop


@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{ asset('/js/tareas/delete-link.js') }}"></script>

    <script type="text/javascript">
        $(function() {
            $( "#accordion" ).accordion({
                active: false,
                collapsible: true,
                heightStyle: "content"
            });
        });
    </script>
@stop
