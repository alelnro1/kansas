@extends('layouts.app')

@section('site-name', 'Manager Log - ' . $sucursal->nombre)

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">

    <style>
        label {
            font-weight: 300;
        }

        input.common-man-log {
            text-align: right;
        }
    </style>
@stop

@section('content')
    <div class="panel-heading">Manager Log de <strong>{{ $sucursal->nombre }}</strong></div>

    <div class="panel-body">
        @if(Session::has('tarea_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('tarea_eliminado') }}
            </div>
        @endif

        @if(Session::has('tarea_creado'))
            <div class="alert alert-success">
                {{ Session::get('tarea_creado') }}
            </div>
        @endif

        @if(Session::has('campos_actualizados'))
            <div class="alert alert-success">
                {{ Session::get('campos_actualizados') }}
            </div>
        @endif

        @if(Session::has('valores_numericos'))
            <div class="alert alert-danger">
                {{ Session::get('valores_numericos') }}
            </div>
        @endif


        <fieldset>
            <legend>De Hoy</legend>

            @if ($manager_log)
                <p>
                    Manager log creado el {{ date("d/m/Y", strtotime($manager_log->created_at)) }}
                    a las {{ date("H:i", strtotime($manager_log->created_at)) }}
                    @if($manager_log->updated_at)
                        y modificado por última vez a las {{ date("H:i", strtotime($manager_log->updated_at)) }}
                    @endif
                </p>

                <div id="accordion">
                    @if (!Auth::user()->esUserSoloTareas())
                        <h3>Datos</h3>
                        <fieldset>
                            @include('man-log.campos-especificos')
                        </fieldset>
                    @endif

                    <h3>Tareas Pendientes</h3>
                    <fieldset>
                        @include('tareas.listar-tareas-pendientes', array('sucursal' => $sucursal))
                    </fieldset>

                    <h3>Tareas Realizadas</h3>
                    <fieldset>
                        @include('tareas.listar-tareas-realizadas', array('sucursal' => $sucursal))
                    </fieldset>

                    @if (!Auth::user()->esUserSoloTareas())
                        <h3>Actividades</h3>
                        <fieldset>
                            @include('actividades.listar')
                        </fieldset>

                        <h3>Cuenta Plata</h3>
                        <fieldset>
                            @include('man-log.cuenta-plata')
                        </fieldset>
                    @endif
                </div>

               {{-- @if (!Auth::user()->esUserSoloTareas())
                    <div style="float:right;margin-top:10px;">
                        <button id="cerrar" class="form-control" data-sucursal-id="{{ $manager_log->sucursal_id }}" data-manager-log-id="{{ $manager_log->id }}" data-confirm="Esta seguro que desea cerrar el Manager Log del {{ date("d/m", strtotime($manager_log->created_at)) }}?">Cerrar</button>
                    </div>
                @endif--}}
            @else
                @if (!Auth::user()->esUserSoloTareas())
                    <p>Aún no se creó. <a href="{{ url('man-log/' . $sucursal->id . '/create') }}">Crear</a></p>
                @endif
            @endif
            <br><br>
        </fieldset>

        @if (!Auth::user()->esUserSoloTareas())
            <fieldset>
                <legend>Histórico</legend>

                @if(count($manager_logs_historicos) > 0)
                    <table class="table table-striped task-table" id="manager_logs_historico">
                        <!-- Table Headings -->
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Última modificación</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($manager_logs_historicos as $manager_log_historico)
                            <tr>
                                <td>{{ date("d/m/Y", strtotime($manager_log_historico->created_at)) }}</td>
                                <td>
                                    @if($manager_log_historico->updated_at != null)
                                        {{ $manager_log_historico->updated_at }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td><a href="{{ url('man-log/' . $sucursal->id . '/show/' .  $manager_log_historico->id) }}">Ver</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    La sucursal no tiene Manager Logs históricos
                @endif
            </fieldset>
        @endif

        <div>
            <br>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{ asset('/js/tareas/delete-link.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $( "#accordion" ).accordion({
                active: false,
                collapsible: true,
                heightStyle: "content"
            });

            $('#cerrar').on('click', function(){
                if(confirm($(this).data('confirm'))){
                    var manager_log_id = $(this).data('manager-log-id'),
                            sucursal_id    = $(this).data('sucursal-id');

                    $.ajax({
                        url: 'cerrar/' + manager_log_id,
                        dataType: 'json',
                        success: function(data){
                            if (data.valid == true){
                                window.location.href = '/man-log/' + sucursal_id;
                            }
                        }
                    })
                }
            });

            // Manejo el calculo de importes brutos
            var campos = ['#descuento_empleados_30_neto', '#descuento_empleados_100_neto', '#rrpp_mgr_bebidas_neto', '#rrpp_malo_cocina_neto', '#rrpp_malo_recepcion_neto', '#rrpp_malo_scv_neto', '#rrpp_malo_valet_neto', '#rrpp_malo_cliente_neto', '#rrpp_malo_expo_neto', '#rrpp_malo_mozo_neto'];

            // Calculo de brutos
            $('#descuento_empleados_30_neto, #descuento_empleados_100_neto, #rrpp_mgr_bebidas_neto, #rrpp_malo_cocina_neto, #rrpp_malo_recepcion_neto, #rrpp_malo_scv_neto, #rrpp_malo_valet_neto, #rrpp_malo_cliente_neto, #rrpp_malo_expo_neto, #rrpp_malo_mozo_neto')
                    .on('change',
                            function(){
                                var bruto = $(this).val() * 1.21,
                                        nombre_input = $(this).attr('id');

                                $('#' + nombre_input + "_bruto").val(bruto.toFixed(2));
                            }
                    );

            $.each(campos, function(index, element){
                var campo = $(element);

                cargarBrutos(campo);
            });

            // Calculo del GUEST AVG
            $('#ventas, #guest').on('change', function(){
                var ventas = parseFloat($('#ventas').val()),
                        guest = parseFloat($('#guest').val());

                // Valido que los valores de ventas y guest no esten vacios
                if (ventas != "" && guest != "") {
                    $('#guest_avg').val(ventas/guest);
                }
            });

            $('.common-man-log').on('change', function(){
                var valor_con_decimales = ponerDosDecimales($(this).val());
                $(this).val(valor_con_decimales);
            }).each(function(){
                var valor_con_decimales = ponerDosDecimales($(this).val());
                $(this).val(valor_con_decimales);
            });

            function cargarBrutos(campo) {
                var bruto = campo.val() * 1.21,
                        nombre_input = campo.attr('id');

                $('#' + nombre_input + "_bruto").val(bruto.toFixed(2));
            }

            function ponerDosDecimales(valor) {
                return parseFloat(valor).toFixed(2);
            }

            oTable = $('#actividades').DataTable({
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                aLengthMenu: [
                    [2, 5, 10],
                    [2, 5, 10]
                ],
                "language": {
                    "info": "Mostrando _START_ a _END_ de _TOTAL_",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "lengthMenu": "Mostrar _MENU_",
                    "search": "Buscar:",
                    "infoFiltered": "(de un total de _MAX_)",
                }
            });
        });
    </script>
@stop