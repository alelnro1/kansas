@extends('layouts.app')

@section('site-name', 'Manager Log')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Seleccionar sucursal</div>

    <div class="panel-body">
        <p>Antes de proceder, seleccione una sucursal para poder visualizar el manager log correspondiente</p>
        
        <ul>
            @foreach ($cargos_sucursales_usuarios as $cargo_sucursal_usuario)
                @foreach($cargo_sucursal_usuario->cargos as $cargo)
                    @if($cargo->manager_log == '1' || Auth::user()->esUserSoloTareas())
                        @foreach($cargo_sucursal_usuario->sucursales as $sucursal)
                            @if($sucursal->id == $cargo_sucursal_usuario->sucursal_id)
                                <li><a href="{{ url('man-log/' . $sucursal->id) }}">{{ $sucursal->nombre }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endforeach


        </ul>
    </div>
@stop