@extends('layouts.app')

@section('site-name', 'Viendo a denominacion')

@section('content')
    <div class="panel-heading">
        Denominacion con nombre <b><i>{{ $denominacion->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('denominacion_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('denominacion_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 60%;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $denominacion->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Valor</strong></td>
                <td>${{ number_format($denominacion->valor, 2) }}</td>
            </tr>
        </table>

        <div>
             <a href="/denominaciones/{{ $denominacion->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
