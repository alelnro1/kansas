@extends('layouts.app')

@section('site-name', 'Listando denominaciones')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Denominaciones

        <div style="float:right;">
            <a href="/denominaciones/create" class="btn btn-xs btn-default">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('denominacion_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('denominacion_eliminado') }}
            </div>
        @endif

        @if(Session::has('denominacion_creado'))
            <div class="alert alert-success">
                {{ Session::get('denominacion_creado') }}
            </div>
        @endif

        @if (count($denominaciones) > 0)
            <table class="table table-striped task-table" id="denominaciones">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Valor</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($denominaciones as $denominacion)
                    <tr>
                        <td>{{ $denominacion->nombre }}</td>
                        <td>{{ $denominacion->valor }}</td>

                        <td>
                            <a href="/denominaciones/{{ $denominacion['id'] }}">Ver</a>
                            |
                            <a href="/denominaciones/{{ $denominacion['id'] }}/edit">Editar</a>
                            |
                            <a href="/denominaciones/{{ $denominacion['id'] }}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar a denominacion con nombre {{ $denominacion->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay denominaciones
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/denominaciones/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/denominaciones/delete-link.js') }}"></script>
@stop