@extends('layouts.app')

@section('site-name', 'Editar denominación')

@section('content')
    <div class="panel-heading">Denominación</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="/denominaciones/{{ $denominacion->id }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $denominacion->nombre }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Valor -->
            <div class="form-group{{ $errors->has('valor') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Valor</label>

                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" class="form-control" name="valor" value="{{ $denominacion->valor }}">
                    </div>

                    @if ($errors->has('valor'))
                        <span class="help-block">
                            <strong>{{ $errors->first('valor') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-wrench"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop