@extends('layouts.app')

@section('site-name', 'Mi Perfil')

@section('content')
    <div class="panel-heading">
        Ver Usuario
    </div>

    <div class="panel-body">
        @if(Session::has('perfil_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('perfil_actualizado') }}
            </div>
        @endif

        @if($usuario->archivo != "")
            <div class="form-group text-center">
                <img src="../{{ $usuario->archivo }}" width="250">
            </div>
        @endif

        <fieldset>

            <legend>Datos</legend>
                <table class="table table-striped task-table">
                    <tr>
                        <td><strong>Nombre</strong></td>
                        <td>{{ $usuario->nombre }}</td>
                    </tr>

                    <tr>
                        <td><strong>Apellido</strong></td>
                        <td>{{ $usuario->apellido }}</td>
                    </tr>

                    <tr>
                        <td><strong>Email</strong></td>
                        <td>{{ $usuario->email }}</td>
                    </tr>

                    <tr>
                        <td><strong>Telefono</strong></td>
                        <td>{{ $usuario->telefono }}</td>
                    </tr>

                    <tr>
                        <td><strong>Fecha de alta</strong></td>
                        <td>{{ date("d/m/Y", strtotime($usuario->created_at)) }}</td>
                    </tr>
                </table>
        </fieldset>

        <div>
            <br>
            <a href="/perfil/edit">Editar</a>
            <br>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

