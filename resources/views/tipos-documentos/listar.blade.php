@extends('layouts.app')

@section('site-name', 'Listando tiposDocumentos')


@section('content')
    <div class="panel-heading">
        Tipos de Documentos

        <div style="float:right;">
            <a href="/tipos-documentos/create" class="btn btn-xs btn-default">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('tipoDocumento_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('tipoDocumento_eliminado') }}
            </div>
        @endif

        @if(Session::has('tipoDocumento_creado'))
            <div class="alert alert-success">
                {{ Session::get('tipoDocumento_creado') }}
            </div>
        @endif

            @if(Session::has('aun_hay_documentos_del_tipo'))
                <div class="alert alert-danger">
                    {{ Session::get('aun_hay_documentos_del_tipo') }}
                </div>
            @endif

        @if (count($tiposDocumentos) > 0)
            <table class="table table-striped task-table" id="tiposDocumentos">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Usa Versionado</th>
                        <th>Cantidad</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($tiposDocumentos as $tipoDocumento)
                    <tr>
                        <td>{{ $tipoDocumento->nombre }}</td>
                        <td>
                            @if($tipoDocumento->versionado == false)
                                No
                            @else
                                Sí
                            @endif
                        </td>
                        <td>{{ $tipoDocumento->cantidad }}</td>

                        <td>
                            <a href="/tipos-documentos/{{ $tipoDocumento['id'] }}/edit">Editar</a>
                            |
                            <a href="/tipos-documentos/{{ $tipoDocumento['id'] }}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar al tipo de documento {{ $tipoDocumento->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay tipos de documentos
        @endif
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/tiposDocumentos/delete-link.js') }}"></script>
@stop