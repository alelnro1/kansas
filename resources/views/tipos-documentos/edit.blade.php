@extends('layouts.app')

@section('site-name', 'Editar tipoDocumento')

@section('content')
    <div class="panel-heading">Tipo de Documento</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="/tipos-documentos/{{ $tipoDocumento->id }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $tipoDocumento->nombre }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Estado -->
            <div class="form-group {{ $errors->has('versionado') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="versionado"></label>

                <div class="col-md-6">
                    <input type="checkbox" name="versionado" @if ($tipoDocumento->versionado == true) checked @endif> Tiene Versionado
                </div>
                <div style="clear:both;"></div><br>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/tiposDocumentos/edit.js') }}"></script>
@stop