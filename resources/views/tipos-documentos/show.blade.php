@extends('layouts.app')

@section('site-name', 'Viendo a tipoDocumento')

@section('content')
    <div class="panel-heading">
        TipoDocumento con nombre <b><i>{{ $tipoDocumento->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('tipoDocumento_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('tipoDocumento_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 60%;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $tipoDocumento->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripcion</strong></td>
                <td>{{ $tipoDocumento->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>{{ $tipoDocumento->estado }}</td>
            </tr>

            <tr>
                <td><strong>Fecha</strong></td>
                <td>{{ date('Y/m/d', strtotime($tipoDocumento->fecha)) }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $tipoDocumento->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $tipoDocumento->email }}</td>
            </tr>

            <tr>
                <td><strong>Telefono</strong></td>
                <td>{{ $tipoDocumento->telefono }}</td>
            </tr>

            <tr>
                <td><strong>Archivo</strong></td>
                <td><img src="/{{ $tipoDocumento->archivo }}" height="250" /></td>
            </tr>

            <tr>
                <td><strong>Fecha Creacion</strong></td>
                <td>{{ $tipoDocumento->created_at }}</td>
            </tr>
        </table>

        <div>
             <a href="/tiposDocumentos/{{ $tipoDocumento->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
