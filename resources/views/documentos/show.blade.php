@extends('layouts.app')

@section('site-name', 'Viendo a documento')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Documento con nombre <b><i>{{ $documento->nombre }}</i></b>
    </div>

    <div class="panel-body">
        <fieldset>
            <legend>Datos</legend>

            <table class="table table-striped task-table" style="width: 60%;">
                <tr>
                    <td><strong>Nombre</strong></td>
                    <td>{{ $documento->nombre }}</td>
                </tr>

                <tr>
                    <td><strong>Descripcion</strong></td>
                    <td>{{ $documento->descripcion }}</td>
                </tr>

                <tr>
                    <td><strong>Estado</strong></td>
                    <td>{{ $documento->estado->nombre }}</td>
                </tr>

                <tr>
                    <td><strong>Doc Tipo</strong></td>
                    <td>{{ $documento->Tipo->nombre }} {{ $documento->nro_identificador }}</td>
                </tr>

                @if (isset($documento->UsuarioAprobador))
                    <tr>
                        <td><strong>Usuario aprobador</strong></td>
                        <td>
                            <a href="{{ url('/usuarios/' . $documento->UsuarioAprobador->id) }}">
                                {{ $documento->UsuarioAprobador->nombre }} {{ $documento->UsuarioAprobador->apellido }}
                            </a>
                        </td>
                    </tr>
                @endif

                <tr>
                    <td><strong>Fecha Creación</strong></td>
                    <td>{{ $documento->created_at }}</td>
                </tr>

                @if($documento->autor_id == Auth::user()->id && $documento->comentario != "")
                    <tr>
                        <td><strong>Comentario Aprobador</strong></td>
                        <td>{{ $documento->comentario }}</td>
                    </tr>
                @endif
            </table>
        </fieldset>

        <fieldset>
            <legend>Parámetros</legend>
            @if(count($parametros) <= 0)
                El documento no tiene parámetros asignados
            @else
                <table class="table table-striped task-table" id="parametros">
                    <thead>
                    <tr>
                        <th>Sucursal</th>
                        <th>Cargo</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($parametros as $parametro)
                        <tr>
                            <td>{{ $parametro->sucursal->nombre }}</td>
                            <td>{{ $parametro->cargo->nombre }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
            <br>
        </fieldset>

        <fieldset>
            <legend>Destinatarios</legend>
            @if(count($destinatarios) <= 0)
                El documento no tiene destinatarios
            @else
                <table class="table table-striped task-table" id="destinatarios">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Leído</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($destinatarios as $destinatario)
                        <tr>
                            <td>
                                <a href="{{ url('usuarios/' . $destinatario->usuario->id) }}">{{ $destinatario->usuario->nombre }} {{ $destinatario->usuario->apellido }}</a>
                            </td>

                            <td>
                                <strong>
                                    @if($destinatario->leido == 0)
                                        No
                                    @else
                                        Sí
                                    @endif
                                </strong>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </fieldset>

        <fieldset>
            <legend>Archivos</legend>

            @if(count($documento->archivos) <= 0)
                No hay archivos
            @else
                @foreach($documento->archivos as $archivo)
                    <div>
                        <a href="../{{ $archivo->url }}" target="_blank">{{ $archivo->nombre }}</a>
                    </div>
                @endforeach
            @endif
            <br><br>
        </fieldset>

        <fieldset>
            <legend>Versiones Anteriores</legend>

            @if(count($documento->DocumentosPadres) <= 0)
                El documento no tiene versiones anteriores
            @else
                <table class="table table-striped task-table" id="versiones_anteriores">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Fecha Creación</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($documento->DocumentosPadres as $version)
                        <tr>
                            <td>
                                <a href="{{ url('documentos/' . $version->id) }}">{{ $version->nombre }}</a>
                            </td>

                            <td>
                                {{ date("d/m/Y H:m", strtotime($version->created_at)) }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </fieldset>

        <hr>
        <div><br>
            @if($documento->user_aprobador_id == Auth::user()->id && $documento->estado->nombre == "Pendiente de Aprobación")
                <div class="form-group">
                    <div class="left col-md-10">
                        <button type="submit" class="btn btn-danger right" id="desaprobar-documento">
                            <i class="fa fa-btn fa-remove"></i>Desaprobar
                        </button>
                    </div>

                    <div class="right">
                        <button type="submit" class="btn btn-success left" id="aprobar-documento" data-documento-id="{{ $documento->id }}">
                            <i class="fa fa-btn fa-check"></i>Aprobar
                        </button>
                    </div>
                </div>
            @elseif($documento->estado->nombre == "Borrador" && $documento->autor_id == Auth::user()->id)
                <a href="/documentos/{{ $documento->id }}/edit">Editar</a>
            @endif
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>

    <div id="aprobar-desaprobar-comentario">
        <p>Si lo desea, escriba un comentario para explicar su decisión</p>

        <form action="" id="form-aprobar-desaprobar">
            {{ csrf_field() }}

            <textarea name="comentario" id="comentario" rows="6" class="form-control"></textarea>
        </form>
    </div>
@stop

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        $(document).ready(function() {
            // La operacion puede ser aprobar o desaprobar
            var operacion = null, documento_id = $('#aprobar-documento').data('documento-id');

            $('#aprobar-desaprobar-comentario').dialog({
                autoOpen: false,
                title: "Comentario",
                show: "blind",
                hide: "explode",
                modal: true,
                width: 500,
                height: 400,
                buttons: {
                    "Confirmar": function(){
                        url = null;

                        if (operacion == "aprobar"){
                            url = '/documentos/' + documento_id + '/aprobar';
                        } else {
                            url = '/documentos/' + documento_id + '/desaprobar';
                        }

                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: $('#form-aprobar-desaprobar').serialize(),
                            dataType: 'json',
                            success: function(data){
                                alert(data.message);
                                window.location.href = '/documentos'
                            }
                        });
                    },
                    "Cancelar": function() {
                        dialog.dialog( "close" );
                    }
                }
            });

            oTable = $('#destinatarios').DataTable({
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                "language": {
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ destinatarios filtrados",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "lengthMenu": "Mostrar _MENU_ destinatarios",
                    "search": "Buscar:",
                    "infoFiltered": "(de un total de _MAX_ destinatarios)",
                }
            });

            oTable2 = $('#parametros').DataTable({
                responsive: true,
                "language": {
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ parámetros filtrados",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "lengthMenu": "Mostrar _MENU_ parámetros",
                    "search": "Buscar:",
                    "infoFiltered": "(de un total de _MAX_ parámetros)",
                }
            });

            $('#aprobar-documento').on('click', function(){
                operacion = "aprobar";

                $('#aprobar-desaprobar-comentario').dialog('open');
            });

            $('#desaprobar-documento').on('click', function(){
                operacion = "desaprobar";

                $('#aprobar-desaprobar-comentario').dialog('open');
            });

            // Detecto que se hizo click en el botón volver. Recargo la página para que en el listado no aparezcan más los "no leídos"
            window.history.pushState('', null, './');
            $(window).on('popstate', function() {
                document.location.href = '/documentos';

            });
        });
    </script>
@stop