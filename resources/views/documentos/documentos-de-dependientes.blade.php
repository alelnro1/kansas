@if (count($documentos_de_dependientes) > 0)
    <table class="table table-striped task-table" id="documentos" style="width: 100%;">
        <!-- Table Headings -->
        <thead>
        <tr>
            <th>fecha_completa</th>
            <th>Tags</th>
            <th>Identificador</th>
            <th>Documento</th>
            <th>Usuario</th>
            <th>Fecha del Documento</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($documentos_de_dependientes as $key => $documento)
            <tr>
                <td>{{ $documento['created_at'] }}</td>
                <td>{{ $documento['tags'] }}</td>
                <td>{{ $documento['nro_identificador'] }}</td>
                <td>
                    <a href="/documentos/{{ $documento['id'] }}">{{ $documento['nombre'] }}</a>
                </td>
                <td>
                    <a href="/usuarios/{{ $documento['usuario_id'] }}">
                        {{ $documento['usuario_nombre'] }} {{ $documento['usuario_apellido'] }}
                    </a>
                </td>
                <td>{{ date('d/m/Y', strtotime($documento['created_at'])) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    No hay documentos no leídos de dependientes
@endif