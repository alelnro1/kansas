@extends('layouts.app')

@section('site-name', 'Biblioteca de Documentos')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">
@stop

@section('content')

    <div class="panel-heading">Biblioteca de Documentos</div>

    <div class="panel-body">
        <div id="accordion">
            @foreach ($documentos_por_tipo as $key => $documento_por_tipo)
                <h3>{{ $key }}</h3>

                <table class="table table-striped task-table display no-wrap" id="documentos" style="width:100%">
                    <!-- Table Headings -->
                    <thead>
                    <tr>
                        <th>fecha_completa</th>
                        <th>descripcion</th>
                        <th>Tags</th>
                        <th>Fecha</th>
                        <th>Nombre</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($documento_por_tipo as $documento)
                        <tr>
                            <td>{{ $documento->created_at }}</td>
                            <td>{{ $documento->descripcion }}</td>
                            <td>{{ $documento->tags }}</td>
                            <td>
                                <span class="hidden">{{ date('Ymd', strtotime($documento->created_at)) }}</span>
                                {{ date('d/m/Y', strtotime($documento->created_at)) }}
                            </td>
                            <td>{{ $documento->nombre }}</td>

                            <td>
                                <a href="/documentos/{{ $documento['id'] }}">Ver</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endforeach
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/documentos/listar.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.1.2/js/dataTables.rowReorder.min.js"></script>
    <script>
        $( function() {
            $( "#accordion" ).accordion();
        } );
    </script>
@stop