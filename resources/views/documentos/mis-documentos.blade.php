@if(Session::has('documento_creado'))
    <div class="alert alert-success">
        {{ Session::get('documento_creado') }}
    </div>
@endif

@if (count($documentos_propios) > 0)
    <table class="table table-striped task-table display no-wrap" id="documentos_propios" style="width: 100%;">
        <!-- Table Headings -->
        <thead>
        <tr>
            <th>fecha_completa</th>
            <th>Tags</th>
            <th>Identificador</th>
            <th>Tipo</th>
            <th>Fecha</th>
            <th>Nombre</th>
            <th>Estado</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach ($documentos_propios as $documento)
            <tr>
                <td>{{ $documento->created_at }}</td>
                <td>{{ $documento->tags }}</td>
                <td>{{ $documento->nro_identificador }}</td>
                <td>{{ $documento->Tipo->nombre }}</td>
                <td>
                    {{ date('d/m/Y', strtotime($documento->created_at)) }}
                </td>
                <td>{{ $documento->nombre }}</td>
                <td>{{ $documento->Estado->nombre }}</td>

                <td>
                    <a href="/documentos/{{ $documento['id'] }}">Ver</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    No hay documentos propios
@endif