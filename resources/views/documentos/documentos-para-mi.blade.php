@if (count($documentos_para_mi) > 0)
    <table class="table table-striped task-table display no-wrap" id="documentos_para_mi" style="width: 100%;">
        <!-- Table Headings -->
        <thead>
        <tr>
            <th>fecha_completa</th>
            <th>Tags</th>
            <th>Identificador</th>
            <th>Tipo</th>
            <th>Fecha</th>
            <th>Nombre</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach ($documentos_para_mi as $documento)
            <tr>
                <td>{{ $documento->created_at }}</td>
                <td>{{ $documento->tags }}</td>
                <td>{{ $documento->nro_identificador }}</td>
                <td>{{ $documento->Tipo->nombre }}</td>
                <td>{{ date('d/m/Y', strtotime($documento->created_at)) }}</td>
                <td>{{ $documento->nombre }}</td>

                <td>
                    <a href="/documentos/{{ $documento['id'] }}">Ver</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    No hay documentos nuevos para leer
@endif