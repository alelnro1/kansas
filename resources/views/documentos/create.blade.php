@extends('layouts.app')

@section('site-name', 'Nuevo documento')

@section('styles')
    <link rel="stylesheet" href="{{ asset("/css/bootstrap-multiselect.css") }}" type="text/css"/>
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="../css/multiple-upload/jquery.fileupload.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo</div>

    <div class="panel-body">
        <form action="/documentos" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <input type="hidden" name="urls_archivos" value="{{ old('urls_archivos') }}">
            <input type="hidden" name="destinatarios_todos" id="destinatarios_todos" value="0">

            <!-- Tipo -->
            <div class="form-group {{ $errors->has('tipo_documento_id') ? ' has-error' : '' }}" id="tipo_documento_id">
                <label class="control-label col-md-4" for="tipo_documento_id">Tipo</label>

                <div class="col-md-6">
                    <select name="tipo_documento_id" class="form-control">
                        <option value="0">Seleccione tipo de documento...</option>

                        @foreach($tipos_documento as $tipo)
                            <option value="{{ $tipo->id }}" data-versionado="{{ $tipo->versionado }}"@if(old('tipo_documento_id') == $tipo->id) selected @endif>
                                {{ $tipo->nombre }}
                            </option>
                        @endforeach

                    </select>

                    @if ($errors->has('tipo_documento_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tipo_documento_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Es primera version -->
            <div class="form-group {{ $errors->has('primera_version') ? ' has-error' : '' }}" id="primera_version_div">
                <label class="control-label col-md-4" for="primera_version"></label>

                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" checked name="primera_version" id="primera_version_check">Es primera version
                        </label>
                    </div>
                </div>
            </div>

            <!-- Version anterior -->
            <div class="form-group {{ $errors->has('version_anterior') ? ' has-error' : '' }}" id="version_anterior">
                <label class="control-label col-md-4" for="version_anterior">Versión Anterior</label>

                <div class="col-md-6">
                    <select name="version_anterior" class="form-control">
                        <option value="0">Seleccione versión anterior...</option>

                        @foreach($documentos as $documento)
                            <option value="{{ $documento->id }}" @if(old('version_anterior') == $documento->id) selected @endif>{{ $documento->nombre }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('version_anterior'))
                        <span class="help-block">
                            <strong>{{ $errors->first('version_anterior') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label for="nombre" class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <hr>
            <div class="form-group">
                <div class="col-md-4"></div>
                <div class="col-md-6"><strong>Publicado para</strong></div>
            </div>
            <!-- Destinatarios -->
            <div id="destinatarios_todos">
                <div class="form-group {{ $errors->has('destinatarios_1') ? ' has-error' : '' }}" id="restriccion_destinatarios_1">
                    <label class="control-label col-md-4" for="destinatarios_1">Destinatarios</label>

                    <div class="col-md-8">
                        <select multiple="multiple" id="destinatarios_1" name="destinatarios_1[]">
                            <optgroup label="Sucursales">
                                @foreach ($sucursales as $sucursal)
                                    <option value="sucursal;{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Cargos">
                                @foreach ($cargos as $cargo)
                                    <option value="cargo;{{ $cargo->id }}">{{ $cargo->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Usuarios">
                                @foreach ($usuarios as $usuario)
                                    <option value="usuario;{{ $usuario->id }}">{{ $usuario->nombre }} {{ $usuario->apellido }}</option>
                                @endforeach
                            </optgroup>
                        </select>

                        @if ($errors->has('destinatarios_1'))
                            <span class="help-block">
                                <strong>{{ $errors->first('destinatarios_1') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('destinatarios_2') ? ' has-error' : '' }}" id="restriccion_destinatarios_2">
                    <label class="control-label col-md-4" for="destinatarios_2">Destinatarios</label>

                    <div class="col-md-8">
                        <select multiple="multiple" id="destinatarios_2" name="destinatarios_2[]">
                            <optgroup label="Sucursales">
                                @foreach ($sucursales as $sucursal)
                                    <option value="sucursal;{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Cargos">
                                @foreach ($cargos as $cargo)
                                    <option value="cargo;{{ $cargo->id }}">{{ $cargo->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Usuarios">
                                @foreach ($usuarios as $usuario)
                                    <option value="usuario;{{ $usuario->id }}">{{ $usuario->nombre }} {{ $usuario->apellido }}</option>
                                @endforeach
                            </optgroup>
                        </select>

                        @if ($errors->has('destinatarios_2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('destinatarios_2') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('destinatarios_3') ? ' has-error' : '' }}" id="restriccion_destinatarios_3">
                    <label class="control-label col-md-4" for="destinatarios_3">Destinatarios</label>

                    <div class="col-md-8">
                        <select multiple="multiple" id="destinatarios_3" name="destinatarios_3[]">
                            <optgroup label="Sucursales">
                                @foreach ($sucursales as $sucursal)
                                    <option value="sucursal;{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Cargos">
                                @foreach ($cargos as $cargo)
                                    <option value="cargo;{{ $cargo->id }}">{{ $cargo->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Usuarios">
                                @foreach ($usuarios as $usuario)
                                    <option value="usuario;{{ $usuario->id }}">{{ $usuario->nombre }} {{ $usuario->apellido }}</option>
                                @endforeach
                            </optgroup>
                        </select>

                        @if ($errors->has('destinatarios_3'))
                            <span class="help-block">
                                <strong>{{ $errors->first('destinatarios_3') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-4"></div>
                    <div class="col-md-6"><a href="#" id="agregar-destinatarios">Agregar destinatarios</a></div>
                </div>
            </div>

            <hr>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="12" class="form-control col-md-6">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Tags -->
            <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                <label for="tags" class="col-md-4 control-label">Tags</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="tags" value="{{ old('tags') }}">

                    @if ($errors->has('tags'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tags') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Estado -->
            <div class="form-group {{ $errors->has('estado_id') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="estado_id">Estado</label>

                <div class="col-md-6">
                    <select name="estado_id" id="estado_id" class="form-control">
                        <option value="0">Seleccione el estado...</option>
                        @foreach ($estados as $estado)
                            <option value="{{ $estado->id }}"
                            @if(old('estado_id') == $estado->id)
                                selected
                            @endif>
                                {{ $estado->nombre }}
                            </option>
                        @endforeach
                    </select>

                    @if ($errors->has('estado_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('estado_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Requiere Aprobacion -->
            <div class="form-group {{ $errors->has('requiere_aprobacion') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="requiere_aprobacion"></label>

                <div class="col-md-6">
                    <div class="checkbox">
                        <label><input type="checkbox" name="requiere_aprobacion" id="requiere_aprobacion" @if(old('requiere_aprobacion') == "on") checked @endif />Requiere aprobación</label>
                    </div>
                </div>
            </div>

            <!-- Usuario que debe aprobar -->
            <div class="form-group {{ $errors->has('user_aprobador_id') ? ' has-error' : '' }}" id="user_aprobador_id">
                <label class="control-label col-md-4" for="user_aprobador_id">Usuario aprobador</label>

                <div class="col-md-6">
                    <select name="user_aprobador_id" class="form-control">
                        <option value="0">Seleccione el usuario que debe aprobar...</option>

                        @foreach ($usuarios as $usuario)
                            <option value="{{ $usuario->id }}" @if(old('user_aprobador_id') == $usuario->id) selected @endif>{{ $usuario->nombre }} {{ $usuario->apellido }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('user_aprobador_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('user_aprobador_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <hr>
            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}" id="div-subir-archivos" data-url-subida="create/subir-archivos">
                <label for="archivo" class="control-label col-md-4">Archivos</label>

                <div class="col-md-6">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Agregar archivo...</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="files[]" multiple>
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-book"></i>Crear
                    </button>
                </div>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    @include('documentos.create-edit-javascripts')
@stop