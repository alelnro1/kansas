<script>
    $(document).ready(function() {
        $('#user_aprobador_id, #version_anterior, #primera_version_div, #restriccion_destinatarios_2, #restriccion_destinatarios_3').hide();

        requiereAprobacion();

        tipoDocumentoVersionado();

        $('#requiere_aprobacion').on('change', function() {
            requiereAprobacion();
        });

        $('#tipo_documento_id').on('change', function() {
            tipoDocumentoVersionado();
        });

        $('#primera_version_check').on('change', function() {
            esPrimeraVersion();
        });

        $('#agregar-destinatarios').on('click', function(e){
            e.preventDefault();

            if (!$('#restriccion_destinatarios_2').is(':visible')) {
                $('#restriccion_destinatarios_2').slideDown();
            } else if (!$('#restriccion_destinatarios_3').is(':visible')) {
                $('#restriccion_destinatarios_3').slideDown();

                // Si estan los 3 visibles => oculto la opcion de mostrar mas
                $(this).slideUp();
            }
        });

        $('#estado_id').on('change', function(){
            var estado_pendiente = ("Pendiente de Aprobación").toLowerCase().trim();

            if ($('#estado_id option:selected').text().toLowerCase().trim() == estado_pendiente) {
                $('#requiere_aprobacion').attr('checked', true);
                $('#requiere_aprobacion').attr('disabled', true);

                requiereAprobacion();
            } else {
                $('#requiere_aprobacion').attr('disabled', false);
            }
        });

        function requiereAprobacion(){
            if($('#requiere_aprobacion').is(":checked")) {
                $('#user_aprobador_id').slideDown();
            } else {
                $('#user_aprobador_id').slideUp();
            }
        }

        function tipoDocumentoVersionado(){
            var tipo_documento = $('#tipo_documento_id option:selected').text().toLowerCase().trim(),
                    versionado = $('#tipo_documento_id option:selected').data('versionado');

            if (versionado == true){ // Si no es super admin tiene que elegir una sucursal
                $('#primera_version_div').slideDown();
                esPrimeraVersion();
            } else {
                $('#primera_version_div, #version_anterior').slideUp();
            }
        }

        function esPrimeraVersion() {
            if ($('#primera_version_check').is(":checked")) {
                $('#version_anterior').slideUp();
            } else {
                $('#version_anterior').slideDown();
            }
        }
    });
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="/js/multiple-upload/vendor/jquery.ui.widget.js"></script>

<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/js/multiple-upload/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/js/multiple-upload/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/js/multiple-upload/jquery.fileupload-process.js"></script>
<!-- The File Upload validation plugin -->
<script src="/js/multiple-upload/jquery.fileupload-validate.js"></script>

<script src="/js/documentos/subir-archivos.js"></script>

<script type="text/javascript" src="/js/documentos/create.js"></script>
<script type="text/javascript" src="/js/bootstrap-multiselect.js"></script>

<script>
    $(document).ready(function() {
        $('select[name=version_anterior]').multiselect({
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Buscar...'
        });

        // La cantidad total de destinatarios, que sirve para checkear con el seleccionar todos
        var cantidad_de_destinatarios = 0;

        setTimeout(function () {
            cantidad_de_destinatarios = $('#restriccion_destinatarios_1 li').size() - 3; // Le saco los titulos: sucursales, usuarios, cargos
        }, 3000);


        $('#destinatarios_1, #destinatarios_2, #destinatarios_3').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Buscar...',
            allSelectedText: 'Todos seleccionados...',
            numberDisplayed: 1,

            onSelectAll: function () {
                todosSeleccionados($(this));
            },

            onDeselectAll: function(){
                todosSeleccionados($(this));
            },

            onChange: function (event) {
                if (event[0]['selected'] == false) {
                    $('#destinatarios_todos').val('0');
                }
            },

            nonSelectedText: 'Seleccione un destinatario...',
            selectAllText: 'Seleccionar todos',
            maxHeight: 220
        });

        function todosSeleccionados(selector) {
            var select_actual = selector[0].$select[0].id; // Obtengo "destinatarios_1", "destinatarios_2" o "destinatarios_3"

            console.log(
                    cantidad_de_destinatarios + " - " + $('#restriccion_' + select_actual + ' li.active').size()
            )
            // Si la cantidad de destinatarios total es igual a los seleccionados => están todos
            if (cantidad_de_destinatarios == $('#restriccion_' + select_actual + ' li.active').size()) {
                $('#destinatarios_todos').val('1');
            } else {
                $('#destinatarios_todos').val('0');
            }
        }

        function wasDeselected(sel, val) {
            if (!val) {
                return true;
            }
            return sel && sel.some(function (d) {
                        return val.indexOf(d) == -1;
                    })
        }
    });
</script>