<div class="form-group {{ $errors->has('destinatarios') ? ' has-error' : '' }}">
    <label class="control-label col-md-4" for="destinatarios">Destinatarios</label>

    <div class="col-md-8">
        <select multiple="multiple" id="destinatarios" name="destinatarios[]">
            <optgroup label="Sucursales">
                @foreach ($sucursales as $sucursal)
                    <option value="{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
                @endforeach
            </optgroup>
            <optgroup label="Cargos">
                @foreach ($cargos as $cargo)
                    <option value="{{ $cargo->id }}">{{ $cargo->nombre }}</option>
                @endforeach
            </optgroup>
            <optgroup label="Usuarios">
                @foreach ($usuarios as $usuario)
                    <option value="{{ $usuario->id }}">{{ $usuario->nombre }}</option>
                @endforeach
            </optgroup>
        </select>
    </div>
</div>