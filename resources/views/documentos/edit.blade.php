@extends('layouts.app')

@section('site-name', 'Editar documento')

@section('styles')
    <link rel="stylesheet" href="{{ asset("/css/bootstrap-multiselect.css") }}" type="text/css"/>
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="../../css/multiple-upload/jquery.fileupload.css">

@stop

@section('content')
    <div class="panel-heading">Documento</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="/documentos/{{ $documento->id }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}

            <input type="hidden" name="urls_archivos" value="{{ old('urls_archivos') }}">

            <!-- Tipo -->
            <div class="form-group {{ $errors->has('tipo_documento_id') ? ' has-error' : '' }}" id="tipo_documento_id">
                <label class="control-label col-md-4" for="tipo_documento_id">Tipo</label>

                <div class="col-md-6">
                    <select name="tipo_documento_id" class="form-control">
                        <option value="0">Seleccione tipo de documento...</option>

                        @foreach($tipos_documento as $tipo)
                            <option value="{{ $tipo->id }}" @if($documento->tipo_documento_id == $tipo->id) selected @endif>
                                {{ $tipo->nombre }}
                            </option>
                        @endforeach

                    </select>

                    @if ($errors->has('tipo_documento_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tipo_documento_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Es primera version -->
            <div class="form-group {{ $errors->has('primera_version') ? ' has-error' : '' }}" id="primera_version_div">
                <label class="control-label col-md-4" for="primera_version"></label>

                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" checked name="primera_version" id="primera_version_check">Es primera version
                        </label>
                    </div>
                </div>
            </div>

            <!-- Version anterior -->
            <div class="form-group {{ $errors->has('version_anterior') ? ' has-error' : '' }}" id="version_anterior">
                <label class="control-label col-md-4" for="version_anterior">Versión Anterior</label>

                <div class="col-md-6">
                    <select name="version_anterior" class="form-control">
                        <option value="0">Seleccione versión anterior...</option>

                        @foreach($documentos as $documento_sub)
                            <option value="{{ $documento_sub->id }}" @if($documento->version_anterior == $documento_sub->id) selected @endif>{{ $documento_sub->nombre }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('version_anterior'))
                        <span class="help-block">
                            <strong>{{ $errors->first('version_anterior') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label for="nombre" class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $documento->nombre }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="12" class="form-control col-md-6">{{ $documento->descripcion }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Tags -->
            <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                <label for="tags" class="col-md-4 control-label">Tags</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="tags" value="{{ $documento->tags }}">

                    @if ($errors->has('tags'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tags') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Estado -->
            <div class="form-group {{ $errors->has('estado_id') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="estado_id">Estado</label>

                <div class="col-md-6">
                    <select name="estado_id" id="estado_id" class="form-control">
                        <option value="0">Seleccione el estado...</option>
                        @foreach ($estados as $estado)
                            <option value="{{ $estado->id }}"
                                    @if($documento->estado_id == $estado->id)
                                    selected
                                    @endif>
                                {{ $estado->nombre }}
                            </option>
                        @endforeach
                    </select>

                    @if ($errors->has('estado_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('estado_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Requiere Aprobacion -->
            <div class="form-group {{ $errors->has('requiere_aprobacion') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="requiere_aprobacion"></label>

                <div class="col-md-6">
                    <div class="checkbox">
                        <label><input type="checkbox" name="requiere_aprobacion" id="requiere_aprobacion" @if($documento->user_aprobador_id == true) checked @endif />Requiere aprobación</label>
                    </div>
                </div>
            </div>

            <!-- Usuario que debe aprobar -->
            <div class="form-group {{ $errors->has('user_aprobador_id') ? ' has-error' : '' }}" id="user_aprobador_id">
                <label class="control-label col-md-4" for="user_aprobador_id">Usuario aprobador</label>

                <div class="col-md-6">
                    <select name="user_aprobador_id" class="form-control">
                        <option value="0">Seleccione el usuario que debe aprobar...</option>

                        @foreach ($usuarios as $usuario)
                            <option value="{{ $usuario->id }}" @if($documento->user_aprobador_id == $usuario->id) selected @endif>{{ $usuario->nombre }} {{ $usuario->apellido }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('user_aprobador_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('user_aprobador_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <hr>
            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}" id="div-subir-archivos" data-url-subida="../edit/subir-archivos">
                <label for="archivo" class="control-label col-md-4">Archivos</label>

                <div class="col-md-6">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Agregar archivo...</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="files[]" multiple>
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif

                    @if(count($documento->archivos) > 0)
                        <table class="table table-striped task-table">
                            <thead>
                                <tr>
                                    <th>Documentos Existentes</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($documento->archivos as $archivo_existente)
                                    <tr id="{{ $archivo_existente->id }}">
                                        <td><a href="../../{{ $archivo_existente->url }}" target="_blank">{{ $archivo_existente->nombre }}</a></td>
                                        <td><a href="#" data-archivo-id="{{ $archivo_existente->id }}" data-documento-id="{{ $archivo_existente->documento_id }}" class="eliminar-archivo-existente">Eliminar</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>


            <hr>
            <div class="form-group">
                <div class="col-md-4"></div>
                <div class="col-md-6"><strong>Publicado para</strong><br><a href="#" id="mostrar-destinatarios">Ver destinatarios</a></div>
            </div>
            <!-- Destinatarios -->
            <div id="destinatarios_todos">
                <div class="form-group {{ $errors->has('destinatarios_1') ? ' has-error' : '' }}" id="restriccion_destinatarios_1">
                    <label class="control-label col-md-4" for="destinatarios_1">Destinatarios</label>

                    <div class="col-md-8">
                        <select multiple="multiple" id="destinatarios_1" name="destinatarios_1[]">
                            <optgroup label="Sucursales">
                                @foreach ($sucursales as $sucursal)
                                    <option value="sucursal;{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Cargos">
                                @foreach ($cargos as $cargo)
                                    <option value="cargo;{{ $cargo->id }}">{{ $cargo->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Usuarios">
                                @foreach ($usuarios as $usuario)
                                    <option value="usuario;{{ $usuario->id }}">{{ $usuario->nombre }}</option>
                                @endforeach
                            </optgroup>
                        </select>

                        @if ($errors->has('destinatarios_1'))
                            <span class="help-block">
                                <strong>{{ $errors->first('destinatarios_1') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('destinatarios_2') ? ' has-error' : '' }}" id="restriccion_destinatarios_2">
                    <label class="control-label col-md-4" for="destinatarios_2">Destinatarios</label>

                    <div class="col-md-8">
                        <select multiple="multiple" id="destinatarios_2" name="destinatarios_2[]">
                            <optgroup label="Sucursales">
                                @foreach ($sucursales as $sucursal)
                                    <option value="sucursal;{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Cargos">
                                @foreach ($cargos as $cargo)
                                    <option value="cargo;{{ $cargo->id }}">{{ $cargo->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Usuarios">
                                @foreach ($usuarios as $usuario)
                                    <option value="usuario;{{ $usuario->id }}">{{ $usuario->nombre }}</option>
                                @endforeach
                            </optgroup>
                        </select>

                        @if ($errors->has('destinatarios_2'))
                            <span class="help-block">
                                <strong>{{ $errors->first('destinatarios_2') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('destinatarios_3') ? ' has-error' : '' }}" id="restriccion_destinatarios_3">
                    <label class="control-label col-md-4" for="destinatarios_3">Destinatarios</label>

                    <div class="col-md-8">
                        <select multiple="multiple" id="destinatarios_3" name="destinatarios_3[]">
                            <optgroup label="Sucursales">
                                @foreach ($sucursales as $sucursal)
                                    <option value="sucursal;{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Cargos">
                                @foreach ($cargos as $cargo)
                                    <option value="cargo;{{ $cargo->id }}">{{ $cargo->nombre }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Usuarios">
                                @foreach ($usuarios as $usuario)
                                    <option value="usuario;{{ $usuario->id }}">{{ $usuario->nombre }}</option>
                                @endforeach
                            </optgroup>
                        </select>

                        @if ($errors->has('destinatarios_3'))
                            <span class="help-block">
                                <strong>{{ $errors->first('destinatarios_3') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div id="dialog-destinatarios-existentes">
                    @if(count($destinatarios) < 0)
                        El documento no tiene destinatarios
                    @else
                        <table class="table table-striped task-table" id="destinatarios_existentes">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Sucursal</th>
                                    <th>Cargo</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($destinatarios as $destinatario)
                                    <tr>
                                        <td>
                                            <a href="{{ url('usuarios/' . $destinatario->usuario->id) }}" target="_blank">{{ $destinatario->usuario->nombre }} {{ $destinatario->usuario->apellido }}</a>
                                        </td>

                                        <td>
                                            <a href="{{ url('sucursales/' . $destinatario->sucursal->id) }}" target="_blank">{{ $destinatario->sucursal->nombre }}</a>
                                        </td>

                                        <td>
                                            <a href="{{ url('cargos/' . $destinatario->cargo->id) }}" target="_blank">{{ $destinatario->cargo->nombre }}</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>

                <div class="form-group">
                    <div class="col-md-4"></div>
                    <div class="col-md-6"><a href="#" id="agregar-destinatarios">Agregar destinatarios</a></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    @include('documentos.create-edit-javascripts')

    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script type="text/javascript">

        $('#dialog-destinatarios-existentes').dialog({
            autoOpen: false,
            title: "Destinatarios",
            show: "blind",
            hide: "explode",
            modal: true,
            width: 500,
            maxHeight: 300
        });

        $('#mostrar-destinatarios').on('click', function(){
            $('#dialog-destinatarios-existentes').dialog('open');
        });

        $('.eliminar-archivo-existente').on('click', function(e){
            e.preventDefault();

            if (confirm('Esta seguro que quiere eliminar el archivo?')) {
                var documento_id = $(this).data('documento-id'),
                    archivo_id = $(this).data('archivo-id');

                $.ajax({
                    url: '/documentos/' + documento_id + '/archivos/' + archivo_id + '/eliminar',
                    dataType: 'json',
                    success: function(data){
                        if (data.valid == true){
                            alert('El archivo se eliminó');

                            $('tr#' + archivo_id).remove();
                        }
                    }
                });
            }
        });
    </script>
@stop