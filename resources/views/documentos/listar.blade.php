@extends('layouts.app')

@section('site-name', 'Listando documentos')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">
@stop

@section('content')
    @if(Session::has('documento_actualizado'))
        <div class="alert alert-success">
            {{ Session::get('documento_actualizado') }}
        </div>
    @endif

    <div class="panel-heading">Documentos</div>

    <div class="panel-body">
        <!-- Documentos a Aprobar -->
        <div id="accordion">
            <h3>
                Control de Documentos
                @if ($cantidad_documentos_a_aprobar > 0)
                    ({{ $cantidad_documentos_a_aprobar }})
                    <!--<div style="width: 5%; float:right;">
                        <span class="fa-stack">
                            <i class="fa fa-bell-o fa-2x" style="color:red;"></i>
                            <span class="fa fa-stack-1x" style="font-size: 11px;">{{ $cantidad_documentos_a_aprobar }}</span>
                        </span>
                    </div>-->
                @endif
            </h3>
            <div>
                @include('documentos.documentos-a-aprobar')
            </div>

            <!-- Documentos para mi-->
            <h3>
                Documentos no leídos
                @if ($cantidad_documentos_para_mi > 0)
                    ({{$cantidad_documentos_para_mi}})
                    <!--<div style="width: 5%; float:right;">
                        <span class="fa-stack">
                            <i class="fa fa-bell-o fa-2x" style="color:red;"></i>
                            <span class="fa fa-stack-1x" style="font-size: 11px;">{{ $cantidad_documentos_para_mi }}</span>
                        </span>
                    </div>-->
                @endif
            </h3>
            <div>
                @include('documentos.documentos-para-mi')
            </div>

            <!-- Mis Documentos -->
            <h3>Documentos Propios</h3>
            <div>
                <legend>
                    <div style="float:right; margin: 10px 0;">
                        <a href="/documentos/create" class="btn btn-default">Nuevo</a>
                    </div>
                </legend>

                @include('documentos.mis-documentos')
            </div>

            @if ($cantidad_cargos_dependientes > 0)
                <!-- Documentos de Dependientes -->
                <h3>Documentos de Dependientes</h3>
                <div>
                    @include('documentos.documentos-de-dependientes')
                </div>
            @endif
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $( "#accordion" ).accordion({
            active: false,
            collapsible: true,
            heightStyle: "content"
        });
    </script>

    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.1.2/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/documentos/listar.js') }}"></script>




@stop