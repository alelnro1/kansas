@section('styles')

    @parent
@stop

@if (count($tareas) > 0)

    <table class="table table-striped task-table" id="tareas">
        <!-- Table Headings -->
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Fecha Inicio</th>
            <th>Fecha Cierre</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach ($tareas as $tarea)
            @if ($tarea->fecha_cierre != "")
                <tr>
                    <td>{{ $tarea->nombre }}</td>
                    <td style="width:30%">{{ $tarea->descripcion }}</td>
                    <td>{{ $tarea->created_at }}</td>
                    <td>
                        @if(!$tarea->fecha_cierre)
                            -
                        @else
                            {{ $tarea->fecha_cierre }}
                        @endif
                    </td>
                    <td>
                        @if($tarea['comentario_cierre'] != null)
                            <a href="#" class="ver-comentario-cierre" data-comentario-cierre="{{ $tarea['comentario_cierre'] }}">Ver comentario</a>
                        @endif
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@else
    No hay tareas pendientes. <a href="{{ url('sucursales/' . $sucursal->id .'/tareas/create') }}">Click aquí</a> para crear una
@endif

@section('javascript')
    <script type="text/javascript">
        $(function() {

            $('.ver-comentario-cierre').on('click', function(e){
                e.preventDefault();

                var comentario_cierre = $(this).data('comentario-cierre');

                alert( comentario_cierre );
            });
        });
    </script>
    @parent
@stop