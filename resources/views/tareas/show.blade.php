@extends('layouts.app')

@section('site-name', 'Viendo a tarea')

@section('content')
    <div class="panel-heading">
        Tarea Pendiente con nombre <b><i>{{ $tarea->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('tarea_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('tarea_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 60%;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $tarea->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripcion</strong></td>
                <td>{{ $tarea->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>{{ $tarea->estado }}</td>
            </tr>

            <tr>
                <td><strong>Fecha</strong></td>
                <td>{{ date('Y/m/d', strtotime($tarea->fecha)) }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $tarea->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $tarea->email }}</td>
            </tr>

            <tr>
                <td><strong>Telefono</strong></td>
                <td>{{ $tarea->telefono }}</td>
            </tr>

            <tr>
                <td><strong>Archivo</strong></td>
                <td><img src="/{{ $tarea->archivo }}" height="250" /></td>
            </tr>

            <tr>
                <td><strong>Fecha Creacion</strong></td>
                <td>{{ $tarea->created_at }}</td>
            </tr>
        </table>

        <div>
             <a href="/tareas/{{ $tarea->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
