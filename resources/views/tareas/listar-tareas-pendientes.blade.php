@section('styles')

    @parent
@stop

@if (count($tareas) > 0)
    @if($manager_log->cerrado == 0)
        <div style="width:150px; float:right;">
            <a href="{{ url('sucursales/' . $sucursal->id .'/tareas/create') }}" class="btn btn-xs btn-default">
                Nueva Tarea
            </a>
        </div>
    @endif

    <table class="table table-striped task-table" id="tareas">
        <!-- Table Headings -->
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Fecha Inicio</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach ($tareas as $tarea)
            @if ($tarea->fecha_cierre == "")
                <tr>
                    <td>{{ $tarea->nombre }}</td>
                    <td style="width:30%">{{ $tarea->descripcion }}</td>
                    <td>{{ $tarea->created_at }}</td>
                    <td>
                        @if(!$tarea->fecha_cierre && $manager_log->cerrado == 0)
                            <a href="/tareas/{{ $tarea['id'] }}/edit">Editar</a>
                            |
                            <a href="/tareas/{{ $tarea['id'] }}/cerrar" class="cerrar-tarea" data-tarea-id="{{ $tarea['id'] }}">
                                Cerrar
                            </a>
                        @endif
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>


    <div id="dialog-cerrar" title="Está seguro que desea dar por finalizada la tarea?">
        <form action="" id="form-cerrar">
            {{ csrf_field() }}
            <p>Si lo desea, ingrese un comentario sobre la tarea</p>

            <textarea id="comentario-cerrar" name="comentario" placeholder="Escríba aquí" class="form-control" rows="5"></textarea>
        </form>
    </div>
@else
    No hay tareas pendientes. <a href="{{ url('sucursales/' . $sucursal->id .'/tareas/create') }}">Click aquí</a> para crear una
@endif

@section('javascript')
    <script type="text/javascript">
        $(function() {
            $('#dialog-cerrar').hide();

            $('.cerrar-tarea').on('click', function(e){
                e.preventDefault();

                var tarea_id   = $(this).data('tarea-id'),
                        comentario = $('#comentario-cerrar').val();

                $( "#dialog-cerrar" ).dialog({
                    width:530,
                    modal: true,
                    buttons: {
                        "Cerrar": function() {
                            $.ajax({
                                url: '/tareas/' + tarea_id + '/cerrar',
                                type: 'POST',
                                data: $('#form-cerrar').serialize(),
                                success: function(data){
                                    if (data.valid == true){
                                        alert('Tarea cerrada');
                                        window.location.reload();
                                    }
                                }
                            });
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
            });
        });
    </script>
    @parent
@stop