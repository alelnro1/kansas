<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @if (Auth::guest())
            Documentos Manager
        @else
            {{ Auth::user()->nombreEmpresaUsuario() }} Manager - @yield('site-name')
        @endif
    </title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    @yield('styles')

    <style>
        body {
            font-family: 'Lato';
            font-size: 13px;
        }

        .fa-btn {
            margin-right: 6px;
        }

        .navbar-right > li, .navbar-header > a {
            margin-top:4px;
        }

        .panel-heading {
            background-color: #a80000 !important;
            color: white !important;
        }
    </style>
</head>
<body id="app-layout">
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                @if (Auth::guest())
                    Documentos Manager
                @else
                    <img
                        style="max-height:25px; max-width:200px; float:left; padding-right:5px" alt=""
                        src="/{{ Auth::user()->logoEmpresaUsuario() }}" />MANAGER
                @endif
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (!Auth::guest())
                    @if ((Auth::user()->puedeVerManagerLog() && !Auth::user()->esSuperAdmin()) || Auth::user()->esUserSoloTareas())
                        <li><a href="{{ url('/man-log') }}">MANAGER LOG</a></li>
                    @endif

                    @if (!Auth::user()->esUserSoloTareas())
                        @if (Auth::user()->esSuperAdmin())
                            <li><a href="{{ url('/empresas') }}">EMPRESAS</a></li>
                        @endif

                        @if (Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin())
                            <li><a href="{{ url('/usuarios') }}">USUARIOS</a></li>
                            <li><a href="{{ url('/tipos-documentos') }}">TIPOS DE DOCUMENTOS</a></li>
                            <li><a href="{{ url('denominaciones') }}">DENOMINACIONES</a></li>
                        @endif

                        @if (!Auth::user()->esSuperAdmin())
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    @if (Auth::user()->tieneDocumentosNuevos())
                                        <i class="fa fa-bell" aria-hidden="true"></i>
                                    @endif
                                    DOCUMENTOS<span class="caret"></span></a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/documentos') }}">Gestión de Documentos</a></li>
                                    <li><a href="{{ url('/biblioteca-de-documentos') }}">Biblioteca de Documentos</a></li>
                                </ul>

                            </li>
                        @endif


                        @if (Auth::user()->esAdministrador() && !Auth::user()->esSuperAdmin())
                            <li><a href="{{ url('/sucursales') }}">SUCURSALES</a></li>
                            <li><a href="{{ url('/cargos') }}">CARGOS</a></li>
                        @endif
                    @endif

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="margin-top: -8px">
                            @if(Auth::user()->archivo)
                                <img src="/{{ Auth::user()->archivo }}" class="img-circle" style="height: 45px;">
                            @else
                                <img src="/avatar.png" class="img-circle" style="height: 35px;">
                            @endif
                            {{ strtoupper(Auth::user()->nombre) }}<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/perfil') }}">Ver mis datos</a></li>
                            <li><a href="{{ url('/cambiar-clave-personal') }}">Cambiar contraseña</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Salir</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<nav role="navigation" class="navbar navbar-default  navbar-fixed-bottom">
    <div class="container text-center">
        <p class="navbar-text col-md-12 col-sm-12 col-xs-12">&copy; 2016. Powered by <a target="_blank" href="http://www.netkey.com.ar">NetKey Argentina SA</a></p>
    </div>
</nav>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}


@yield('javascript')
</body>
</html>
