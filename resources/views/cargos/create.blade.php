@extends('layouts.app')

@section('site-name', 'Nuevo cargo')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo</div>

    <div class="panel-body">
        <form action="/cargos" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Ve Manager Log -->
            <div class="form-group {{ $errors->has('manager_log') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="manager_log"></label>

                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="manager_log" id="manager_log" @if(old('manager_log') == "on") checked @endif>Ve Manager Log
                        </label>
                    </div>
                </div>
            </div>

            <!-- Tiene cargo padre -->
            <div class="form-group {{ $errors->has('tiene_padre') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="tiene_padre"></label>

                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="tiene_padre" id="tiene_padre" @if(old('tiene_padre') == "on") checked @endif>Tiene padre
                        </label>
                    </div>
                </div>
            </div>

            <!-- Cargo Padre -->
            <div class="form-group{{ $errors->has('padre_id') ? ' has-error' : '' }}" id="padre_id">
                <label class="col-md-4 control-label">Cargo padre</label>

                <div class="col-md-6">
                    <select name="padre_id" id="padre_id" class="form-control">
                        <option value="0">Seleccione un cargo padre...</option>

                        @foreach ($cargos as $cargo)
                            <option value="{{ $cargo->id }}">{{ $cargo->nombre }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('padre_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('padre_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Crear
                    </button>
                </div>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/cargos/manejar_cargo_padre.js') }}"></script>
@stop