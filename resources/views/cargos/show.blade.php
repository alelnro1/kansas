@extends('layouts.app')

@section('site-name', 'Cargo ' . $cargo->nombre)

@section('content')
    <div class="panel-heading">
        Cargo con nombre <b><i>{{ $cargo->nombre }}</i></b>
    </div>

    <div class="panel-body">
        <table class="table table-striped task-table" style="width: 60%;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $cargo->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Fecha Creacion</strong></td>
                <td>{{ $cargo->created_at }}</td>
            </tr>
        </table>

        @if(Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin())
            <div>
                 <a href="/cargos/{{ $cargo->id }}/edit">Editar</a>
            </div>
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
