@extends('layouts.app')

@section('site-name', 'Cargos')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Cargos

        <div style="float:right;">
            <a href="/cargos/create">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('cargo_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('cargo_eliminado') }}
            </div>
        @endif

        @if(Session::has('cargo_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('cargo_actualizado') }}
            </div>
        @endif

        @if(Session::has('cargo_creado'))
            <div class="alert alert-success">
                {{ Session::get('cargo_creado') }}
            </div>
        @endif

        @if(Session::has('cargo_tiene_usuarios'))
            <div class="alert alert-danger">
                {{ Session::get('cargo_tiene_usuarios') }}
            </div>
        @endif


        @if (count($cargos) > 0)
            <div id="listado_cargos" class="hide" data-cargos="{{ $cargos }}"></div>

            <table class="table table-striped task-table" id="cargos">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Padre</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($cargos as $cargo)
                    <tr>
                        <td>{{ $cargo->nombre }}</td>
                        <td>
                            @if($cargo->CargoPadre != null)
                                {{ $cargo->CargoPadre->nombre }}
                            @endif
                        </td>
                        <td>
                            @if(Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin())
                                <a href="/cargos/{{ $cargo['id'] }}/edit">Editar</a>
                                |
                                <a href="/cargos/{{ $cargo['id'] }}"
                                   data-method="delete"
                                   data-token="{{ csrf_token() }}"
                                   data-confirm="Esta seguro que desea eliminar el cargo con nombre {{ $cargo->nombre }}?">
                                    Eliminar
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <fieldset>
                <legend>Organigrama</legend>

                <div id="chart_div"></div>
            </fieldset>
        @else
            No hay cargos
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.1.2/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/cargos/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/cargos/delete-link.js') }}"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script>
        var cargos = $('#listado_cargos').data('cargos');

        google.charts.load('current', {packages:["orgchart"]});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Nombre');
            data.addColumn('string', 'Manager');
            data.addColumn('string', 'Tooltip');

            $.each(cargos, function(key, cargo){
                console.log(cargo['nombre']);
                var link_eliminar =
                        '<div>' +
                        '<a href="/cargos/' + cargo["id"] + '"' +
                        'data-method="delete" ' +
                        'data-token="{{ csrf_token() }}"' +
                        'data-confirm="Esta seguro que desea eliminar el cargo con nombre ' + cargo['nombre'] + '?">Eliminar</a></div>';

                var padre_id;

                if (cargo['cargo_padre'] != null) {
                    padre_id = String(cargo['padre_id']);
                } else {
                    padre_id = '';
                }

                data.addRows([
                    [
                        {v:String(cargo['id']), f:cargo['nombre']  + link_eliminar},
                        padre_id, cargo['nombre']
                    ]
                ]);
            });

            // Create the chart.
            var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
            // Draw the chart, setting the allowHtml option to true for the tooltips.
            chart.draw(data, {allowHtml:true});

            manejarLinkEliminado();
        }

        function manejarLinkEliminado() {
            !function(){var t={initialize:function(){this.methodLinks=$("a[data-method]"),this.token=$("a[data-token]"),this.registerEvents()},registerEvents:function(){this.methodLinks.on("click",this.handleMethod)},handleMethod:function(e){var n,i=$(this),a=i.data("method").toUpperCase();if(-1!==$.inArray(a,["PUT","DELETE"])){if(i.data("confirm")&&!t.verifyConfirm(i))return!1;n=t.createForm(i),n.submit(),e.preventDefault()}},verifyConfirm:function(t){return confirm(t.data("confirm"))},createForm:function(t){var e=$("<form>",{method:"POST",action:t.attr("href")}),n=$("<input>",{type:"hidden",name:"_token",value:t.data("token")}),i=$("<input>",{name:"_method",type:"hidden",value:t.data("method")});return e.append(n,i).appendTo("body")}};t.initialize()}();
        }
    </script>
@stop