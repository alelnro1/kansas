@extends('layouts.app')

@section('site-name', 'Editar Cargo ' . $cargo->nombre)

@section('content')
    <div class="panel-heading">Cargo</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="/cargos/{{ $cargo->id }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $cargo->nombre }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Ve Manager Log -->
            <div class="form-group {{ $errors->has('manager_log') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="manager_log"></label>

                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="manager_log" id="manager_log" @if($cargo->manager_log) checked @endif>Ve Manager Log
                        </label>
                    </div>
                </div>
            </div>

            <!-- Tiene cargo padre -->
            <div class="form-group {{ $errors->has('tiene_padre') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="tiene_padre"></label>

                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="tiene_padre" id="tiene_padre" @if($cargo->padre_id) checked @endif>Tiene padre
                        </label>
                    </div>
                </div>
            </div>

            <!-- Cargo Padre -->
            <div class="form-group{{ $errors->has('padre_id') ? ' has-error' : '' }}" id="padre_id">
                <label class="col-md-4 control-label">Cargo padre</label>

                <div class="col-md-6">
                    <select name="padre_id" id="padre_id" class="form-control">
                        <option value="0">Seleccione un cargo padre...</option>

                        @foreach ($cargos as $cargo_menos_actual)

                            <option value="{{ $cargo_menos_actual->id }}"
                                @if ($cargo_menos_actual->id == $cargo->padre_id) selected @endif>
                                {{ $cargo_menos_actual->nombre }}
                            </option>
                        @endforeach
                    </select>

                    @if ($errors->has('padre_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('padre_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/cargos/manejar_cargo_padre.js') }}"></script>
@stop