@extends('layouts.app')

@section('site-name', 'Viendo actividad')

@section('content')
    <div class="panel-heading">
        Actividad con nombre <b><i>{{ $actividad->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('actividad_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('actividad_actualizado') }}
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 60%;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $actividad->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripcion</strong></td>
                <td>{{ $actividad->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>{{ $actividad->estado }}</td>
            </tr>

            <tr>
                <td><strong>Fecha</strong></td>
                <td>{{ date('Y/m/d', strtotime($actividad->fecha)) }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $actividad->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $actividad->email }}</td>
            </tr>

            <tr>
                <td><strong>Telefono</strong></td>
                <td>{{ $actividad->telefono }}</td>
            </tr>

            <tr>
                <td><strong>Archivo</strong></td>
                <td><img src="/{{ $actividad->archivo }}" height="250" /></td>
            </tr>

            <tr>
                <td><strong>Fecha Creacion</strong></td>
                <td>{{ $actividad->created_at }}</td>
            </tr>
        </table>

        <div>
             <a href="/actividades/{{ $actividad->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
