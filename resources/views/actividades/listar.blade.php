@if(Session::has('actividad_eliminado'))
    <div class="alert alert-success">
        {{ Session::get('actividad_eliminado') }}
    </div>
@endif

@if(Session::has('actividad_creado'))
    <div class="alert alert-success">
        {{ Session::get('actividad_creado') }}
    </div>
@endif

@if(Session::has('actividad_actualizada'))
    <div class="alert alert-success">
        {{ Session::get('actividad_actualizada') }}
    </div>
@endif

@if($manager_log->cerrado == 0)
    <div style="width:150px; float:right; margin-bottom:10px;">
        <a href="{{ url('man-log/' . $manager_log->id .'/sucursales/' . $sucursal->id .'/actividades/create') }}" class="btn btn-xs btn-default">
            Nueva Actividad
        </a>
    </div>
@endif

@if (count($actividades) > 0)
    <table class="table table-striped task-table" id="actividades" style="width:100%;">
        <!-- Table Headings -->
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($actividades as $actividad)
            <tr>
                <td>{{ $actividad->nombre }}</td>
                <td style="width:60%">{{ $actividad->descripcion }}</td>

                <td>
                    @if($manager_log->cerrado == 0)
                        <a href="{{ url('man-log/' . $manager_log->id .'/sucursales/' . $sucursal->id .'/actividades/' . $actividad['id'] . '/edit') }}">Editar</a>
                        |
                        <a href="{{ url('man-log/' . $manager_log->id .'/sucursales/' . $sucursal->id .'/actividades/' . $actividad['id']) }}"
                           data-method="delete"
                           data-token="{{ csrf_token() }}"
                           data-confirm="Esta seguro que desea eliminar la actividad con nombre {{ $actividad->nombre }}?">
                            Eliminar
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    No hay actividades
@endif