@extends('layouts.app')

@section('site-name', 'Viendo a empresa')

@section('content')
    <div class="panel-heading">
        Empresa con nombre <b><i>{{ $empresa->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(Session::has('empresa_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('empresa_actualizado') }}
            </div>
        @endif

        @if($empresa->archivo != "")
            <div class="form-group text-center">
                <img src="../{{ $empresa->archivo }}" width="250">
            </div>
        @endif

        <table class="table table-striped task-table" style="width: 60%;">
            <tr>
                <td><strong>Razón Social</strong></td>
                <td>{{ $empresa->razon_social }}</td>
            </tr>

            <tr>
                <td><strong>CUIT</strong></td>
                <td>{{ $empresa->cuit }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $empresa->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Fecha Creacion</strong></td>
                <td>{{ $empresa->created_at }}</td>
            </tr>
        </table>

        <div>
             <a href="/empresas/{{ $empresa->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
