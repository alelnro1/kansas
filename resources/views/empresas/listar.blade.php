@extends('layouts.app')

@section('site-name', 'Listando empresas')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Empresas

        <div style="float:right;">
            <a href="/empresas/create">Nueva</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('empresa_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('empresa_eliminado') }}
            </div>
        @endif

        @if(Session::has('empresa_creado'))
            <div class="alert alert-success">
                {{ Session::get('empresa_creado') }}
            </div>
        @endif

        @if (count($empresas) > 0)
            <table class="table table-striped task-table" id="empresas">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Razón Social</th>
                        <th>CUIT</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($empresas as $empresa)
                    <tr>
                        <td>{{ $empresa->razon_social }}</td>
                        <td style="width:75%">{{ $empresa->cuit }}</td>

                        <td>
                            <a href="/empresas/{{ $empresa['id'] }}">Ver</a>
                            |
                            <a href="/empresas/{{ $empresa['id'] }}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar a la empresa {{ $empresa->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay empresas
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/empresas/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/empresas/delete-link.js') }}"></script>
@stop