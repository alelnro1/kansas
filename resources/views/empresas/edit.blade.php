@extends('layouts.app')

@section('site-name', 'Editar empresa')

@section('content')
    <div class="panel-heading">Empresa</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="/empresas/{{ $empresa->id }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}

            <!-- Razon Social -->
            <div class="form-group{{ $errors->has('razon_social') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Razón Social</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="razon_social" value="{{ $empresa->razon_social }}">

                    @if ($errors->has('razon_social'))
                        <span class="help-block">
                            <strong>{{ $errors->first('razon_social') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- CUIT -->
            <div class="form-group {{ $errors->has('cuit') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">CUIT</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cuit" value="{{ $empresa->cuit }}">

                    @if ($errors->has('cuit'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuit') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Logo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Logo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="archivo">

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Domicilio -->
            <div class="form-group{{ $errors->has('domicilio') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio" value="{{ $empresa->domicilio }}" id="domicilio" autocomplete="off">

                    @if ($errors->has('domicilio'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-building-o"></i>Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAjTpj9h5ANX5iTQIKxkAhI-zcoPxl8GtY"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/empresas/edit.js') }}"></script>
@stop