@extends('layouts.app')

@section('site-name', 'Crear usuario')

@section('content')
    <div class="panel-heading">Crear usuario</div>
    <div class="panel-body">
        <form class="form-horizontal" role="form" method="POST" action="/usuarios/store" id="form-nuevo-usuario" enctype="multipart/form-data">
            {{ csrf_field() }}

            <input type="hidden" name="sucursales_cargos" id="sucursales_cargos" value="">

            <fieldset>
                <legend>Datos</legend>


                <!-- Nombre -->
                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Nombre</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">

                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Apellido -->
                <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Apellido</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="apellido" value="{{ old('apellido') }}">

                        @if ($errors->has('apellido'))
                            <span class="help-block">
                                <strong>{{ $errors->first('apellido') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Email -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autocomplete="off">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Telefono -->
                <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Telefono</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="telefono" value="{{ old('telefono') }}">

                        @if ($errors->has('telefono'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Foto -->
                <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                    <label for="archivo" class="control-label col-md-4">Foto</label>

                    <div class="col-md-6">
                        <input type="file" class="form-control" name="archivo">

                        @if ($errors->has('archivo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('archivo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Cargos</legend>

                <!-- Es nuevo usuario es admin -->
                @if (Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin())
                    <div class="form-group {{ $errors->has('estado') ? ' has-error' : '' }}" id="div_es_admin">
                        <label class="control-label col-md-4"></label>

                        <div class="col-md-6">
                            <input type="checkbox" name="es_admin" id="es_admin"
                                @if (Auth::user()->esSuperAdmin()) checked disabled @endif
                                > Es Administrador
                            <br><br>
                            @if (!Auth::user()->esSuperAdmin())
                                <input type="checkbox" name="solo_tareas"
                                       @if(old('solo_tareas') == "on") checked @endif> Sólo administra tareas
                            @endif
                        </div>
                        <div style="clear:both;"></div><br>
                    </div>
                @endif

                @if(!Auth::user()->esSuperAdmin())
                    @include('usuarios.sucursal-cargo-select')

                    <div>
                        <span class="col-md-4"></span>
                        <div class="col-md-6">
                            <div>
                                <table class="table table-striped task-table" id="lista-sucursales-cargos">
                                    <thead>
                                    <tr>
                                        <th>Sucursal</th>
                                        <th>Cargo</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody id="sucursales-cargos">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </fieldset>

            @if(Auth::user()->esSuperAdmin())
                <fieldset>
                    <legend>Vincular con Empresa</legend>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Empresa</label>

                        <div class="col-md-6">
                            <select name="empresa_id" id="empresa_id" class="form-control">
                                <option value="0">Seleccione una empresa...</option>

                                @foreach ($empresas as $empresa)
                                    <option value="{{ $empresa->id }}" @if(old('empresa_id') == $empresa->id) selected @endif>{{ $empresa->razon_social }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('empresa_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('empresa_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </fieldset>
            @endif

            <fieldset>
                <legend>Setear Contraseña</legend>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Contraseña</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" autocomplete="off">

                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-4 control-label">Repetir constraseña</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i> Crear
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/usuarios/crear-editar.js') }}"></script>
    <script>
        $('#lista-sucursales-cargos').hide();
    </script>
@stop