@extends('layouts.app')

@section('site-name', 'Usuarios')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Usuarios
        <div style="width:100px; float:right;">
            <a href="/usuarios/create" class="btn btn-xs btn-default">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('usuario_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('usuario_eliminado') }}
            </div>
        @endif

        @if(Session::has('usuario_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('usuario_actualizado') }}
            </div>
        @endif

        @if (count($usuarios) > 0)
            <table class="table table-striped task-table display no-wrap" id="usuarios" style="width:100%">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Email</th>
                        @if (Auth::user()->esSuperAdmin())
                            <th>Empresa</th>
                        @else
                            <th>Sucursal | Cargo</th>
                        @endif
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($usuarios as $usuario)
                    <tr>
                        <td>{{ $usuario->nombre }}</td>
                        <td>{{ $usuario->apellido }}</td>
                        <td>{{ $usuario->email }}</td>

                        @if (Auth::user()->esSuperAdmin())
                            @if (isset($usuario->empresa->razon_social) && $usuario->empresa->razon_social != "")
                                <td>{{ $usuario->empresa->razon_social }}</td>
                            @else
                                <td>-</td>
                            @endif
                        @else
                            <td>
                                <table class="table table-striped task-table">
                                    @foreach ($usuario->sucursales as $sucursal)
                                        <tr>
                                            <td>{{ $sucursal->nombre }}</td>
                                            <td>
                                                @foreach($usuario->cargos as $cargo)
                                                    @if ($cargo->id == $cargo->pivot->cargo_id && $sucursal->id == $cargo->pivot->sucursal_id)
                                                        {{ $cargo->nombre }}
                                                    @endif
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>
                        @endif

                        <td>
                            <a href="/usuarios/{{ $usuario['id'] }}">Ver</a>
                            |
                            @if(Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin())
                                <a href="/usuarios/{{ $usuario['id'] }}/edit">Editar</a>
                                |
                                <a href="/usuarios/{{ $usuario->id }}/delete" onclick="return confirm('Esta seguro que desea eliminar al usuario {{ $usuario->nombre }} ?');">Eliminar</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            NO HAY USUARIOS
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/usuarios/listar.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.1.2/js/dataTables.rowReorder.min.js"></script>
@stop
