@extends('layouts.app')

@section('site-name', 'Editar usuario ' . $usuario->nombre . ' ' . $usuario->apellido )

@section('content')
    <div class="panel-heading">Editar usuario</div>
    <div class="panel-body">
        <form class="form-horizontal" action="/usuarios/{{ $usuario->id }}" method="POST" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}

            <input type="hidden" name="sucursales_cargos" id="sucursales_cargos" data-sucursales="{{ $sucursales_cargos }}" value="">

            @if($usuario->archivo != "")
                <div class="form-group text-center">
                    <img src="../../{{ $usuario->archivo }}" width="250">
                </div>
            @endif

            <fieldset>
                <legend>Datos</legend>

                <!-- Nombre -->
                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Nombre</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="nombre" value="{{ $usuario->nombre }}">

                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Apellido -->
                <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Apellido</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="apellido" value="{{ $usuario->apellido }}">

                        @if ($errors->has('apellido'))
                            <span class="help-block">
                                <strong>{{ $errors->first('apellido') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Email -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $usuario->email }}" autocomplete="off">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Telefono -->
                <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Telefono</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="telefono" value="{{ $usuario->telefono }}">

                        @if ($errors->has('telefono'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <!-- Foto -->
                <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                    <label for="archivo" class="control-label col-md-4">Cambiar foto</label>

                    <div class="col-md-6">
                        <input type="file" class="form-control" name="archivo">

                        @if ($errors->has('archivo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('archivo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Editar Cargos</legend>

                <!-- Es nuevo usuario es admin -->
                @if (Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin())
                    <div class="form-group {{ $errors->has('estado') ? ' has-error' : '' }}" id="div_es_admin">
                        <label class="control-label col-md-4"></label>

                        <div class="col-md-6">
                            <input type="checkbox" name="es_admin" id="es_admin" @if($usuario->esAdministrador()) checked @endif
                            > Es Administrador
                            <br><br>
                            <input type="checkbox" name="solo_tareas" @if($usuario->solo_tareas) checked @endif> Sólo administra tareas
                        </div>
                        <div style="clear:both;"></div><br>
                    </div>
                @endif

                @if(!Auth::user()->esSuperAdmin())
                    @include('usuarios.sucursal-cargo-select')

                    <div>
                        <span class="col-md-4"></span>
                        <div class="col-md-6">
                            <div>
                                <table class="table table-striped task-table" id="lista-sucursales-cargos">
                                    <thead>
                                        <tr>
                                            <th>Sucursal</th>
                                            <th>Cargo</th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody id="sucursales-cargos">
                                    @if(count($sucursales_cargos) > 0)
                                        @foreach($sucursales_cargos as $sucursales_cargo)
                                            @for($i = 1; $i <= count($sucursales_cargo->sucursales); $i++)
                                                <tr id='{{ $sucursales_cargo->sucursales->first()->id }}'>
                                                    <td>{{ $sucursales_cargo->sucursales->first()->nombre }}</td>
                                                    <td>{{ $sucursales_cargo->cargos->first()->nombre }}</td>
                                                    <td><a href='#' data-sucursal='{{ $sucursales_cargo->sucursales->first()->id }}' data-cargo='{{ $sucursales_cargo->cargos->first()->id }}' class='eliminar-sucursal-cargo'><i class='fa fa-times' aria-hidden='true'></i></a>
                                                </tr>
                                            @endfor

                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </fieldset>

            @if(Auth::user()->esSuperAdmin())
                <fieldset>
                    <legend>Vincular con Empresa</legend>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Empresa</label>

                        <div class="col-md-6">
                            <select name="empresa_id" id="empresa_id" class="form-control">
                                <option value="0">Seleccione una empresa...</option>

                                @foreach ($empresas as $empresa)
                                    <option value="{{ $empresa->id }}" @if($usuario->empresa_id == $empresa->id) selected @endif>{{ $empresa->razon_social }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('empresa_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('empresa_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </fieldset>
            @endif

            <fieldset>
                <legend>Editar Contraseña</legend>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Contraseña</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" autocomplete="off">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-4 control-label">Repetir constraseña</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </fieldset>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-pencil" aria-hidden="true"></i> Editar
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/usuarios/crear-editar.js') }}"></script>
@stop