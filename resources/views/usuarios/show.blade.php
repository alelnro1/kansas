@extends('layouts.app')

@section('site-name', 'Usuario ' . $usuario->nombre . ' ' . $usuario->apellido)

@section('content')
    <div class="panel-heading">
        Ver Usuario
    </div>

    <div class="panel-body">
        <fieldset>
            <legend>Datos</legend>
                @if($usuario->archivo != "")
                    <div class="form-group text-center">
                        <img src="../{{ $usuario->archivo }}" width="250">
                    </div>
                @endif

            <table class="table table-striped task-table" id="sucursales">
                <tr>
                    <td><strong>Nombre</strong></td>
                    <td>{{ $usuario->nombre }}</td>
                </tr>

                <tr>
                    <td><strong>Apellido</strong></td>
                    <td>{{ $usuario->apellido }}</td>
                </tr>

                <tr>
                    <td><strong>Email</strong></td>
                    <td>{{ $usuario->email }}</td>
                </tr>

                <tr>
                    <td><strong>Teléfono</strong></td>
                    <td>{{ $usuario->telefono }}</td>
                </tr>

                <tr>
                    <td><strong>Sólo tareas</strong></td>
                    <td>
                        @if ($usuario->solo_tareas)
                            Sí
                        @else
                            No
                        @endif
                    </td>
                </tr>


            @if($usuario->esAdministrador())
                    <tr>
                        <td><strong>Es admin</strong></td>
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    </tr>
                @endif

                @if(Auth::user()->esSuperadmin())
                    <tr>
                        <td><strong>Empresa</strong></td>
                        <td>
                            @if (empty($usuario->empresa))
                                No asignada
                            @else
                                {{ $usuario->empresa->razon_social }}
                            @endif
                        </td>
                    </tr>
                @endif
            </table>
        </fieldset>

        @if (!$usuario->esAdministrador())
            <fieldset>
            <legend>Sucursales</legend>
            <div>
                @if(count($usuario->sucursales) <= 0)
                    El usuario no pertenece a ninguna sucursal
                @else
                    <table class="table table-striped task-table" id="sucursales">
                        <!-- Table Headings -->
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Cargo en Sucursal</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($usuario->sucursales as $sucursal)
                                <tr>
                                    <td>{{ $sucursal->nombre }}</td>
                                    <td>
                                        @foreach($usuario->cargos as $cargo)
                                            @if ($cargo->id == $cargo->pivot->cargo_id && $sucursal->id == $cargo->pivot->sucursal_id)
                                                {{ $cargo->nombre }}
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </fieldset>
        @endif

        <fieldset>
            <legend>Documentos</legend>

            <div>
                @if(count($usuario->documentos) <= 0)
                    El usuario no tiene documentos creados
                @else
                    @foreach($usuario->documentos as $documento)
                        <div>
                            <a href="/documentos/{{ $documento->id }}">{{ $documento->nombre }}</a>
                        </div>
                    @endforeach
                @endif
            </div>
        </fieldset>

        <div>
            @if(Auth::user()->esAdministrador() || Auth::user()->esSuperAdmin())
                <br>
                <a href="/usuarios/{{ $usuario->id }}/edit">Editar</a>
            @endif
            <br>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

