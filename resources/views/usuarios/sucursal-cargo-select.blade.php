<div class="form-group{{ $errors->has('sucursal_id') ? ' has-error' : '' }}" id="sucursal">
    <label class="col-md-4 control-label">Sucursal</label>

    <div class="col-md-6">
        <select name="sucursal_id" id="sucursal_id" class="form-control">
            <option value="0">Seleccione una sucursal...</option>

            @foreach ($sucursales as $sucursal)
                <option value="{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
            @endforeach
        </select>

        @if ($errors->has('sucursal_id'))
            <span class="help-block">
                <strong>{{ $errors->first('sucursal_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('cargo_id') ? ' has-error' : '' }}" id="cargo">
    <label class="col-md-4 control-label">Cargo</label>

    <div class="col-md-6">
        <select name="cargo_id" id="cargo_id" class="form-control">
            <option value="0">Seleccione un cargo...</option>

            @foreach ($cargos as $cargo)
                <option value="{{ $cargo->id }}">{{ $cargo->nombre }}</option>
            @endforeach
        </select>

        @if ($errors->has('cargo_id'))
            <span class="help-block">
                <strong>{{ $errors->first('cargo_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div id="agregar-sucursal-cargo">
    <span class="col-md-4"></span>
    <div class="col-md-6">
        <div class="right">
            <button id="agregar-sucursal" style="float: right;">Agregar</button>
        </div>
    </div>
</div>