$(document).ready(function() {
    oTable = $('#denominaciones').DataTable({
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ denominaciones filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ denominaciones",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ denominaciones)",
        }
    });
});