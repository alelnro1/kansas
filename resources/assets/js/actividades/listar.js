$(document).ready(function() {
    oTable = $('#actividades').DataTable({
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ actividades filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ actividades",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ actividades)",
        }
    });
});