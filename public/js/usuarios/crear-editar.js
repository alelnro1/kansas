$(function(){
    // Si ya hay algo en el input de sucursales_cargos => estamos en editar
    var sucursales_cargos_para_editar = $('#sucursales_cargos').data('sucursales');

    // Con la que trabajamos realmente en jquery
    var sucursales_cargos = [];

    if (sucursales_cargos_para_editar != undefined){
        $.each(sucursales_cargos_para_editar, function(index, data){
            sucursales_cargos.push({
                sucursal_id: data.sucursal_id,
                cargo_id: data.cargo_id
            });
        });
    }

    $('#agregar-sucursal').on('click', function(e){
        e.preventDefault();

        var sucursal_seleccionada = $('#sucursal_id');
        var cargo_seleccionado = $('#cargo_id');

        if (sucursal_seleccionada.val() == "0" || cargo_seleccionado.val() == "0"){
            alert('Seleccione una sucursal y un cargo');
        } else if(cargoExisteEnSucursal(sucursal_seleccionada.val(), cargo_seleccionado.val())){
            alert('El cargo ya existe en la sucursal');
        } else if(sucursalYaTieneUnCargo(sucursal_seleccionada.val())){
            alert('La sucursal seleccionada ya tiene un cargo');
        } else {
            agregarCargoEnSucursal(sucursal_seleccionada, cargo_seleccionado);
        }
    });

    $('#es_admin').on('click', function(e) {
        var selectores = $('#sucursal, #cargo, #agregar-sucursal-cargo, #lista-sucursales-cargos');

        if ($(this).is(':checked')) {
            selectores.slideUp();
        } else {
            selectores.slideDown();
        }
    });

    $('body').on('click', 'a.eliminar-sucursal-cargo', function(e){
        e.preventDefault();

        var sucursal_id = $(this).data('sucursal');

        // Eliminar de la lista de sucursales_cargos
        $.each(sucursales_cargos, function(i){
            if(sucursales_cargos[i].sucursal_id == sucursal_id) {
                sucursales_cargos.splice(i,1);
                return false;
            }
        });

        // Eliminar la fila con la sucursal y el cargo
        $('tr#' + sucursal_id).remove();

        // Si no hay sucursales seleccionadas, ocultar la tabla
        if(sucursales_cargos.length <= 0){
            $('#lista-sucursales-cargos').slideUp();
        }
    });

    function sucursalYaTieneUnCargo(sucursal){
        var existe = false;

        $.each(sucursales_cargos, function(index, value){
            // Si existe el mismo cargo y sucursal en el array => no lo agrego
            if(value.sucursal_id == sucursal){
                existe = true;

                // Salgo del each
                return true;
            }
        });

        return existe;
    }

    function agregarCargoEnSucursal(sucursal, cargo){
        sucursales_cargos.push({
            sucursal_id: parseInt(sucursal.val()),
            cargo_id: parseInt(cargo.val())
        });

        $('#sucursales-cargos').append(
            "<tr id='" + sucursal.val() + "'>" +
            "   <td>" + $('#sucursal_id option:selected').html() + "</td>" +
            "   <td>" + $('#cargo_id option:selected').html() + "</td>" +
            "   <td><a href='#' data-sucursal='" + sucursal.val() + "' data-cargo='" + cargo.val() + "' class='eliminar-sucursal-cargo'><i class='fa fa-times' aria-hidden='true'></i></a>" +
            "</tr>"
        );

        $('#lista-sucursales-cargos').slideDown();
    }

    function cargoExisteEnSucursal(sucursal, cargo){
        var existe = false;

        $.each(sucursales_cargos, function(index, value){
            // Si existe el mismo cargo y sucursal en el array => no lo agrego
            if(value.sucursal_id == sucursal && value.cargo_id == cargo){
                existe = true;

                // Salgo del each
                return true;
            }
        });

        return existe;
    }

    $('.btn-primary').on('click', function(e){
        // Cada vez que se trate de crear el usuario, reinicio las sucursales y usuarios para que no se dupliquen
        $('input#sucursales_cargos').val('');

        $.each(sucursales_cargos, function(index, value){
            var sucursal_cargo = value.sucursal_id + "," + value.cargo_id;
            var separador = "||";

            // Obtengo el valor que ya esta en la sucursal_cargo
            var valores_que_ya_estan = $('input#sucursales_cargos').val();

            // Si no hay valores, no necesito un separador al principio
            if (valores_que_ya_estan == ""){
                separador = "";
            }

            // Antes de enviar el formulario, escribo las sucursales_cargos en el input
            $('input#sucursales_cargos').val(sucursal_cargo + separador + valores_que_ya_estan);
        });
    });
});