$(function(){
    ocultarOMostrarCargoPadre();

    $('#tiene_padre').on('click', function(e){
        ocultarOMostrarCargoPadre();
    });

    function ocultarOMostrarCargoPadre(){
        if($('#tiene_padre').is(":checked")) {
            $('#padre_id').show();
        } else {
            $('#padre_id').hide();
        }
    }
});