$(document).ready(function() {
    oTable = $('#tareas').DataTable({
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ tareas filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ tareas",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ tareas)",
        }
    });
});