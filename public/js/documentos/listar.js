$(document).ready(function() {
    oTable = $('#documentos, #documentos_a_aprobar, #documentos_para_mi, #documentos_propios').DataTable({
        responsive: true,
        columnDefs: [
            { orderable: false, targets: -1 },
            {
                "targets": [ 0, 1 ],
                "visible": false
            }
        ],
        order: [[0, 'desc']],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ documentos filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ documentos",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ documentos)",
        }
    });
});